# Script to create list of file to import for Tanuki wrapper .conf file
import os

DIR="../build/libs/libs"
libList = os.listdir(DIR)
file = open("../build/libs/libList.txt","w")
file.truncate()
index = 3;
for lib in libList:
    if os.path.isfile(os.path.join(DIR, lib)) and "wrapper" not in str(lib):
        file.write("wrapper.java.classpath."+str(index)+"=../lib/"+lib+"\n")
        index+=1
file.close()

print "List of libs created in " + DIR
