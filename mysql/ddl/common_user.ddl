CREATE TABLE IF NOT EXISTS `user`
(
  `userId`   INT(11)      NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(64)  NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `username_index` (`username`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `role`
(
  `roleId`      INT(11)      NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(64)  NOT NULL,
  `description` VARCHAR(255),
  PRIMARY KEY (`roleId`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `user_role`
(
  `userId` INT(11) NOT NULL,
  `roleId` INT(11) NOT NULL,
  PRIMARY KEY (`userId`, `roleId`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `permission`
(
  `roleId`      INT(11)      NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(64)  NOT NULL,
  `group`        VARCHAR(64),
  `description` VARCHAR(255),
  PRIMARY KEY (`roleId`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `user_permission`
(
  `userId`       INT(11) NOT NULL,
  `permissionId` INT(11) NOT NULL,
  PRIMARY KEY (`userId`, `permissionId`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `role_permission`
(
  `roleId`       INT(11) NOT NULL,
  `permissionId` INT(11) NOT NULL,
  PRIMARY KEY (`roleId`, `permissionId`)
)
  ENGINE = InnoDB;
