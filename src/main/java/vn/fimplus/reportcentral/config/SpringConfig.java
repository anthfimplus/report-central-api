package vn.fimplus.reportcentral.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;


@Configuration
public class SpringConfig {
	
	// COMMON DATA
	@Primary
	@Bean(name = "commonDataSource")
	@ConfigurationProperties(prefix = "datasource.common")
	public DataSource commonDataSource() {
		return DataSourceBuilder.create().build();
	}
	
	@Primary
	@Bean(name = "commonJDBC")
	@ConfigurationProperties(prefix = "datasource.common")
	public JdbcTemplate commonJDBC(@Qualifier("commonDataSource") DataSource commonDataSource) {
		return new JdbcTemplate(commonDataSource);
	}
	
	@Primary
	@Bean(name = "commonNamedJDBC")
	@ConfigurationProperties(prefix = "datasource.common")
	public NamedParameterJdbcTemplate commonNamedJDBC(@Qualifier("commonDataSource") DataSource commonDataSource) {
		return new NamedParameterJdbcTemplate(commonDataSource);
	}
	
	// REPORT DATA
	@Bean(name = "reportDataSource")
	@ConfigurationProperties(prefix = "datasource.report")
	public DataSource reportDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "localDataSource")
	@ConfigurationProperties(prefix = "datasource.local")
	public DataSource localDataSource(){
		return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "reportJDBC")
	@ConfigurationProperties(prefix = "datasource.report")
	public JdbcTemplate reportJDBC(@Qualifier("reportDataSource") DataSource reportDataSource) {
		return new JdbcTemplate(reportDataSource);
	}
	
	@Bean(name = "reportNamedJDBC")
	@ConfigurationProperties(prefix = "datasource.report")
	public NamedParameterJdbcTemplate reportNamedJDBC(@Qualifier("reportDataSource") DataSource reportDataSource) {
		return new NamedParameterJdbcTemplate(reportDataSource);
	}
}