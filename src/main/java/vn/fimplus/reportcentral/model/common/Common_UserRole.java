package vn.fimplus.reportcentral.model.common;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Common_UserRole {
	
	public static class Mapper implements RowMapper<Common_UserRole> {
		
		@Override
		public Common_UserRole mapRow(ResultSet rs, int rowNum) throws SQLException {
			Common_UserRole result = new Common_UserRole();
			result.setUserId(rs.getInt("userId"));
			result.setRoleId(rs.getInt("roleId"));
			return result;
		}
		
		
	}
	
	private int userId;
	private int roleId;
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public int getRoleId() {
		return roleId;
	}
	
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
}
