package vn.fimplus.reportcentral.model.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Common_User {
	
	public static class Mapper implements RowMapper<Common_User> {
		
		@Override
		public Common_User mapRow(ResultSet rs, int rowNum) throws SQLException {
			Common_User result = new Common_User();
			result.setUserId(rs.getInt("userId"));
			result.setUsername(rs.getString("username"));
			result.setPassword(rs.getString("password"));
			return result;
		}
	}
	
	private int      userId;
	private String   username;
	@JsonIgnore
	private String   password;
	@JsonIgnore
	private String   token;
	@JsonIgnore
	private DateTime tokenExpireDate;
	
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public DateTime getTokenExpireDate() {
		return tokenExpireDate;
	}
	
	public void setTokenExpireDate(DateTime tokenExpireDate) {
		this.tokenExpireDate = tokenExpireDate;
	}
}
