package vn.fimplus.reportcentral.model.common;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Common_Role {
	
	public static class Mapper implements RowMapper<Common_Role> {
		
		@Override
		public Common_Role mapRow(ResultSet rs, int rowNum) throws SQLException {
			Common_Role result = new Common_Role();
			result.setRoleId(rs.getInt("roleId"));
			result.setName(rs.getString("name"));
			result.setDescription(rs.getString("description"));
			return result;
		}
	}
	
	private int    roleId;
	private String name;
	private String description;
	
	
	public int getRoleId() {
		return roleId;
	}
	
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
