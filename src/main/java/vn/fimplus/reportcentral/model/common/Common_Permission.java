package vn.fimplus.reportcentral.model.common;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Common_Permission {
	
	public static class Mapper implements RowMapper<Common_Permission> {
		
		@Override
		public Common_Permission mapRow(ResultSet rs, int rowNum) throws SQLException {
			Common_Permission result = new Common_Permission();
			result.setPermissionId(rs.getInt("permissionId"));
			result.setName(rs.getString("name"));
			result.setGroup(rs.getString("group"));
			result.setDescription(rs.getString("description"));
			return result;
		}
		
	}
	
	private int    permissionId;
	private String name;
	private String group;
	private String description;
	
	public int getPermissionId() {
		return permissionId;
	}
	
	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getGroup() {
		return group;
	}
	
	public void setGroup(String group) {
		this.group = group;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
