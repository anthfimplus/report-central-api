package vn.fimplus.reportcentral.model;

import vn.fimplus.reportcentral.constant.ApiResponseConstant;

public class ApiResponse {
	
	private int    status  = ApiResponseConstant.STATUS.SUCCESS;
	private String message = ApiResponseConstant.MESSAGE.SUCCESS;
	private Object data;
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Object getData() {
		return data;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
	
	public ApiResponse() {
	}
	
	public ApiResponse(int status, String message, Object data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}
	
	public static ApiResponse success(Object data) {
		return new ApiResponse(ApiResponseConstant.STATUS.SUCCESS, ApiResponseConstant.MESSAGE.SUCCESS, data);
	}
	
	public static ApiResponse error(int errorCode, String message) {
		return new ApiResponse(errorCode, message, null);
	}
}
