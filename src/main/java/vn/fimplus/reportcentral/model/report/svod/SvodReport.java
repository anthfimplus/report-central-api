package vn.fimplus.reportcentral.model.report.svod;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SvodReport {
	
	private Map<String, Record> data;
	
	public Map<String, Record> getData() {
		return data;
	}
	
	public SvodReport(List<String> keys) {
		data = new TreeMap<>();
		for (String key : keys) {
			data.put(key, new Record());
		}
	}
	
	public Record getRecord(String key) {
		return data.get(key);
	}
	
	public void putTrial(String key, Trial trial) {
		Record record = getRecord(key);
		if (record == null) {
			record = new Record();
			data.put(key, record);
		}
		record.setTrial(trial);
	}
	
	public void putAuto(String key, Auto auto) {
		Record record = getRecord(key);
		if (record == null) {
			record = new Record();
			data.put(key, record);
		}
		record.setAuto(auto);
	}
	
	public void putNonAuto(String key, NonAuto nonAuto) {
		Record record = getRecord(key);
		if (record == null) {
			record = new Record();
			data.put(key, record);
		}
		record.setNonAuto(nonAuto);
	}
}
