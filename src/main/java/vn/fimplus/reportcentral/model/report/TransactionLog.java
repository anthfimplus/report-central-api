package vn.fimplus.reportcentral.model.report;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class TransactionLog {
	
	public static class Mapper implements RowMapper<TransactionLog> {
		
		@Override
		public TransactionLog mapRow(ResultSet rs, int rowNum) throws SQLException {
			TransactionLog result = new TransactionLog();
			result.setId(rs.getString("id"));
			result.setTransactionId(rs.getString("transactionId"));
			result.setUserId(rs.getString("userId"));
			result.setCreatedAt(new DateTime(rs.getTimestamp("createdAt")));
			result.setItemId(rs.getString("itemId"));
			result.setName(rs.getString("name"));
			result.setMethodName(rs.getString("methodName"));
			result.setExpiryDate(new DateTime(rs.getTimestamp("expiryDate")).toDate());
			result.setPlatform(rs.getString("platform"));
			result.setActiveCode(rs.getString("activeCode"));
			result.setCodePartner(rs.getString("codePartner"));
			return result;
		}
	}
	
	private String   id;
	private String   transactionId;
	private String   userId;
	private DateTime createdAt;
	private String   itemId;
	private String   name;
	private String   methodName;
	private Date     expiryDate;
	private String   platform;
	private String   deviceName;
	private String   activeCode;
	private String   codePartner;
	
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public DateTime getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		if (createdAt != null)
			this.createdAt = new DateTime(createdAt);
		else
			this.createdAt = null;
	}
	
	public String getItemId() {
		return itemId;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getMethodName() {
		return methodName;
	}
	
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	
	public Date getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public String getPlatform() {
		return platform;
	}
	
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	public String getDeviceName() {
		return deviceName;
	}
	
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	public String getActiveCode() {
		return activeCode;
	}
	
	public void setActiveCode(String activeCode) {
		this.activeCode = activeCode;
	}
	
	public String getCodePartner() {
		return codePartner;
	}
	
	public void setCodePartner(String codePartner) {
		this.codePartner = codePartner;
	}
	
}
