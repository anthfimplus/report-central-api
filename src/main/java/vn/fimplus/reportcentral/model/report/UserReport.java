package vn.fimplus.reportcentral.model.report;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserReport {

    public static class Mapper implements RowMapper<UserReport>{

        @Override
        public UserReport mapRow(ResultSet rs, int rowNum) throws SQLException{
            UserReport result = new UserReport();
            result.setId(rs.getString("id"));
            result.setFullName(rs.getString("fullName"));
            result.setLocalAccId(rs.getString("localAccId"));
            result.setDob(new DateTime(rs.getDate("dob")));
            result.setCreateAt(new DateTime(rs.getDate("createAt")));
            result.setSubName(rs.getString("name"));
            result.setType(rs.getString("type"));
            try {
                result.setSubCreateAt(new DateTime(rs.getDate("subCreateAt")));
            }catch (Exception e){
                result.setSubCreateAt(null);
            }
            return result;
        }
    }

    private String id;
    private String fullName;
    private DateTime dob;
    private DateTime createAt;
    private String localAccId;
    private String type;
    private String subName;
    private DateTime subCreateAt;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public DateTime getDob() {
        return dob;
    }

    public void setDob(DateTime dob) {
        this.dob = dob;
    }

    public DateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(DateTime createAt) {
        this.createAt = createAt;
    }

    public String getLocalAccId() {
        return localAccId;
    }

    public void setLocalAccId(String localAccId) {
        this.localAccId = localAccId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public DateTime getSubCreateAt() {
        return subCreateAt;
    }

    public void setSubCreateAt(DateTime subCreateAt) {
        this.subCreateAt = subCreateAt;
    }
}