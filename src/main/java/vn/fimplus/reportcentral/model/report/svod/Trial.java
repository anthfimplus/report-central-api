package vn.fimplus.reportcentral.model.report.svod;

public class Trial {
	
	private int newTrial;
	private int expired;
	private int end;
	
	public int getNewTrial() {
		return newTrial;
	}
	
	public void setNewTrial(int newTrial) {
		this.newTrial = newTrial;
	}
	
	public int getExpired() {
		return expired;
	}
	
	public void setExpired(int expired) {
		this.expired = expired;
	}
	
	public int getEnd() {
		return end;
	}
}
