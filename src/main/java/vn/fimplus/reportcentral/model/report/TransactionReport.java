package vn.fimplus.reportcentral.model.report;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;
import vn.fimplus.reportcentral.model.CsvExportData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class TransactionReport implements CsvExportData {
	
	public static class Mapper implements RowMapper<TransactionReport> {
		
		@Override
		public TransactionReport mapRow(ResultSet rs, int rowNum) throws SQLException {
			TransactionReport result = new TransactionReport();
			result.setId(rs.getString("id"));
			result.setTransactionId(rs.getString("transactionId"));
			result.setUserId(rs.getString("userId"));
			result.setCreatedAt(new DateTime(rs.getTimestamp("createdAt")));
			result.setItemId(rs.getString("itemId"));
			result.setName(rs.getString("name"));
			result.setMethodName(rs.getString("methodName"));
			result.setExpiryDate(new DateTime(rs.getTimestamp("expiryDate")));
			result.setPlatform(rs.getString("platform"));
			result.setActiveCode(rs.getString("activeCode"));
			result.setCodePartner(rs.getString("codePartner"));
			return result;
		}
	}
	
	
	private String   id;
	private String   transactionId;
	private String   userId;
	private DateTime createdAt;
	private String   itemId;
	private String   name;
	private String   methodName;
	private DateTime expiryDate;
	private String   platform;
	private String   deviceName;
	private String   activeCode;
	private String   codePartner;
	
	@Override
	public String getCSVHeader() {
		return "id" + "," +
				"transactionId" + "," +
				"userId" + "," +
				"createdAt" + "," +
				"itemId" + "," +
				"name" + "," +
				"methodName" + "," +
				"expiryDate" + "," +
				"platform" + "," +
				"deviceName" + "," +
				"activeCode" + "," +
				"codePartner" + "\n";
	}
	
	@Override
	public String toCSV() {
		return this.getId() + "," +
				this.getTransactionId() + "," +
				this.getUserId() + "," +
				this.getCreatedAt() + "," +
				this.getItemId() + "," +
				this.getName() + "," +
				this.getMethodName() + "," +
				this.getExpiryDate() + "," +
				this.getPlatform() + "," +
				this.getDeviceName() + "," +
				this.getActiveCode() + "," +
				this.getCodePartner() + "\n";
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public DateTime getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		if (createdAt != null)
			this.createdAt = new DateTime(createdAt);
		else
			this.createdAt = null;
	}
	
	public String getItemId() {
		return itemId;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getMethodName() {
		return methodName;
	}
	
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	
	public DateTime getExpiryDate() {
		return expiryDate;
	}
	
	public void setExpiryDate(DateTime expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	public String getPlatform() {
		return platform;
	}
	
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	public String getDeviceName() {
		return deviceName;
	}
	
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	public String getActiveCode() {
		return activeCode;
	}
	
	public void setActiveCode(String activeCode) {
		this.activeCode = activeCode;
	}
	
	public String getCodePartner() {
		return codePartner;
	}
	
	public void setCodePartner(String codePartner) {
		this.codePartner = codePartner;
	}
	
}
