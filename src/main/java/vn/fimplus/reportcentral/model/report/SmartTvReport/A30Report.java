package vn.fimplus.reportcentral.model.report.SmartTvReport;

import java.util.Map;
import java.util.TreeMap;

public class A30Report {
    Map<String, Map<String, Integer>> report;

    public A30Report(int month1, int month2, int count){
        String createAt = month1 + "";
        createAt = createAt.substring(0, 4) + "-" + createAt.substring(4);
        String activeAt = month2 + "";
        activeAt = activeAt.substring(0, 4) + "-" + activeAt.substring(4);
        if(report.containsKey(createAt)){
            if(report.get(createAt).containsKey(activeAt))
                report.get(createAt).put(activeAt, report.get(createAt).get(activeAt) + 1);
            else
                report.get(createAt).put(activeAt, 1);
        }
        else {
            Map<String, Integer> monthly = new TreeMap<>();
            monthly.put(activeAt, 1);
            report.put(createAt, monthly);
        }
    }
}
