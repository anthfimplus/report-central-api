package vn.fimplus.reportcentral.model.report.MoneyReport2018;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TvodReport {

    public static class Mapper implements RowMapper<TvodReport>{

        @Override
        public TvodReport mapRow(ResultSet rs, int rowNum) throws SQLException{
            TvodReport result = new TvodReport();
            result.setId(rs.getString("id"));
            result.setPlatform(rs.getString("platform"));
            result.setUserId(rs.getString("userId"));
            result.setCreatedAt(rs.getDate("createdAt").toString());
            return result;
        }
    }

    private String id;
    private String userId;
    private String platform;
    private String userFullName;
    private String userEmail;
    private String createdAt;

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        if(platform != null) {
            switch (platform) {
                case "tv_tizen":
                    this.platform = "SAMSUNG";
                    break;
                case "tv_opera":
                    this.platform = "SONY";
                    break;
                case "tv_android":
                    this.platform = "SONY";
                    break;
                case "tv_webos":
                    this.platform = "LG";
                    break;
                case "tv_netcast":
                    this.platform = "LG";
                    break;
                case "web":
                    this.platform = "PC";
                    break;
                case "web_mobile":
                    this.platform = "PC";
                    break;
                case "web_safari":
                    this.platform = "PC";
                    break;
                case "galaxy-event":
                    this.platform = "PC";
                    break;
                default:
                    this.platform = platform;
            }
        }
        else{}
    }
}
