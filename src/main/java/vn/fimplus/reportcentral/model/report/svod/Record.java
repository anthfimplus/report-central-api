package vn.fimplus.reportcentral.model.report.svod;

public class Record {
	
	int       total;
	Trial     trial     = new Trial();
	Auto      auto      = new Auto();
	NonAuto   nonAuto   = new NonAuto();
	PayingSub payingSub = new PayingSub();
	
	public int getTotal() {
		return total;
	}
	
	public Trial getTrial() {
		return trial;
	}
	
	public void setTrial(Trial trial) {
		this.trial = trial;
	}
	
	public Auto getAuto() {
		return auto;
	}
	
	public void setAuto(Auto auto) {
		this.auto = auto;
	}
	
	public NonAuto getNonAuto() {
		return nonAuto;
	}
	
	public void setNonAuto(NonAuto nonAuto) {
		this.nonAuto = nonAuto;
	}
	
	public PayingSub getPayingSub() {
		return payingSub;
	}
	
	public void calcTotal() {
	}
	
}
