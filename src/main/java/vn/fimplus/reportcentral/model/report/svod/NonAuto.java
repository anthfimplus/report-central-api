package vn.fimplus.reportcentral.model.report.svod;

public class NonAuto {
	
	private int firstTime;
	private int repurchase;
	private int expired;
	private int end;
	
	public int getFirstTime() {
		return firstTime;
	}
	
	public void setFirstTime(int firstTime) {
		this.firstTime = firstTime;
	}
	
	public int getRepurchase() {
		return repurchase;
	}
	
	public void setRepurchase(int repurchase) {
		this.repurchase = repurchase;
	}
	
	public int getExpired() {
		return expired;
	}
	
	public void setExpired(int expired) {
		this.expired = expired;
	}
	
	public int getEnd() {
		return end;
	}
	
	public void setEnd(int end) {
		this.end = end;
	}
}
