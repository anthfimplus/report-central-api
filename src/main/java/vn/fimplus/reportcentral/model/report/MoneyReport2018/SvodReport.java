package vn.fimplus.reportcentral.model.report.MoneyReport2018;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SvodReport {

    public static class Mapper implements RowMapper<SvodReport> {

        @Override
        public SvodReport mapRow(ResultSet rs, int rowNum) throws SQLException{
            SvodReport result = new SvodReport();
            result.setTransactionId(rs.getString("transactionId"));
            result.setMethodName(rs.getString("methodName"));
            result.setItemId(rs.getString("itemId"));
            result.setPlatform(rs.getString("platform"));
            result.setBundling(rs.getString("isBundling"));
            return result;
        }
    }

    private String transactionId;
    private String methodName;
    private String platform;
    private String itemId;
    private boolean isBundling;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        if(platform == null || platform.equals("")){
            if(methodName.equals("MOBIFONE"))
                this.platform = "MBF";
            else if(itemId.equals("gstart"))
                this.platform = "PC";
            else if(itemId.equals("cac38cc6-29ee-4b89-902d-0241bef2c8b5") || itemId.equals("1846638d-a3d3-4482-8419-aa170f39f3d6"))
                this.platform = "SAMSUNG";
            else if(itemId.equals("4853d9bd-f6c5-4748-aa8a-8fecd6844c38"))
                this.platform = "SONY";
        }
        else if(platform.equals("tv_tizen"))
            this.platform = "SAMSUNG";
        else if(platform.equals("tv_opera") || platform.equals("tv_android"))
            this.platform = "SONY";
        else if(platform.equals("tv_webos") || platform.equals("tv_netcast"))
            this.platform = "LG";
        else if(platform.equals("web") || platform.equals("web_safari") || platform.equals("web_mobile") || platform.equals("galaxy-event"))
            this.platform = "PC";
        else
            this.platform = platform;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public boolean isBundling() {
        return isBundling;
    }

    public void setBundling(String bundling) {
        if(bundling.equals("bundling"))
            isBundling = true;
        else
            isBundling = false;
    }
}
