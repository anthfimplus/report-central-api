package vn.fimplus.reportcentral.model.report;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.core.type.TypeReference;
import vn.fimplus.reportcentral.util.JsonUtils;
import vn.fimplus.reportcentral.model.UserActivityObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserActivityReport{
   List<UserActivityObject> list;

   public UserActivityReport getList(JsonNode node) throws IOException {
       if(this.list == null)
           this.list = new ArrayList<>();

       JsonNode temp = node.get("hits").get("hits");
       for(JsonNode aNode : temp){
           JsonNode target = aNode.get("_source");
           this.list.add(JsonUtils.parseJson(target.toString(), new TypeReference<UserActivityObject>() {
           }, true));
       }
       return this;
   }

   public void appendUser(UserActivityObject user){
       if(this.list == null){
           this.list = new ArrayList<>();
           this.list.add(user);
       }
       else
           this.list.add(user);
   }

   public UserActivityReport getUserActivityReport(){
       return this;
   }

    public List<UserActivityObject> getList() {
        return list;
    }

    public void setList(List<UserActivityObject> list) {
        this.list = list;
    }
}
