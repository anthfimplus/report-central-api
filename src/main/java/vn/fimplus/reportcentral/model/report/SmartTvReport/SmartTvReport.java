package vn.fimplus.reportcentral.model.report.SmartTvReport;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SmartTvReport {
    public static class Mapper implements RowMapper<SmartTvReport>{
        @Override
        public SmartTvReport mapRow(ResultSet rs, int rowNum) throws SQLException{
            SmartTvReport result = new SmartTvReport();
            result.setId(rs.getString("id"));
            result.setTransactionId(rs.getString("transactionId"));
            result.setUserId(rs.getString("userId"));
            result.setCreatedAt(rs.getDate("createdAt"));
            result.setItemId(rs.getString("itemId"));
            result.setName(rs.getString("name"));
            result.setMethodName(rs.getString("methodName"));
            result.setExpiryDate(rs.getDate("expiryDate"));
            result.setPlatform(rs.getString("platform"));
            result.setDeviceName(rs.getString("deviceName"));
            result.setGroupContentId(rs.getString("groupContentId"));
            try{
                result.setActiveCode(rs.getString("activeCode"));
            }catch (Exception e){
                result.setActiveCode(null);
            }
            try{
                result.setCodePartner(rs.getString("codePartner"));
            }catch (Exception e){
                result.setCodePartner(null);
            }
            return result;
        }
    }

    private String id;
    private String transactionId;
    private String userId;
    private String itemId;
    private String name;
    private String methodName;
    private String platform;
    private String deviceName;
    private String activeCode;
    private String codePartner;
    private String groupContentId;
    @JsonIgnore
    private DateTime createdAt;
    @JsonIgnore
    private DateTime expiryDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUserId(){
        return userId;
    }

    public void setUserId(String userId){
        this.userId = userId;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createAt) {
        this.createdAt = DateTime.parse(createAt.toString());
    }

    public void setCreatedAt(DateTime createAt){
        this.createdAt = createAt;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public DateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = DateTime.parse(expiryDate.toString());
    }

    public void setExpiryDate(DateTime expiryDate){
        this.expiryDate = expiryDate;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getGroupContentId() {
        return groupContentId;
    }

    public void setGroupContentId(String groupContentId) {
        this.groupContentId = groupContentId;
    }

    public String getActiveCode() {
        return activeCode;
    }

    public void setActiveCode(String activeCode) {
        this.activeCode = activeCode;
    }

    public String getCodePartner() {
        return codePartner;
    }

    public void setCodePartner(String codePartner) {
        this.codePartner = codePartner;
    }


    public String getStringCreateAt(){
        return createdAt.toString("yyyy-MM-dd HH:mm:ss");
    }

    public String getStringExpiryDate(){
        return expiryDate.toString("yyyy-MM-dd HH:mm:ss");
    }

    public String getStringCreatedAt(){
        return createdAt.toString("yyyy-MM-dd HH:mm:ss");
    }
}
