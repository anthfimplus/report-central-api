package vn.fimplus.reportcentral.model.report;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;
import vn.fimplus.reportcentral.model.CsvExportData;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MBFTransaction implements CsvExportData {
	
	
	public static class Mapper implements RowMapper<MBFTransaction> {
		
		@Override
		public MBFTransaction mapRow(ResultSet rs, int rowNum) throws SQLException {
			MBFTransaction result = new MBFTransaction();
//		setUserId(rs.getString("userId"));
			result.setTransactionId(rs.getString("id"));
			result.setMsisdn(rs.getString("msisdn"));
			result.setTransactionCode(rs.getString("transactionCode"));
			result.setCreatedAt(new DateTime(rs.getTimestamp("createdAt")));
			return result;
		}
	}
	
	//	private String   userId;
	private String   transactionId;
	private String   msisdn;
	private String   transactionCode;
	private DateTime createdAt;
	
	@Override
	public String getCSVHeader() {
		return "transactionId" + "," +
				"msisdn" + "," +
				"transactionCode" + "," +
				"createdAt" + "\n";
	}
	
	@Override
	public String toCSV() {
		return this.getTransactionId() + "," +
				this.getMsisdn() + "," +
				this.getTransactionCode() + "," +
				this.getCreatedAt() + "\n";
	}

//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}
	
	
	public String getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getMsisdn() {
		return msisdn;
	}
	
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
	public String getTransactionCode() {
		return transactionCode;
	}
	
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	
	public DateTime getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		if (createdAt != null)
			this.createdAt = new DateTime(createdAt);
		else
			this.createdAt = null;
	}
}
