package vn.fimplus.reportcentral.model.report;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class UserRegisterReport {
	
	public static class Mapper implements RowMapper<UserRegisterReport> {
		
		@Override
		public UserRegisterReport mapRow(ResultSet rs, int rowNum) throws SQLException {
			UserRegisterReport result = new UserRegisterReport();
			result.setUserId(rs.getString("id"));
			result.setFullName(rs.getString("fullName"));
			result.setEmail(rs.getString("email"));
			result.setPendingEmail(rs.getString("pendingEmail"));
			result.setCreatedAt(new DateTime(rs.getTimestamp("createdAt")));
			result.setMobile(rs.getString("mobile"));
			result.setDeviceName(rs.getString("type"));
			result.setIsBanned(rs.getInt("isBanned"));
			result.setPlatform(rs.getString("type_platform"));
			return result;
		}
		
	}
	
	private String   userId;
	private String   fullName;
	private String   email;
	private String   pendingEmail;
	private DateTime createdAt;
	private String   mobile;
	private String   deviceName;
	@JsonIgnore
	private int      isBanned;
	private String   platform;
	
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPendingEmail() {
		return pendingEmail;
	}
	
	public void setPendingEmail(String pendingEmail) {
		this.pendingEmail = pendingEmail;
	}
	
	public DateTime getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}
	
	public void setCreatedAt(Date createdAt) {
		if (createdAt != null)
			this.createdAt = new DateTime(createdAt);
		else
			this.createdAt = null;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	public String getDeviceName() {
		return deviceName;
	}
	
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	public int getIsBanned() {
		return isBanned;
	}
	
	public void setIsBanned(int isBanned) {
		this.isBanned = isBanned;
	}
	
	public String getStatus() {
		switch (isBanned) {
			case 0:
				return "Normal";
			case 2:
				return "InReview";
			default:
				return "Banned";
			
		}
	}
	
	public String getPlatform() {
		return platform;
	}
	
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	
	public String getChannel() {
		if (mobile != null && !mobile.isEmpty())
			return "Mobile Phone";
		return "Facebook";
	}
}
