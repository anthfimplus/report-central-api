package vn.fimplus.reportcentral.model.report;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class User {
    public static class Mapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException{
            User result = new User();
            result.setId(rs.getString("id"));
            if(rs.getDate("dob") != null)
                result.setDob(new DateTime(rs.getDate("dob")).toString("yyyy-MM-dd"));
            result.setFullName(rs.getString("fullName"));
            result.setCreateAt(new DateTime(rs.getDate("createdAt")).toString("yyyy-MM-dd"));
            result.setLocalAccId(rs.getString("localAccId"));
            result.setFbAccId(rs.getString("fbAccId"));
            result.setGgAccId(rs.getString("ggAccId"));
            result.setEmail(rs.getString("email"));
            result.setUuid(rs.getString("uuid"));
            result.setPlatform(rs.getString("platform"));
            result.setModelId(rs.getString("modelId"));
            result.setIsBanned(rs.getInt("isBanned"));
            return result;
        }
    }

    private String id;
    private String fullName;
    private String dob;
    private String createAt;
    private String localAccId;
    private String fbAccId;
    private String ggAccId;
    private String email;
    private String uuid;
    private String platform;
    private String modelId;
    private int isBanned;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getLocalAccId() {
        return localAccId;
    }

    public void setLocalAccId(String localAccId) {
        this.localAccId = localAccId;
    }

    public String getFbAccId() {
        return fbAccId;
    }

    public void setFbAccId(String fbAccId) {
        this.fbAccId = fbAccId;
    }

    public String getGgAccId() {
        return ggAccId;
    }

    public void setGgAccId(String ggAccId) {
        this.ggAccId = ggAccId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public int getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(int isBanned) {
        this.isBanned = isBanned;
    }
}
