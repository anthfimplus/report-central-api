package vn.fimplus.reportcentral.model.report;

import org.joda.time.DateTime;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TransactionOppo {
    public static class Mapper implements RowMapper<TransactionOppo> {

        @Override
        public TransactionOppo mapRow(ResultSet rs, int rowNum) throws SQLException {
            TransactionOppo result = new TransactionOppo();
            result.setId(rs.getString("id"));
            result.setUserId(rs.getString("userId"));
            result.setCreatedAt(new DateTime(rs.getTimestamp("createdAt")));
            result.setItemId(rs.getString("itemId"));
            result.setExpiryDate(new DateTime(rs.getTimestamp("expiryDate")));
            result.setPlatform(rs.getString("platform"));
            return result;
        }
    }
    private String   id;
    private String   userId;
    private DateTime createdAt;
    private String   itemId;
    private DateTime expiryDate;
    private String   platform;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public DateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(DateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getStringExpiryDate() {
        return expiryDate.toString("yyyy-MM-dd HH:mm:ss");
    }

    public String getStringCreatedAt() {
        return createdAt.toString("yyyy-MM-dd HH:mm:ss");
    }
}
