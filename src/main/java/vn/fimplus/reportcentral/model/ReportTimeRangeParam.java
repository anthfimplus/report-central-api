package vn.fimplus.reportcentral.model;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Months;
import org.joda.time.Years;

import java.util.ArrayList;
import java.util.List;

public class ReportTimeRangeParam {
	
	private DateTime     from;
	private DateTime     to;
	private int          cycleType;
	private List<String> cycleList;
	private boolean      isSameYear;
	private boolean      isSameMonth;
	
	public ReportTimeRangeParam(DateTime from, DateTime to, int cycleType) {
		this.from = from;
		this.to = to;
		this.cycleType = cycleType;
		cycleList = getCycleList(from, to, cycleType);
		isSameYear = false;
		isSameMonth = false;
		int month = Months.monthsBetween(from, to).getMonths();
		if (month == 0) {
			isSameMonth = true;
			isSameYear = true;
		}
		if (to.getYear() == from.getYear())
			isSameYear = true;
	}
	
	public String getCycleKey(DateTime input) {
		return getCycleKey(cycleType, isSameYear, isSameMonth, input);
	}
	
	public static String getCycleKey(int cycleType, boolean isSameYear, boolean isSameMonth, DateTime input) {
		String result;
		switch (cycleType) {
			case 0: // day
				result = (isSameYear ? "" : input.getYear() + "-") +
						(isSameMonth ? "" : String.format("%02d", input.getMonthOfYear()) + "-") +
						String.format("%02d", input.getDayOfMonth());
				break;
			case 2: // quarter
				result = (isSameYear ? "" : input.getYear() + "-") +
						"Q" + ((input.getMonthOfYear() + 2) / 3);
				break;
			case 3: // year
				result = input.toString("yyyy");
				break;
			case 1: // month
			default:
				result = (isSameYear ? "" : input.getYear() + "-") +
						String.format("%02d", input.getMonthOfYear());
				break;
		}
		return result;
	}
	
	public static List<String> getCycleList(DateTime from, DateTime to, int cycleType) {
		boolean isSameYear = false;
		boolean isSameMonth = false;
		int month = Months.monthsBetween(from, to).getMonths();
		if (month == 0) {
			isSameMonth = true;
			isSameYear = true;
		}
		if (to.getYear() == from.getYear())
			isSameYear = true;
		List<String> result = new ArrayList<>();
		switch (cycleType) {
			case 0: // day
				int dayDiff = Days.daysBetween(from, to).getDays();
				for (int i = 0; i <= dayDiff; i++)
					result.add(getCycleKey(cycleType, isSameYear, isSameMonth, from.plusDays(i)));
				break;
			case 2: // quarter
				int quarterDiff = (Months.monthsBetween(from, to).getMonths() - 1 + 2) / 3;
				for (int i = 0; i <= quarterDiff; i++)
					result.add(getCycleKey(cycleType, isSameYear, isSameMonth, from.plusMonths(i * 3)));
				break;
			case 3: // year
				int yearDiff = Years.yearsBetween(from, to).getYears();
				for (int i = 0; i <= yearDiff; i++)
					result.add(getCycleKey(cycleType, isSameYear, isSameMonth, from.plusYears(i)));
				break;
			case 1: // month
			default:
				int monthDiff = Months.monthsBetween(from, to).getMonths();
				for (int i = 0; i <= monthDiff; i++)
					result.add(getCycleKey(cycleType, isSameYear, isSameMonth, from.plusMonths(i)));
				break;
		}
		return result;
	}
	
	
	public boolean isInRange(DateTime timeToCheck) {
		return from.compareTo(timeToCheck) * timeToCheck.compareTo(to) >= 0;
	}
	
	public DateTime getFrom() {
		return from;
	}
	
	public void setFrom(DateTime from) {
		this.from = from;
	}
	
	public DateTime getTo() {
		return to;
	}
	
	public void setTo(DateTime to) {
		this.to = to;
	}
	
	public int getCycleType() {
		return cycleType;
	}
	
	public void setCycleType(int cycleType) {
		this.cycleType = cycleType;
	}
	
	public List<String> getCycleList() {
		return cycleList;
	}
	
	public void setCycleList(List<String> cycleList) {
		this.cycleList = cycleList;
	}
	
	public boolean isSameYear() {
		return isSameYear;
	}
	
	public void setSameYear(boolean sameYear) {
		isSameYear = sameYear;
	}
	
	public boolean isSameMonth() {
		return isSameMonth;
	}
	
	public void setSameMonth(boolean sameMonth) {
		isSameMonth = sameMonth;
	}
	
}
