package vn.fimplus.reportcentral.model;

public interface CsvExportData {
	
	String getCSVHeader();
	
	String toCSV();
}
