package vn.fimplus.reportcentral.constant;

public class ApiResponseConstant {
	public static final class STATUS {
		public static final int	SUCCESS	= 0;
		public static final int	ERROR	= 1;
	}
	
	public static final class MESSAGE {
		// Global
		public static final String	SUCCESS					= "SUCCESS";
		public static final String	FAIL					= "FAIL";
		public static final String	ERROR					= "ERROR";
		public static final String	PROTECTED_OBJECT		= "PROTECTED_OBJECT";
		public static final String	WRONG_FORMAT			= "WRONG_FORMAT";
		public static final String	NOT_PERMISS				= "NOT_PERMISS";
		public static final String	INVALID_TOKEN			= "INVALID_TOKEN";
		// User
		public static final String	WRONG_PASSWORD			= "WRONG_PASSWORD";
		public static final String	BANED					= "BANED";
		public static final String	EXIST					= "EXIST";
		public static final String	NOT_EXIST				= "NOT_EXIST";
		public static final String	WRONG_GROUP				= "WRONG_GROUP";
		// Resource
		public static final String	UPLOAD_FAIL				= "UPLOAD_FAIL";
		public static final String	WRONG_IMAGE_FORMAT		= "WRONG_IMAGE_FORMAT";
		public static final String	WRONG_CONFIG_FORMAT		= "WRONG_CONFIG_FORMAT";
	}
	
}
