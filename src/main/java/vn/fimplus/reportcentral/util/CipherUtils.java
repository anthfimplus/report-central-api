package vn.fimplus.reportcentral.util;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;

public class CipherUtils {
	
	public final static String DEFAULT_KEY = "Y^W_}6z!B'k3$pyF";
	public final static String IV          = "Dh7Qu6jTLTjtzBzK";
	
	public static String encryptDefault(String input) {
		return encrypt(input, DEFAULT_KEY);
	}
	
	public static String decryptDefault(String input) {
		return decrypt(input, DEFAULT_KEY);
	}
	
	public static String encrypt(String input, String key) {
		byte[] inputBytes = input.getBytes();
		try {
			SecretKeySpec desKey = new SecretKeySpec(key.getBytes(), "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(IV.getBytes());
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			
			cipher.init(Cipher.ENCRYPT_MODE, desKey, ivSpec);
			byte[] encrypted = cipher.doFinal(inputBytes);
			return DatatypeConverter.printBase64Binary(encrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String decrypt(String input, String key) {
		byte[] inputBytes = Base64.decodeBase64(input);
		try {
			SecretKeySpec desKey = new SecretKeySpec(key.getBytes(), "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(IV.getBytes());
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			
			cipher.init(Cipher.DECRYPT_MODE, desKey, ivSpec);
			byte[] decrypted = cipher.doFinal(inputBytes);
			return new String(decrypted, StandardCharsets.UTF_8);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
