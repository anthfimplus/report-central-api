package vn.fimplus.reportcentral.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.joda.time.DateTime;
import org.slf4j.LoggerFactory;
import vn.fimplus.reportcentral.model.report.SmartTvReport.SmartTvReport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class ExcelExport {

    private static final Logger log = LoggerFactory.getLogger(HttpJsonUtils.class);

    private static String directory = System.getProperty("user.dir");

    public static void singleExport(List<String> userList, String reportname){
        new File(directory + File.separator + "Report").mkdir();
        String fileName = directory + File.separator + "Report" + File.separator + reportname + ".xls";
        File file = new File(fileName);
        XSSFWorkbook workbook;
        try{
            if(!file.isFile()){
                workbook = new XSSFWorkbook();
                createGenericExcel(userList, workbook);
            }
            else {
                FileInputStream fis = new FileInputStream(file);
                workbook = new XSSFWorkbook(fis);
                createGenericExcel(userList, workbook);
            }
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
        }catch (Exception e) {
            log.info(e.getMessage());
        }
    }


    public static void genericExport(Map map, String reportname){
        new File(directory + File.separator + "Report").mkdir();
        String fileName = directory + File.separator + "Report" + File.separator + reportname + ".xls";
        File file = new File(fileName);
        XSSFWorkbook workbook;
        try{
            if(!file.isFile()){
                workbook = new XSSFWorkbook();
                ExcelExport.createGenericExcel(map, workbook);
            }
            else {
                FileInputStream fis = new FileInputStream(file);
                workbook = new XSSFWorkbook(fis);
                createGenericExcel(map, workbook);
            }
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    public static void genericExport(List<String> list, String reportname){
        new File(directory + File.separator + "Report").mkdir();
        String fileName = directory + File.separator + "Report" + File.separator + reportname + ".xls";
        try{
            XSSFWorkbook workbook = new XSSFWorkbook();
            createGenericExcel(list, workbook);
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    public static void exportUserList(Map<String, Map<String, Map<String, List<String>>>> map, String reportname){
        String date = new DateTime().toString("yyyyMMdd");
        new File(directory + File.separator + "Report").mkdir();
        String fileName = directory + File.separator + "Report" + File.separator + reportname + " " + date + ".xls";
        try{
            XSSFWorkbook workbook = new XSSFWorkbook();
            createUserListExcel(map, workbook);
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    private static void createUserListExcel(Map<String, Map<String, Map<String, List<String>>>> map, XSSFWorkbook workbook){
        for(String i: map.get("samsung").keySet()) {
            XSSFSheet sheet = workbook.getSheet(i);
            if (sheet == null) {
                sheet = workbook.createSheet(i);
            }
            int currentRow = 0;
            int currentCollumn;

            Row row = sheet.createRow(currentRow);
            for (currentCollumn = 0; currentCollumn < 3; currentCollumn++) {
                Cell cell = row.createCell(currentCollumn);
                if (currentCollumn == 0)
                    cell.setCellValue("User Id");
                else if (currentCollumn == 1)
                    cell.setCellValue("date");
                else
                    cell.setCellValue("platform");
            }
        }
        parseUserList(workbook,map,1);
    }

    private static void parseUserList(XSSFWorkbook workbook, Map<String, Map<String, Map<String, List<String>>>> map, int startRow){
        String[] sheetName = {"First Transaction As Bundling", "Independent User List"};
        for(String report: sheetName){
            XSSFSheet sheet = workbook.getSheet(report);
            int current = startRow;
            for(String platform: map.keySet()){
                for(String date: map.get(platform).get(report).keySet()){
                    List<String> userList = map.get(platform).get(report).get(date);
                    if(userList.size() != 0) {
                        for (String userId : userList) {
                            Row currentRow = sheet.createRow(current);
                            currentRow.createCell(0).setCellValue(userId);
                            currentRow.createCell(1).setCellValue(date);
                            currentRow.createCell(2).setCellValue(platform);
                            current++;
                        }
                    }
                }
            }
        }
    }

    public static void exportExcelElastic(Map<String, Map<String, Map<String, Integer>>> map, String reportname){
        new File(directory + File.separator + "Report").mkdir();
        String fileName = directory + File.separator + "Report" + File.separator + reportname + ".xls";
        File file = new File(fileName);
        XSSFWorkbook workbook;
        try{
            if(!file.isFile()){
                workbook = new XSSFWorkbook();
                ExcelExport.createExcelElastic(map, workbook);
            }
            else{
                FileInputStream fis = new FileInputStream(file);
                workbook = new XSSFWorkbook(fis);
                XSSFSheet mySheet = workbook.getSheetAt(0);
                int i = 0;
                while(mySheet.getRow(i) != null){
                    i++;
                }
                parseValueElastic(mySheet, map, i);
            }
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
        }catch (Exception e){
            log.info(e.getMessage());
        }

    }

    private static void createExcelElastic(Map<String, Map<String, Map<String, Integer>>> map, XSSFWorkbook workbook){
        XSSFSheet sheet = workbook.getSheet("sheet 1");
        if(sheet == null)
            sheet = workbook.createSheet("sheet 1");
        generateFormatElastic(sheet);
        parseValueElastic(sheet, map,2);
    }

    private static void parseValueElastic(XSSFSheet sheet, Map<String, Map<String, Map<String, Integer>>> map, int startRow){
        int currentRow = startRow;
        for(String movieName : map.keySet()){
            int currentColumn = 0;
            if(sheet.getRow(currentRow) == null)
                sheet.createRow(currentRow);
            for(; currentColumn < 8; currentColumn++) {
                if (currentColumn != 1) {
                    if (sheet.getRow(currentRow).getCell(currentColumn) == null)
                        sheet.getRow(currentRow).createCell(currentColumn);
                    switch (currentColumn) {
                        case 0:
                            sheet.getRow(currentRow).getCell(currentColumn).setCellValue(movieName);
                            break;
                        case 2:
                            if (map.get(movieName).containsKey("1-month-premium") && map.get(movieName).get("1-month-premium").containsKey("watchTime"))
                                sheet.getRow(currentRow).getCell(currentColumn).setCellValue(map.get(movieName).get("1-month-premium").get("watchTime"));
                            break;
                        case 3:
                            if (map.get(movieName).containsKey("1-month-basic") && map.get(movieName).get("1-month-basic").containsKey("watchTime"))
                                sheet.getRow(currentRow).getCell(currentColumn).setCellValue(map.get(movieName).get("1-month-basic").get("watchTime"));
                            break;
                        case 4:
                            if (map.get(movieName).containsKey("1-month-premium") && map.get(movieName).get("1-month-premium").containsKey("uid"))
                                sheet.getRow(currentRow).getCell(currentColumn).setCellValue(map.get(movieName).get("1-month-premium").get("uid"));
                            break;
                        case 5:
                            if (map.get(movieName).containsKey("1-month-basic") && map.get(movieName).get("1-month-basic").containsKey("uid"))
                                sheet.getRow(currentRow).getCell(currentColumn).setCellValue(map.get(movieName).get("1-month-basic").get("uid"));
                            break;
                        case 6:
                            if (map.get(movieName).containsKey("a90") && map.get(movieName).get("a90").containsKey("1-month-premium"))
                                sheet.getRow(currentRow).getCell(currentColumn).setCellValue(map.get(movieName).get("a90").get("1-month-premium"));
                            break;
                        case 7:
                            if (map.get(movieName).containsKey("a90") && map.get(movieName).get("a90").containsKey("1-month-basic"))
                                sheet.getRow(currentRow).getCell(currentColumn).setCellValue(map.get(movieName).get("a90").get("1-month-basic"));
                            break;
                    }
                }
            }
            currentRow++;
        }
    }

    private static void generateFormatElastic(XSSFSheet sheet){
        String[] cellTitle = {"name", "Movie On Time", "Premium", "Basic", "Premium", "Basic", "Premium", "Basic"};
        if(sheet.getRow(0) == null)
            sheet.createRow(0);
        int currentColumn = 2;
        for(int i = 0; i < 5; i++) {
            if (currentColumn % 2 == 0)
                switch (currentColumn) {
                    case 2:
                        sheet.getRow(0).createCell(currentColumn).setCellValue("Time-on-view a90");
                        break;
                    case 4:
                        sheet.getRow(0).createCell(currentColumn).setCellValue("Sub a90");
                        break;
                    case 6:
                        sheet.getRow(0).createCell(currentColumn).setCellValue("Total sub a90");
                        break;
                }
            else {
                sheet.getRow(0).createCell(currentColumn);
                sheet.addMergedRegion(new CellRangeAddress(0, 0, currentColumn - 1, currentColumn));
            }
            currentColumn++;
        }
        currentColumn = 0;
        sheet.createRow(1);
        for(; currentColumn < 8; currentColumn++){
            sheet.getRow(1).createCell(currentColumn).setCellValue(cellTitle[currentColumn]);
        }
    }

    public static void exportExcelSmartTv(Map<String, List<SmartTvReport>> map, String reportname){
        try{
            new File(directory + File.separator + "Report").mkdir();
            String fileName = directory + File.separator + "Report" + File.separator + reportname + ".xls";
            XSSFWorkbook workbook = new XSSFWorkbook();
            ExcelExport.createExcelSmartTv(map, workbook);
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    public static void exportExcel(Map map, String reportname){
        try{
            new File(directory + File.separator + "Report").mkdir();
            String fileName = directory + File.separator + "Report" + File.separator + reportname + ".xls";
            XSSFWorkbook workbook = new XSSFWorkbook();
            ExcelExport.createExcel(map, workbook);
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
        }catch (Exception e){
            log.info(e.getMessage());
        }
    }

    private static void createExcelSmartTv(Map<String, List<SmartTvReport>> aMap, XSSFWorkbook workBook){
        for (Object key : aMap.keySet()) {
            XSSFSheet sheet = workBook.getSheet(key.toString());
            if(sheet == null)
                sheet = workBook.createSheet(key.toString());
            generateFormatSmartTv(sheet);
            parseValueSmartTv(sheet, aMap.get(key));
        }
    }

    private static void generateFormatSmartTv(XSSFSheet sheet){
        if(sheet.getRow(0) == null)
            sheet.createRow(0);
        int currentColumn = 0;
        Field[] list = SmartTvReport.class.getDeclaredFields();
        for(Field i : list){
            if(sheet.getRow(0).getCell(currentColumn) == null){
                sheet.getRow(0).createCell(currentColumn);
                sheet.getRow(0).getCell(currentColumn).setCellValue(i.getName());

            }
            currentColumn++;
        }
    }

    //Create Excel file with 1 sheet
    private static void createExcel2(XSSFWorkbook workBook){
        XSSFSheet sheet = workBook.getSheet("Sheet1");
        if(sheet == null)
            sheet = workBook.createSheet("Sheet1");
        if(sheet.getRow(0) == null)
            sheet.createRow(0);
        if(sheet.getRow(0).getCell(0) == null)
            sheet.getRow(0).createCell(0).setCellValue("userId");
    }

    private static void createGenericExcel(Map aMap,XSSFWorkbook workbook){
        XSSFSheet sheet = workbook.getSheet("Sheet1");
        if(sheet == null)
            sheet = workbook.createSheet("Sheet1");
        if(sheet.getRow(0) == null)
            sheet.createRow(0);
        for(int i = 0; i < 2; i++){
            if(sheet.getRow(0).getCell(i) == null){
                if(i == 0){
                    sheet.getRow(0).createCell(i).setCellValue("userId");
                }
                else
                    sheet.getRow(0).createCell(i).setCellValue("Insert Title");
            }
        }
        int currentRow = 1;
        while(sheet.getRow(currentRow) != null){
            currentRow++;
        }
        for(Object key : aMap.keySet()){
            Row row = sheet.createRow(currentRow);
            row.createCell(0).setCellValue(key.toString());
            row.createCell(1).setCellValue(aMap.get(key).toString());
            currentRow++;
        }
    }

    private static void createGenericExcel(List<String> list, XSSFWorkbook workbook){
        XSSFSheet sheet = workbook.getSheet("Sheet1");
        if(sheet == null)
            sheet = workbook.createSheet("Sheet1");
        if(sheet.getRow(0) == null)
            sheet.createRow(0);
        if(sheet.getRow(0).getCell(0) == null){
            sheet.getRow(0).createCell(0).setCellValue("userId");
        }
        int currentRow = 1;
        while(sheet.getRow(currentRow) != null){
            currentRow++;
        }
        for(int i = 0; i < list.size(); i++){
            Row row = sheet.createRow(currentRow);
            row.createCell(0).setCellValue(list.get(i));
            currentRow++;
        }
    }

    //Create excel file with multiple sheet corresponding to specific report
    private static void createExcel(Map aMap, XSSFWorkbook workBook) {
        for (Object key : aMap.keySet()) {
            if (aMap.get(key) instanceof Map) {
                XSSFSheet sheet = workBook.getSheet(key.toString());
                if(sheet == null)
                    sheet = workBook.createSheet(key.toString());
                generateFormat(0,0,(Map) aMap.get(key),sheet);
                parseValue(0, 0, (Map) aMap.get(key), sheet);
            }
            else {
                XSSFSheet sheet = workBook.getSheet("Sheet1");
                if(sheet == null)
                    sheet = workBook.createSheet("Sheet1");
                generateFormat(0,0, aMap, sheet);
                parseValue(0, 0, aMap, sheet);
            }
        }
    }

    private static void parseValueSmartTv(XSSFSheet sheet, List<SmartTvReport> aList){
        int currentRow = 1;
        for(SmartTvReport i: aList){
            int currentColumn = 0;
            if(sheet.getRow(currentRow) == null){
                sheet.createRow(currentRow);
            }
            while(sheet.getRow(0).getCell(currentColumn) != null){
                if(sheet.getRow(currentRow).getCell(currentColumn) == null)
                    sheet.getRow(currentRow).createCell(currentColumn);
                switch(sheet.getRow(0).getCell(currentColumn).getStringCellValue()){
                    case "id":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getId());
                        break;
                    case "transactionId":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getTransactionId());
                        break;
                    case "userId":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getUserId());
                        break;
                    case "itemid":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getItemId());
                        break;
                    case "name":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getName());
                        break;
                    case "methodName":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getMethodName());
                        break;
                    case "platform":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getPlatform());
                        break;
                    case "deviceName":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getDeviceName());
                        break;
                    case "acticeCode":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getActiveCode());
                        break;
                    case "codePartner":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getCodePartner());
                        break;
                    case "groupContentId":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getGroupContentId());
                        break;
                    case "createdAt":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getStringCreatedAt());
                        break;
                    case "expiryDate":
                        sheet.getRow(currentRow).getCell(currentColumn).setCellValue(i.getStringExpiryDate());
                        break;
                }
                currentColumn++;
            }
            currentRow++;
        }
    }

    private static int generateFormat(int currentRow, int currentColumn, Map aMap, XSSFSheet sheet){
        for(Object key: aMap.keySet()){
            if(sheet.getRow(currentRow) == null)
                 sheet.createRow(currentRow);
            if(aMap.get(key) instanceof Map){
                int fromRow = currentRow;
                sheet.getRow(fromRow).createCell(currentColumn);
                sheet.getRow(fromRow).getCell(currentColumn).setCellValue(key.toString());
                currentRow = generateFormat(currentRow, currentColumn + 1, (Map) aMap.get(key), sheet);
                int endRow = currentRow -1;
                for(int i = fromRow + 1; i <= endRow; i++){
                    sheet.getRow(i).createCell(currentColumn);
                }
                sheet.addMergedRegion(new CellRangeAddress(fromRow,endRow,currentColumn,currentColumn));
            }
            else{
                Cell cell = sheet.getRow(currentRow).createCell(currentColumn);
                cell.setCellValue(key.toString());
                currentRow++;
            }
        }
        return currentRow;
    }

    private static int parseValue(int currentRow, int currentColumn, Map aMap, XSSFSheet sheet){
        for(int i = 0; i < aMap.size(); i++){
            String key = sheet.getRow(currentRow).getCell(currentColumn).getStringCellValue();
            if(aMap.get(key) instanceof Map){
                currentRow = parseValue(currentRow, currentColumn + 1, (Map) aMap.get(key), sheet);
            }
            else{
                Cell valueCell = sheet.getRow(currentRow).createCell(currentColumn + 1);
                valueCell.setCellValue(aMap.get(key).toString());
                currentRow++;
            }
        }
        return currentRow;
    }

    private static boolean checkCellEmpty(Cell cell){
        if(cell == null || (cell.getCellType() == Cell.CELL_TYPE_BLANK))
            return true;
        if(cell.getCellType() == Cell.CELL_TYPE_STRING && cell.getStringCellValue().isEmpty())
            return true;
        return false;
    }
}
