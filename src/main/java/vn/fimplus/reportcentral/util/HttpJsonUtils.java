package vn.fimplus.reportcentral.util;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class HttpJsonUtils {
	
	private static final Logger log = LoggerFactory.getLogger(HttpJsonUtils.class);
	
	
	// HTTP POST request
	public static JsonNode sendJsonRequest(String url, String json) {
		try {
			log.info("[HttpJsonUtils.sendJsonRequest] {}", url);
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpResponse response;
			HttpEntity entity;
			HttpPost httpRequest = new HttpPost(url);
			
			httpRequest.setHeader("Content-Type", "application/json");
			httpRequest.setEntity(new StringEntity(json));
			
			response = httpclient.execute(httpRequest);
			if (response.getStatusLine().getStatusCode() == 404)
				throw new Exception();
			entity = response.getEntity();
			InputStream inputStream = entity.getContent();
			String responseText = IOUtils.toString(inputStream);
			return JsonUtils.parseJson(responseText);
		} catch (Exception e) {
			log.error("[HttpJsonUtils.sendJsonRequest] ", e);
			return null;
		}
	}
}
