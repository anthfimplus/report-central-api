package vn.fimplus.reportcentral.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.text.DateFormatSymbols;


public class DateTimeUtils {
	
	public static final String DATE_FORMAT     = "yyyy-MM-dd";
	public static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	public static String toUTCString(DateTime input) {
		return input.withZone(DateTimeZone.UTC).toString(DATETIME_FORMAT);
	}
	
	public static DateTime getStartOfMonth(DateTime input) {
		return input.dayOfMonth().withMinimumValue().millisOfDay().withMinimumValue();
	}
	
	public static DateTime getEndOfMonth(DateTime input) {
		return input.dayOfMonth().withMaximumValue().millisOfDay().withMaximumValue();
	}

	public static String getMonthFromInt(int m){
		String month = "invalid";
		DateFormatSymbols dtf = new DateFormatSymbols();
		String[] months = dtf.getMonths();
		if(m >= 0 && m <= 11)
			month = months[m];
		return month;
	}
	
}
