package vn.fimplus.reportcentral.util;


import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TokenUtils{
	
	public static final String KEY_VALUE = "1af17e73721dbe0c40011b82ed4bb1a7dbe3ce29";

	
	public static String generateToken(Object... valueRequests) {
		StringBuilder valueRequest = new StringBuilder();
		for (Object value : valueRequests) {
			if (value != null) {
				valueRequest.append(value.toString());
			}
		}

		return generateHash(valueRequest.toString() + KEY_VALUE);
	}
	
	public static String generateHash(String input) {
		StringBuilder hash = new StringBuilder();
		
		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] hashedBytes = sha.digest(input.getBytes());
			return DatatypeConverter.printHexBinary(hashedBytes).toLowerCase();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return hash.toString();
	}
}
