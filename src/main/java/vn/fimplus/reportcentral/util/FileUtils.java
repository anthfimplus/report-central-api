package vn.fimplus.reportcentral.util;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import vn.fimplus.reportcentral.model.CsvExportData;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileUtils {
	
	public static boolean exportCSV(String path, List<CsvExportData> dataList) {
		try {
			FileWriter writer = new FileWriter(path);
			
			for (CsvExportData data : dataList)
				writer.append(data.toCSV());
			
			writer.flush();
			writer.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static XSSFSheet openFile(String fileName){
		XSSFSheet tempSheet = null;
		try {
			FileInputStream fis = new FileInputStream(fileName);
			XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
			tempSheet = myWorkBook.getSheetAt(0);
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tempSheet;
	}
}
