package vn.fimplus.reportcentral.util;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public class JsonUtils {
	
	public static JsonNode parseJson(final String jsonString) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonFactory jsonFactory = new JsonFactory(); // or, for data binding, org.codehaus.jackson.mapper.MappingJsonFactory
		JsonParser jp = jsonFactory.createParser(jsonString);
		return mapper.readTree(jp);
	}
	
	public static <T> T parseJson(final String jsonString, final TypeReference<T> typeReference, boolean skipUnknowFields) throws IOException {
		ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, !skipUnknowFields);
		JsonFactory jsonFactory = new JsonFactory(); // or, for data binding, org.codehaus.jackson.mapper.MappingJsonFactory
		JsonParser jp = jsonFactory.createParser(jsonString);
		return mapper.readValue(jp, typeReference);
	}
	
	public static <T> T parseJson(final String jsonString, final Class<T> clazz, boolean skipUnknowFields) throws IOException {
		ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, !skipUnknowFields);
		JsonFactory jsonFactory = new JsonFactory(); // or, for data binding, org.codehaus.jackson.mapper.MappingJsonFactory
		JsonParser jp = jsonFactory.createParser(jsonString);
		return mapper.readValue(jp, clazz);
	}
	
	public static String generateJson(Object object) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException ignored) {
		}
		return null;
	}
	
	
}
