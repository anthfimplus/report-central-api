package vn.fimplus.reportcentral.util;

import java.io.*;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.DataStoreCredentialRefreshListener;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.IOUtils;
import com.google.api.client.util.store.*;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;

public class TestGoogleApi {

    private static String directory = System.getProperty("user.dir");
    private static final String APPLICATION_NAME = "Google Sheets Example";
    private static Credential credential = null;
    private static final DataStoreFactory dataStoreFactory = new MemoryDataStoreFactory();
    private final static JsonFactory jsonFactory = new JacksonFactory();
    private static HttpTransport transport = null;
    private static void authorize() throws IOException, GeneralSecurityException {
         transport = GoogleNetHttpTransport.newTrustedTransport();
        // build GoogleClientSecrets from JSON file
        InputStream in = TestGoogleApi.class.getResourceAsStream("/credential.json");
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JacksonFactory.getDefaultInstance(), new InputStreamReader(in));


        List<String> scopes = Arrays.asList(SheetsScopes.SPREADSHEETS);

        // build Credential object
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(), clientSecrets, scopes).setDataStoreFactory(dataStoreFactory)
                .setAccessType("offline").build();
        credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        /*GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream( directory + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "opposervice.json"))
                .createScoped(scopes)
                .setAccessToken("new token");*/
        //StoredCredential storedCredential = StoredCredential.getDefaultDataStore(dataStoreFactory).get(credential.getServiceAccountId());

//        credential.setAccessToken(storedCredential.getAccessToken());
    }

    public static Sheets getSheetsService() throws IOException, GeneralSecurityException {
        if(credential == null)
            authorize();
        return new Sheets.Builder(transport, jsonFactory, credential).setApplicationName(APPLICATION_NAME).build();
    }
}
