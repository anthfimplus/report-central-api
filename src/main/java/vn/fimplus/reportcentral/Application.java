package vn.fimplus.reportcentral;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;
import vn.fimplus.reportcentral.controller.OppoProject;
import vn.fimplus.reportcentral.controller.TestController;

import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class Application implements WrapperListener {

	private static Logger log = Logger.getLogger(Application.class);
	
	public static void main(String[] args) {


		WrapperManager.start(new Application(), args);

	}
	
	@Override
	public Integer start(String[] strings) {
		log.info("////////    Starting >>>>>>>>>>>>");
		try {
			SpringApplication.run(Application.class);
			log.info("////////////////////////////////////////////////////");
			log.info("////////    SmartTvReport Central Service Ready    ////////");
			log.info("////////////////////////////////////////////////////");
		} catch (Exception e) {
			log.error("Error starting application", e);
			System.exit(1);
		}
		return null;
	}
	
	@Override
	public int stop(int i) {
		return 0;
	}
	
	@Override
	public void controlEvent(int i) {
	
	}
}
