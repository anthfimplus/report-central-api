package vn.fimplus.reportcentral.service;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.report.User;

import vn.fimplus.reportcentral.repository.TransactionRepository;
import vn.fimplus.reportcentral.repository.UserReportRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
public class UserReportService extends AbstractService{
    @Autowired
    UserReportRepository userReportRepository;
    @Autowired
    TransactionRepository transactionRepository;

    public List<User> getUserById(List<String> userIdList){
        List<User> result = new ArrayList<>();
        for(String userId : userIdList){
            result.add(userReportRepository.getUserById(userId));
        }
        return result;
    }

    public Map<String, String> getPackageName(XSSFSheet mySheet){
        int currentRow = 1;
        Map<String, String> result = new TreeMap<>();
        boolean cellEmpty = false;
        int count = 0;
        while(mySheet.getRow(currentRow) != null && !cellEmpty) {
            Row row = mySheet.getRow(currentRow);
            List<String> userDetail = new ArrayList<>();
            for (int currentColumn = 0; currentColumn < 2; currentColumn++) {
                Cell cell = row.getCell(currentColumn);
                String value = "";
                if(currentColumn == 0){
                    try {
                        value = cell.getStringCellValue();
                    } catch (Exception e) {
                        cellEmpty = true;
                    }
                    userDetail.add(value);
                }
                else if(currentColumn == 1){
                    userDetail.add(new DateTime(cell.getDateCellValue()).plusHours(7).toString("yyyy-MM-dd HH:mm:ss"));
                }
            }
            if(!cellEmpty)
                result.put(userDetail.get(0), transactionRepository.getPackageName(userDetail.get(0), userDetail.get(1)));
            System.out.println("Count: " + count);
            currentRow++;
            count++;
        }
        return result;
    }
}
