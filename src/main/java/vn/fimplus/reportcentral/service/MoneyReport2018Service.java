package vn.fimplus.reportcentral.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.report.MoneyReport2018.SvodReport;
import vn.fimplus.reportcentral.model.report.MoneyReport2018.TvodReport;
import vn.fimplus.reportcentral.model.report.User;
import vn.fimplus.reportcentral.repository.MoneyReport2018Resposistory;

import java.util.*;

@Service
public class MoneyReport2018Service {

    @Autowired
    MoneyReport2018Resposistory moneyReport2018Resposistory;
    @Autowired
    UserReportService userReportService;

    protected final Log logger = LogFactory.getLog(getClass());

    public List<String> test(DateTime dateFrom, DateTime dateTo){
        List<TvodReport> report = moneyReport2018Resposistory.tvod(dateFrom, dateTo);
        List<String> result = new ArrayList<>();
        for(TvodReport t : report){
            if(t.getPlatform().equals("unidentify")){
                result.add(t.getId());
            }
            else if(t.getPlatform().equals("")){
                result.add(t.getId());
            }
        }
        return result;
    }

    public Map<String, Map<String, Integer>> periodic(ArrayList dateList, String apiType){
        Map<String, Map<String, Integer>> result = new TreeMap<>();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
        if(apiType.equals("tvod")){
            for(int i = 0; i < dateList.size(); i++){
                ArrayList temp = (ArrayList) dateList.get(i);
                DateTime fromDate = new DateTime();
                DateTime toDate = new DateTime();
                try {
                    fromDate = dtf.parseDateTime(temp.get(0).toString()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
                    toDate = dtf.parseDateTime(temp.get(1).toString()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                List<TvodReport> report = moneyReport2018Resposistory.tvod(fromDate, toDate);
                Map<String, Map<String, Map<String, Integer>>> queryResult = new TreeMap<>();
                if(!report.isEmpty()){
                    queryResult.put("TVOD", getTvodPlatformReport(report));
                }
                Map<String, Integer> tempResult = new HashMap<>();
                for(Object key : queryResult.get("TVOD").keySet()){
                    if(key != null && key != "")
                        tempResult.put((String)key, queryResult.get("TVOD").get(key).get("reportCount"));
                }
                result.put(toDate.toString("yyyy-MM"), tempResult);
            }
        }
        return result;
    }

    public Map<String, Object> getTvodReport(DateTime dateFrom, DateTime dateTo){
        List<TvodReport> report = moneyReport2018Resposistory.tvod(dateFrom, dateTo);
        Map<String, Object> result = new TreeMap<>();
        List<String> idList = new ArrayList<>();
        if(!report.isEmpty()){
            for(TvodReport i : report){
                idList.add(i.getUserId());
            }
            List<User> userList = userReportService.getUserById(idList);
            for(int i = 0; i < idList.size(); i++){
                report.get(i).setUserFullName(userList.get(i).getFullName());
                report.get(i).setUserEmail(userList.get(i).getEmail());
            }
            result.put("TVOD", getTvodPlatformReport(report));
            result.put("detailReport", report);
        }
        return result;
    }

    public List<String> test(DateTime dateIn){
        List<SvodReport> report = moneyReport2018Resposistory.svod(dateIn);
        System.out.println(report.toString());
        List<String> result = new ArrayList<>();
        if(!report.isEmpty()){
            result = test(report);
        }
        return result;
    }

    public Map<String, Object> getSvodReport(DateTime dateIn){
        List<SvodReport> report = moneyReport2018Resposistory.svod(dateIn);
        System.out.println(report.toString());
        Map<String, Object> result = new TreeMap<>();
        if(!report.isEmpty()){
            result.put("SVOD", getSvodPlatformReport(report));
            //result.put("detailReport", report);
        }
        return result;
    }

    private Map<String, Map<String, Integer>> uniqueUser(List<TvodReport> list){
        Map<String, Map<String, Integer>> result = new HashMap<>();
        for(int i = 0; i < list.size(); i++){
            String platform = list.get(i).getPlatform();
            String id = list.get(i).getUserId();
            if(!result.containsKey(platform)){
                result.put(platform, new HashMap<>());
            }
            if(!result.get(platform).containsKey(id))
                result.get(platform).put(id, 1);
            else
                result.get(platform).put(id, result.get(platform).get(id) + 1);

        }
        return result;
    }

    private Map<String, Map<String, Integer>> getTvodPlatformReport(List<TvodReport> list) {
        Map<String, Map<String, Integer>> result = new HashMap<>();
        Map<String, Map<String, Integer>> uniqueUser = uniqueUser(list);
        for (int i = 0; i < list.size(); i++) {
            String platform = list.get(i).getPlatform();
            if(platform == null)
                platform = "need checking";
            if(platform != null && platform != "") {
                if (!result.containsKey(platform)) {
                    Map<String, Integer> count = new HashMap<>();
                    count.put("userCount", uniqueUser.get(platform).size());
                    count.put("reportCount", 1);
                    result.put(platform, count);
                } else {
                    result.get(platform).put("reportCount", result.get(platform).get("reportCount") + 1);
                }
            }
        }
        return result;
    }

    private List<String> test(List<SvodReport> list){
        List<String> transId = new ArrayList<>();
        for(int i = 0; i < list.size(); i++) {
            String platform = list.get(i).getPlatform();
            if (platform == null) {
                transId.add(list.get(i).getTransactionId());
            }
        }
        return transId;
    }

    private Map<String, Map<String, Integer>> getSvodPlatformReport(List<SvodReport> list){
        Map<String, Map<String, Integer>> result = new HashMap<>();
        for(int i = 0; i < list.size(); i++) {
            String platform = list.get(i).getPlatform();
            if (platform == null){
                platform = "need Checking";
            }
            if (!result.containsKey(platform)) {
                Map<String, Integer> count = new HashMap<>();
                count.put("Bundling", 0);
                count.put("Independent", 0);
                result.put(platform, count);
                if (list.get(i).isBundling())
                    result.get(platform).put("Bundling", result.get(platform).get("Bundling") + 1);
                else
                    result.get(platform).put("Independent", result.get(platform).get("Independent") + 1);
            } else {
                if (list.get(i).isBundling())
                    result.get(platform).put("Bundling", result.get(platform).get("Bundling") + 1);
                else
                    result.get(platform).put("Independent", result.get(platform).get("Independent") + 1);
            }
        }
        for(String key : result.keySet()){
            result.get(key).put("TOTAL", result.get(key).get("Bundling") + result.get(key).get("Independent"));
        }
        return result;
    }
}
