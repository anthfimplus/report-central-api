package vn.fimplus.reportcentral.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.CsvExportData;
import vn.fimplus.reportcentral.model.ReportTimeRangeParam;
import vn.fimplus.reportcentral.model.report.TransactionReport;
import vn.fimplus.reportcentral.model.report.svod.SvodReport;
import vn.fimplus.reportcentral.model.report.svod.Trial;
import vn.fimplus.reportcentral.repository.TransactionRepository;
import vn.fimplus.reportcentral.util.DateTimeUtils;
import vn.fimplus.reportcentral.util.FileUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SubscriptionReportService extends AbstractService {
	
	
	@Autowired
	TransactionRepository transactionRepository;
	
	public SvodReport reportSubscription(DateTime from, DateTime to, int cycleType) {
		long timeStart = System.currentTimeMillis();
		
		List<TransactionReport> transactionData = getSubscriptionTransactionData(from, to);
		logger.info("transactionSize: " + transactionData.size());
		
		ReportTimeRangeParam timeRangeParam = new ReportTimeRangeParam(from, to, cycleType);
		
		SvodReport svodReport = new SvodReport(timeRangeParam.getCycleList());
		Map<String, List<TransactionReport>> usersTrialTrans = new HashMap<>();
		Map<String, List<TransactionReport>> usersAutoTrans = new HashMap<>();
		Map<String, List<TransactionReport>> usersManualTrans = new HashMap<>();
		Map<String, List<TransactionReport>> usersMBFTrans = new HashMap<>();
		for (TransactionReport tran : transactionData) {
			Map<String, List<TransactionReport>> mapToPut = null;
			
			switch (tran.getMethodName()) {
				case "Credit Card":
					if (tran.getItemId().toLowerCase().contains("trial")) {
						mapToPut = usersTrialTrans;
						break;
					}
				case "MOMO":
					mapToPut = usersAutoTrans;
					break;
				case "MOBIFONE":
					mapToPut = usersMBFTrans;
					break;
				default:
					mapToPut = usersManualTrans;
					break;
			}
			
			// AND t.methodName = 'Credit Card' AND t.itemId LIKE '%TRIAL%' -- trial
			// AND t.methodName IN ('Credit Card', 'MOMO') AND t.itemId NOT LIKE '%TRIAL%' -- Auto Renew
			// AND t.methodName NOT IN ('Credit Card', 'MOMO', 'MOBIFONE')  -- Non-Auto Renew
			// AND t.methodName IN ('MOBIFONE')  -- MBF
			
			
			List<TransactionReport> userTransactionList = mapToPut.get(tran.getUserId());
			if (userTransactionList == null) {
				userTransactionList = new ArrayList<>();
				mapToPut.put(tran.getUserId(), userTransactionList);
			}
			userTransactionList.add(tran);
		}
		List<CsvExportData> data = new ArrayList<>();
		for (Map.Entry<String, List<TransactionReport>> entry : usersTrialTrans.entrySet()) {
			data.addAll(entry.getValue());
		}
		FileUtils.exportCSV("/Users/antran/Work/SmartTvReport/svod/trial.csv", data);
		
		Map<String, Trial> trialMap = calcTrial(usersTrialTrans, timeRangeParam);
		
		for (Map.Entry<String, Trial> trialEntry : trialMap.entrySet()) {
			svodReport.putTrial(trialEntry.getKey(), trialEntry.getValue());
		}
		
		
		data = new ArrayList<>();
		for (Map.Entry<String, List<TransactionReport>> entry : usersAutoTrans.entrySet()) {
			data.addAll(entry.getValue());
		}
		FileUtils.exportCSV("/Users/antran/Work/SmartTvReport/svod/auto.csv", data);
		data = new ArrayList<>();
		for (Map.Entry<String, List<TransactionReport>> entry : usersManualTrans.entrySet()) {
			data.addAll(entry.getValue());
		}
		FileUtils.exportCSV("/Users/antran/Work/SmartTvReport/svod/manual.csv", data);
		data = new ArrayList<>();
		for (Map.Entry<String, List<TransactionReport>> entry : usersMBFTrans.entrySet()) {
			data.addAll(entry.getValue());
		}
		FileUtils.exportCSV("/Users/antran/Work/SmartTvReport/svod/mbf.csv", data);

//		Map<String, Map<String, Object>> result = new TreeMap<>();
//		int monthDiff = Months.monthsBetween(from, to).getMonths();
//		for (int i = 0; i <= monthDiff; i++) {
//
//			DateTime currentCheck = from.plusMonths(i);
//			DateTime startOfMonth = DateTimeUtils.getStartOfMonth(currentCheck);
//			DateTime endOfMonth = DateTimeUtils.getEndOfMonth(currentCheck);
//
//			Map<String, Object> monthResult = new HashMap<>();
//			result.put(startOfMonth.toString("yyyy-MM"), monthResult);
//
//			Map<String, Object> opening = this.getOpening(packageCode, startOfMonth);
//			monthResult.putAll(opening);
//
//		}
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[TransactionReportService.reportSubscription] from=" + from.toString(DateTimeUtils.DATETIME_FORMAT) + ", to=" + to.toString(DateTimeUtils.DATETIME_FORMAT) + " DONE. duration=" + (timeEnd - timeStart));
		return svodReport;
	}
	
	private List<TransactionReport> getSubscriptionTransactionData(DateTime from, DateTime to) {
		long timeStart = System.currentTimeMillis();
		List<TransactionReport> allTrans = transactionRepository.getPartnerSVODData(from, to);
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[TransactionReportService.getSubscriptionTransactionData] " + from.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return allTrans;
	}
	
	private Map<String, Trial> calcTrial(Map<String, List<TransactionReport>> usersTrialTrans, ReportTimeRangeParam timeRangeParam) {
		
		// Get check if users had trans before start time
		List<String> userHadTransBefore = transactionRepository.checkPrevSVOD(timeRangeParam.getFrom(), TransactionRepository.SVOD_TYPE_TRIAL, new ArrayList<>(usersTrialTrans.keySet()));
		
		Map<String, List<TransactionReport>> trialFirstTime = new HashMap<>();
		Map<String, List<TransactionReport>> trialExpired = new HashMap<>();
		
		for (Map.Entry<String, List<TransactionReport>> userEntry : usersTrialTrans.entrySet()) {
			boolean checkedFirstTime = false;
			for (TransactionReport entry : userEntry.getValue()) {
				// check if this user didn't have transaction before => firsttime
				if (!checkedFirstTime && !userHadTransBefore.contains(entry.getUserId())) {
					checkedFirstTime = true;
					addToMap(trialFirstTime, timeRangeParam.getCycleKey(entry.getExpiryDate()), entry);
					userHadTransBefore.add(entry.getUserId());
				} else {
					checkedFirstTime = true;
				}
				// check expired trans
				if (entry.getExpiryDate() != null && timeRangeParam.isInRange(entry.getExpiryDate()))
					addToMap(trialExpired, timeRangeParam.getCycleKey(entry.getExpiryDate()), entry);
			}
		}
		
		Map<String, Trial> result = new HashMap<>();
		for (Map.Entry<String, List<TransactionReport>> userEntry : trialFirstTime.entrySet()) {
			Trial trial = result.get(userEntry.getKey());
			if (trial == null)
				trial = new Trial();
			trial.setNewTrial(userEntry.getValue().size());
			result.put(userEntry.getKey(), trial);
		}
		for (Map.Entry<String, List<TransactionReport>> userEntry : trialExpired.entrySet()) {
			Trial trial = result.get(userEntry.getKey());
			if (trial == null)
				trial = new Trial();
			trial.setExpired(userEntry.getValue().size());
			result.put(userEntry.getKey(), trial);
		}
		return result;
	}
	
	private <K> void addToMap(Map<String, List<K>> mapToAdd, String key, K object) {
		List<K> k = mapToAdd.get(key);
		if (k == null) {
			k = new ArrayList<>();
			mapToAdd.put(key, k);
		}
		k.add(object);
	}
}
