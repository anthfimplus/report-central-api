package vn.fimplus.reportcentral.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.constant.SystemConstant;
import vn.fimplus.reportcentral.model.common.Common_Permission;
import vn.fimplus.reportcentral.model.common.Common_User;
import vn.fimplus.reportcentral.repository.common.Common_PermissionRepository;
import vn.fimplus.reportcentral.repository.common.Common_UserRepository;
import vn.fimplus.reportcentral.util.TokenUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AccountService {
	
	protected final      Log    logger = LogFactory.getLog(getClass());
	private static final String HASH   = "p878x5bwgyYB";
	
	@Autowired
	private Common_UserRepository       userRepository;
	@Autowired
	private Common_PermissionRepository permissionRepository;
	
	private Map<String, Common_User>        onlineUsers;
	private Map<String, String>             tokenMaps;
	private Map<Integer, Common_Permission> permissionMap;
	
	@PostConstruct
	public void Init() {
		logger.info(">> Init AccountService");
		if (onlineUsers == null)
			onlineUsers = new HashMap<>();
		if (tokenMaps == null)
			tokenMaps = new HashMap<>();
		loadPermissions();
	}
	
	public void loadPermissions() {
		permissionMap = new HashMap<>();
		List<Common_Permission> permissionList = permissionRepository.getAllPermissions();
		for (Common_Permission permission : permissionList) {
			permissionMap.put(permission.getPermissionId(), permission);
		}
	}
	
	public Common_User login(String username, String password) {
		Common_User user = getUserByAuthentication(username, password);

		if (user != null) {
			Common_User onlineUser = onlineUsers.get(username);
			String token = TokenUtils.generateToken(username, password, String.valueOf((new DateTime()).getMillis()));
			user.setToken(token);
			user.setTokenExpireDate((new DateTime()).plusMinutes(15));
			onlineUsers.put(username, user);
			tokenMaps.put(token, username);
			if(onlineUser!=null) // remove expired session token
				tokenMaps.remove(onlineUser.getToken());
			logger.info("[Login_Success] username=" + user.getUsername() + ", token=" + token);
			return user;
		}
		return null;
	}
	
	public void logout(String token) {
		String username = tokenMaps.get(token);
		tokenMaps.remove(token);
		onlineUsers.remove(username);
		logger.info("[Logout_Success] username=" + username);
	}
	
	public Common_User getUserByToken(String token) {
		String username = tokenMaps.get(token);
		return username == null ? null : getUser(username);
	}
	
	public Common_User getUser(String username) {
		return onlineUsers.get(username);
	}
	
	public boolean verifyAuthentication(HttpServletRequest request, Object... valueRequests) {
		String userToken = request.getHeader(SystemConstant.USER_TOKEN);
		String validateToken = request.getHeader(SystemConstant.HASH);
		logger.info("usertoken: " + userToken + " validateToken" + validateToken);
		if (userToken == null || validateToken == null)
			return false;
		//return true;
		return verifyAuthentication(userToken, validateToken, valueRequests);
	}
	
	public boolean verifyAuthentication(String userToken, String validateToken, Object... valueRequests) {
		Common_User user = getUserByToken(userToken);
		if (user != null) {
			if (user.getTokenExpireDate().compareTo(new DateTime()) < 0)
				return false;
			List<String> valueList = new ArrayList<>();
			for (int i = 0; i < valueRequests.length; i++) {
				valueList.add(valueRequests[i].toString());
			}

			valueList.add(user.getToken());
			String token = TokenUtils.generateToken(valueList.toArray());
			if (token.equals(validateToken)) {
				user.setTokenExpireDate((new DateTime()).plusMinutes(15));
				return true;
			} else {
				logger.info("[AccountService] Token verify fail. username=" + user.getUsername() + ", userToken=" + userToken + ", validateToken=" + validateToken + ", values=" + valueList.toString());
			}
		}
		return false;
	}
	
	private Common_User getUserByAuthentication(String username, String password) {
		Common_User user = userRepository.findUserByUsername(username);
		String hashPassword = TokenUtils.generateHash(password + HASH);
		if (user != null && user.getPassword().equals(hashPassword)) {
//			Apis_Group group = groupRepository.findById(result.getGroupId());
//			if (group != null)
//				result.setGroupName(group.getGroupName());
//			String permissionString = getPermissionsStringLogin(result.getUserId(), result.getGroupId());
//			result.setPermissions(permissionString);
			return user;
		}
		return null;
	}

//	public String updateUserInfo(String username, String password, String email) {
//		Common_User user = getUserByAuthentication(username, password);
//		if (user != null) {
//			user.setEmail(email);
//			user.setUpdateBy(username);
//			user.setUpdateTime(new Date());
//			try {
//				userRepository.update(user);
//				log.info("UPDATE_USER_INFO_SUCCESS username=" + user.getUsername());
//				return ApiReturnConstant.STATUS.SUCCESS;
//			} catch (Exception e) {
//				log.error(">>>>>>>>>> UPDATE_USER_INFO_ERROR \n", e);
//				return ApiReturnConstant.STATUS.ERROR;
//			}
//		}
//		return ApiReturnConstant.STATUS.WRONG_PASSWORD;
//	}

//	public String updateUserPassword(String username, String oldPassword, String newPassword) {
//		Common_User user = getUserByAuthentication(username, oldPassword);
//		if (user != null) {
//			String newPasswordEncrypted = Utility.md5(newPassword + AUTHENTICATE_CODE);
//			user.setPassword(newPasswordEncrypted);
//			user.setUpdateBy(username);
//			user.setUpdateTime(new Date());
//			user.setFirstLogin(false);
//			try {
//				userRepository.update(user);
//				log.info("UPDATE_USER_PASSWORD_SUCCESS username=" + user.getUsername());
//				return ApiReturnConstant.STATUS.SUCCESS;
//			} catch (Exception e) {
//				log.error(">>>>>>>>>> UPDATE_USER_PASSWORD_ERROR \n", e);
//				return ApiReturnConstant.STATUS.ERROR;
//			}
//		}
//		return ApiReturnConstant.STATUS.WRONG_PASSWORD;
//	}
}
