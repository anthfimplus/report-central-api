package vn.fimplus.reportcentral.service;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.report.User;
import vn.fimplus.reportcentral.repository.ElasticSearchRepository;
import vn.fimplus.reportcentral.repository.UserReportRepository;
import vn.fimplus.reportcentral.util.ExcelExport;
import vn.fimplus.reportcentral.util.HttpJsonUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class ElasticSearch {

    @Autowired
    ElasticSearchRepository elasticSearchRepository;
    @Autowired
    UserReportRepository userReportRepository;

    public void getUniqueUserByGenre(String genre){
        Settings settings = ImmutableSettings.builder()
                .put("cluster.name", "fimplus-log-center2")
                .put("client.transport.sniffOnStart", true)
                .put("client.transport.keepAlive", true)
                .put("client.transport.maxRetries", 10)
                .build();
        Client client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.93", 9300))
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.95", 9300));
        DateTime enddate = DateTime.now().minusMonths(1).dayOfMonth().withMaximumValue();
        DateTime startDate = new DateTime().withDate(2016, 2, 1);
        List<DateTime> months = new ArrayList<>();
        while(startDate.getMillis() < enddate.getMillis()) {
            months.add(startDate);
            startDate = startDate.plusMonths(1);
        }

        for(DateTime month : months){
            DateTime firstDay = month.dayOfMonth().withMinimumValue().hourOfDay().withMinimumValue().minuteOfHour().withMinimumValue().secondOfMinute().withMinimumValue();
            DateTime lastDay = month.dayOfMonth().withMaximumValue().hourOfDay().withMaximumValue().minuteOfHour().withMaximumValue().secondOfMinute().withMaximumValue();
            DateTime currentDay = firstDay;
            String[] list = new String[lastDay.getDayOfMonth()];
            for(int i = 0; i < lastDay.getDayOfMonth(); i++){
                list[i] = "log-nginx-" + currentDay.plusDays(i).toString("yyyy.MM.dd");
            }
            List<String> result = elasticSearchRepository.getUniqueUserByGenre(client, list, firstDay.getMillis(), lastDay.getMillis(),genre);
            HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/_cache/clear", "");
            ExcelExport.singleExport(result, genre);
        }
        client.close();
    }

    public Map<String, Integer> getTimeOnViewByUser(XSSFSheet mySheet, String reportName){
        int currentRow = 1;
        boolean cellEmpty = false;
        List<String> users = new ArrayList<>();
        while(mySheet.getRow(currentRow) != null && !cellEmpty){
            Row row = mySheet.getRow(currentRow);
            Cell cell = row.getCell(0);
            try {
                users.add(cell.getStringCellValue());
            } catch (Exception e) {
                cellEmpty = true;
            }
            currentRow++;
        }

        Map<String, Integer> result = new TreeMap<>();
        Settings settings = ImmutableSettings.builder()
                .put("cluster.name", "fimplus-log-center2")
                .put("client.transport.sniffOnStart", true)
                .put("client.transport.keepAlive", true)
                .put("client.transport.maxRetries", 10)
                .build();
        Client client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.93", 9300))
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.95", 9300));

        for(String aUser : users){
            Map<String, Integer> exportResult = new TreeMap<>();
            User user = userReportRepository.getUserById(aUser);
            DateTime enddate = DateTime.now().minusMonths(1).dayOfMonth().withMaximumValue();
            DateTime startDate = new DateTime(user.getCreateAt());
            DateTime checkDate = new DateTime().withDate(2017,03,06);
            if(startDate.getMillis() < checkDate.getMillis())
                startDate = checkDate;
            List<DateTime> months = new ArrayList<>();
            while(startDate.getMillis() < enddate.getMillis()){
                months.add(startDate);
                startDate = startDate.plusMonths(1);
            }

            for(DateTime month : months){
                DateTime firstDay = month.dayOfMonth().withMinimumValue().hourOfDay().withMinimumValue().minuteOfHour().withMinimumValue().secondOfMinute().withMinimumValue();
                DateTime lastDay = month.dayOfMonth().withMaximumValue().hourOfDay().withMaximumValue().minuteOfHour().withMaximumValue().secondOfMinute().withMaximumValue();
                DateTime currentDay = firstDay;
                String[] list = new String[lastDay.getDayOfMonth()];
                for(int i = 0; i < lastDay.getDayOfMonth(); i++){
                    list[i] = "log-nginx-" + currentDay.plusDays(i).toString("yyyy.MM.dd");
                }

                int watchTime = elasticSearchRepository.getTimeStampById(client, aUser, list, firstDay.getMillis(), lastDay.getMillis(), "movieID");

                if(result.get(aUser) == null)
                    result.put(aUser, watchTime);
                else
                    result.put(aUser, result.get(aUser) + watchTime);
            }
            exportResult.put(aUser, result.get(aUser));
            HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/_cache/clear", "");
            ExcelExport.genericExport(exportResult, reportName);
            System.out.println("User clear: " + user);
        }
        client.close();
        return result;
    }


    public Map<String, Map<String, Map<String, Integer>>> getTotalActivityReport(XSSFSheet mySheet, String type){

        DateTime date = new DateTime();

        Settings settings = ImmutableSettings.builder()
                .put("cluster.name", "fimplus-log-center2")
                .put("client.transport.ignore_cluster_name",true)
                .put("client.transport.sniff", true)
                .put("client.transport.sniffOnConnectionFault", true)
                .put("client.transport.keepAlive", true)
                .put("client.transport.maxRetries", 10)
                .build();

        Client client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.93", 9300))
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.95", 9300));

        Iterator<Row> rowIterator = mySheet.iterator();

        Map<String, Map<String, Map<String, Integer>>> result = new TreeMap<>();

        while(rowIterator.hasNext()) {
            Map<String, Map<String, Map<String, Integer>>> result1 = new TreeMap<>();
            Row row = rowIterator.next();
            List<String> movieDetail = new ArrayList<>();
            if (row.getRowNum() == 0)
                row = rowIterator.next();
            int currentColumn = 0;
            for (; currentColumn < 2; currentColumn++) {
                Cell cell = row.getCell(currentColumn);
                switch (cell.getColumnIndex()) {
                    case 0:
                        String value;
                        try {
                            value = cell.getStringCellValue();
                        } catch (Exception e) {
                            value = cell.getNumericCellValue() + "";
                        }
                        movieDetail.add(value);
                        break;
                    case 1:
                        movieDetail.add(new DateTime(cell.getDateCellValue()).toString("yyyy-MM-dd"));
                        break;
                }
            }
            org.joda.time.format.DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
            DateTime currentDate = dtf.parseDateTime("2017-01-01");
            DateTime endDate = dtf.parseDateTime("2018-05-31");
            Long start = currentDate.getMillis();
            Long end = endDate.getMillis();
            Long dateBetween = TimeUnit.MILLISECONDS.toDays(end - start);
            String[] list = new String[dateBetween.intValue()];
            for(int i = 0; i < dateBetween.intValue(); i++){
                currentDate.plusDays(i);
                list[i] = "log-nginx-" + currentDate.plusDays(i).toString("yyyy.MM.dd");
            }

            Map<String, Map<String, Integer>> a90Basic = elasticSearchRepository.getBasicPackage(client, movieDetail.get(0), list, start, end, type);

            Map<String, Map<String, Integer>> a90Premium = elasticSearchRepository.getpremiumPackage(client, movieDetail.get(0), list, start, end, type);

            Map<String, Map<String, Integer>> temp = result1.get(movieDetail.get(0));
            if(temp == null) {
                temp = new TreeMap<>();
                result1.put(movieDetail.get(0), temp);
            }
            temp.putAll(a90Basic);
            temp.putAll(a90Premium);
            result.putAll(result1);
            System.out.println(movieDetail + " finish");
            ExcelExport.exportExcelElastic(result1, "total-" + date.toString("yyyyMMdd"));
        }
        try {
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        client.close();
        return result;
    }


    public Map<String, Map<String, Map<String, Integer>>> getReport(XSSFSheet mySheet, String type){
        DateTime date = new DateTime();

        Settings settings = ImmutableSettings.builder()
                .put("cluster.name", "fimplus-log-center2")
                .put("client.transport.sniffOnStart", true)
                .put("client.transport.keepAlive", true)
                .put("client.transport.maxRetries", 10)
                .build();

        Client client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.93", 9300))
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.95", 9300));

        Iterator<Row> rowIterator = mySheet.iterator();

        Map<String, Map<String, Map<String, Integer>>> result = new TreeMap<>();

        while(rowIterator.hasNext()) {
            Map<String, Map<String, Map<String, Integer>>> result1 = new TreeMap<>();
            Row row = rowIterator.next();
            List<String> movieDetail = new ArrayList<>();
            if (row.getRowNum() == 0)
                row = rowIterator.next();
            int currentColumn = 0;
            for (; currentColumn < 3; currentColumn++) {
                Cell cell = row.getCell(currentColumn);
                String value;
                switch (cell.getColumnIndex()) {
                    case 0:
                        try {
                            value = cell.getStringCellValue();
                        } catch (Exception e) {
                            value = cell.getNumericCellValue() + "";
                        }
                        movieDetail.add(value);
                        break;
                    case 1:
                        movieDetail.add(new DateTime(cell.getDateCellValue()).toString("yyyy-MM-dd"));
                        break;
                    case 2:
                        try{
                            value = cell.getStringCellValue();
                        } catch(Exception e){
                            value = cell.getNumericCellValue() + "";
                        }
                        movieDetail.add(value);
                        break;
                }
            }
            org.joda.time.format.DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
            DateTime currentDate = dtf.parseDateTime(movieDetail.get(1));
            DateTime endDate = currentDate.plusDays(90);
            DateTime lastDate = dtf.parseDateTime("2018-06-20");
            if (endDate.compareTo(lastDate) == 1)
                endDate = lastDate;
            int daysBetween = Days.daysBetween(currentDate.withTimeAtStartOfDay(), endDate.withTimeAtStartOfDay()).getDays();
            if (daysBetween >= 0) {
                String[] list = new String[daysBetween];
                Long start = currentDate.getMillis();
                Long end = endDate.getMillis();
                for (int i = 0; i < daysBetween; i++) {
                    list[i] = "log-nginx-" + currentDate.plusDays(i).toString("yyyy.MM.dd");
                }

                Map<String, Map<String, Integer>> a90Basic = elasticSearchRepository.getBasicPackage(client, movieDetail.get(0), list, start, end, type);

                Map<String, Map<String, Integer>> a90Premium = elasticSearchRepository.getpremiumPackage(client, movieDetail.get(0), list, start, end, type);

                Map<String, Map<String, Integer>> a90total = elasticSearchRepository.getA90(client, list, start, end);

                Map<String, Map<String, Integer>> temp = result1.get(movieDetail.get(0));
                if (temp == null) {
                    temp = new TreeMap<>();
                    result1.put(movieDetail.get(2), temp);
                }
                temp.putAll(a90Basic);
                temp.putAll(a90Premium);
                temp.putAll(a90total);
                result.putAll(result1);
                System.out.println(movieDetail + " finish");
                ExcelExport.exportExcelElastic(result1, "a90-" + date.toString("yyyyMMdd"));
            }
            try {
                Thread.sleep(15000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        client.close();
        return result;
    }

    public List<String> getA30Report(List<DateTime> dateList){
        Settings settings = ImmutableSettings.builder()
                .put("cluster.name", "fimplus-log-center2")
                .put("client.transport.sniffOnStart", true)
                .put("client.transport.keepAlive", true)
                .put("client.transport.maxRetries", 10)
                .build();

        Client client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.93", 9300))
                .addTransportAddress(new InetSocketTransportAddress("10.10.14.95", 9300));

        List<String> result = new ArrayList<>();

        for(DateTime endDate : dateList){
            DateTime startDate = endDate.withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).plusDays(-30);
            int daysBetween = Days.daysBetween(startDate.withTimeAtStartOfDay(), endDate.withTimeAtStartOfDay()).getDays();
            if (daysBetween >= 0) {
                String[] list = new String[daysBetween];
                Long start = startDate.getMillis();
                Long end = endDate.plusDays(1).getMillis();
                for (int i = 0; i < daysBetween; i++) {
                    list[i] = "log-nginx-" + startDate.plusDays(i).toString("yyyy.MM.dd");
                }
                int a30 = elasticSearchRepository.getA30(client, list, start, end);
                result.add(endDate.toString("yyyy-MM-dd") + ", " + a30);
            }
        }
        return result;
    }
}
