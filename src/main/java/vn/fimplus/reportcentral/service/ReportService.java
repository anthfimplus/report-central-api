package vn.fimplus.reportcentral.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.report.TransactionReport;
import vn.fimplus.reportcentral.model.report.UserRegisterReport;
import vn.fimplus.reportcentral.repository.TransactionRepository;

import java.util.*;

@Service
public class ReportService extends AbstractService {
	
	@Autowired
	TransactionRepository transactionRepository;
	
	@Autowired
	@Qualifier("reportJDBC")
	private JdbcTemplate jdbc;
	
	public Object reportFTP(DateTime fromTime, DateTime toTime) {
//		result =this.transactionFTP(fromTime, toTime);
		
		this.logger.info("[ReportService.reportFTP] Done  #{startDate} - #{endDate}" + " ReportService" + " success");
		return null;
	}
	
	public Object reportDaily(DateTime fromTime, DateTime toTime) {
		
		List<UserRegisterReport> userReport = this.getUserReport(fromTime.toDate(), toTime.toDate());
		List<TransactionReport> transactionReport = this.getTransactionReport(fromTime.toDate(), toTime.toDate());
		Map result = new HashMap<String, Object>();
		result.put("userReport", userReport);
		result.put("transactionReport", transactionReport);
		return result;
	}
	
	public List<UserRegisterReport> getUserReport(Date startDate, Date endDate) {
//		header = ["CreatedAt","UserId","FullName","Email","pendingEmail","Mobile","DeviceName","Status","Platform","Channel"];
//		query = "SELECT DATE_FORMAT(ADDDATE(u.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS `createdAt`, u.id, u.fullName, u.email, u.pendingEmail, concat("'", l.mobile) AS `mobile`, m.type, IF(u.isBanned = 0, 'Normal', IF(u.isBanned = 2, 'INREVIEW', 'BANNED')) AS `status`, m.type_platform, IF(l.mobile IS NULL, 'Facebook', 'Mobile Phone') AS `channel` FROM hd1cas_db.User u LEFT JOIN hd1cas_db.LocalAccount l ON l.id = u.localAccId LEFT JOIN hd1cas_db.Device d ON d.userId = u.id LEFT JOIN hd1cas_db.map_platform m ON m.platform = d.platform WHERE u.createdAt > '#{startDate}' AND u.createdAt <= '#{endDate}' GROUP BY u.id, d.userId ORDER BY u.createdAt ASC;"
		long timeStart = System.currentTimeMillis();
		String query = "SELECT " +
				"DATE_FORMAT(ADDDATE(u.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS `createdAt`, " +
				"u.id, " +
				"u.fullName, " +
				"u.email, " +
				"u.pendingEmail, " +
				"l.mobile, " +
				"m.type , " +
				"u.isBanned, " +
				"m.type_platform " +
				"FROM hd1cas_db.User u " +
				"LEFT JOIN hd1cas_db.LocalAccount l ON l.id = u.localAccId " +
				"LEFT JOIN hd1cas_db.Device d ON d.userId = u.id " +
				"LEFT JOIN hd1cas_db.map_platform m ON m.platform = d.platform " +
				"WHERE u.createdAt > ?  and u.createdAt <= ? " +
				"GROUP BY u.id, d.userId ORDER BY u.createdAt ASC;";
		
		List<UserRegisterReport> result = new ArrayList<>();
		try {
			result = jdbc.query(query,
					new Object[]{startDate, endDate},
					new UserRegisterReport.Mapper());
			long timeEnd = System.currentTimeMillis();
			this.logger.info("[ReportService.getUserReport] from=" + startDate + " - to=" + endDate + " DONE. duration=" + (timeEnd - timeStart));
		} catch (Exception e) {
			long timeEnd = System.currentTimeMillis();
			this.logger.error("[ReportService.getUserReport] from=" + startDate + " - to=" + endDate + " FAIL. duration=" + (timeEnd - timeStart) + ". message=" + e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
	
	public List<TransactionReport> getTransactionReport(Date startDate, Date endDate) {
//		header = ["TransactionId","CreatedAt","UserId","UserCreatedAt","Type","ID","Name","Price","Source","Platform","DeviceType","ActiveCode","Partner","MobilePhone","Email","DeviceId","SerialId","ModelId"]
//		query = "SELECT t.transactionId, DATE_FORMAT(ADDDATE(t.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS createdAt, t.userId, DATE_FORMAT(ADDDATE(u.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS usercreatedat, t.type, t.itemId, t.name, t.price, t.methodName, m.type_platform, m.type AS devicetype, t.activeCode, t.codePartner, u.mobile, u.email, t.deviceId, t.serialId, t.modelId FROM hd1report_db.transaction_report t LEFT JOIN hd1billing_db.map_platform m ON m.platform = t.platform LEFT JOIN hd1report_db.user_report u ON u.id = t.userId WHERE t.status IN ('success', 'waiting') AND t.createdAt >= '#{startDate}' AND t.createdAt < '#{endDate}' ORDER BY t.createdAt;"
		long timeStart = System.currentTimeMillis();
		String query = "SELECT " +
				"t.id, " +
				"t.transactionId, " +
				"DATE_FORMAT(ADDDATE(t.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS createdAt, " +
				"t.userId, " +
				"DATE_FORMAT(ADDDATE(u.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS userCreatedAt, " +
				"t.type, " +
				"t.itemId, " +
				"t.name, " +
				"t.price, " +
				"t.methodName, " +
				"m.type_platform, " +
				"m.type as deviceType, " +
				"t.activeCode, " +
				"t.codePartner, " +
				"u.mobile, " +
				"u.email, " +
				"t.deviceId, " +
				"t.serialId, " +
				"t.modelId " +
				"FROM hd1report_db.transaction_report t " +
				"LEFT JOIN hd1billing_db.map_platform m on m.platform = t.platform " +
				"LEFT JOIN hd1report_db.user_report u on u.id = t.userId " +
				"WHERE t.status in ('success', 'waiting') " +
				"AND t.createdAt >= ? " +
				"AND t.createdAt < ? " +
				"ORDER BY t.createdAt;";
		
		List<TransactionReport> result = new ArrayList<>();
		try {
			result = jdbc.query(query,
					new Object[]{startDate, endDate},
					new TransactionReport.Mapper());
			long timeEnd = System.currentTimeMillis();
			this.logger.info("[ReportService.getTransactionReport] from=" + startDate + " - to=" + endDate + " DONE. Duration=" + (timeEnd - timeStart));
		} catch (Exception e) {
			long timeEnd = System.currentTimeMillis();
			this.logger.error("[ReportService.getTransactionReport] from=" + startDate + " - to=" + endDate + " FAIL. duration=" + (timeEnd - timeStart) + ". message=" + e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
	
	public void getTransactionFTPReport() {

//		transactionFtp: (endDate, startDate, config, done) ->
//				file_dir_date = endDate.split(' ')[0 ].replace(/-/g,'')
//		list_file = []
//		query_list = [
//		file:  file_dir_date + '_User.txt',
//		query = "SELECT DATE_FORMAT(ADDDATE(u.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS `createdAt`, u.id, u.fullName, f.email, u.pendingEmail, concat("'", l.mobile) AS `mobile`, m.type, IF(u.isBanned = 0, 'Normal', IF(u.isBanned = 2, 'INREVIEW', 'BANNED')) AS `status`, m.type_platform, IF(l.mobile IS NULL, 'Facebook', 'Mobile Phone') AS `channel` FROM hd1cas_db.User u LEFT JOIN hd1cas_db.FacebookAccount f ON f.id = u.fbAccId LEFT JOIN hd1cas_db.LocalAccount l ON l.id = u.localAccId LEFT JOIN hd1cas_db.Device d ON d.userId = u.id LEFT JOIN hd1cas_db.map_platform m ON m.platform = d.platform WHERE u.createdAt > '#{startDate}' AND u.createdAt <= '#{endDate}' GROUP BY u.id, d.userId ORDER BY u.createdAt ASC;"
		String query = "SELECT " +
				"   DATE_FORMAT(ADDDATE(u.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS `createdAt`, " +
				"   u.id, " +
				"   u.fullName, " +
				"   f.email, " +
				"   u.pendingEmail, " +
				"   l.mobile, " +
				"   m.type, " +
				"   u.isBanned " +
				"   m.type_platform, " +
				"FROM hd1cas_db.User u " +
				"   LEFT JOIN hd1cas_db.FacebookAccount f ON f.id = u.fbAccId " +
				"   LEFT JOIN hd1cas_db.LocalAccount l ON l.id = u.localAccId " +
				"   LEFT JOIN hd1cas_db.Device d ON d.userId = u.id " +
				"   LEFT JOIN hd1cas_db.map_platform m ON m.platform = d.platform " +
				"WHERE u.createdAt > ? " +
				"   AND u.createdAt <= ? " +
				"GROUP BY u.id, d.userId " +
				"ORDER BY u.createdAt ASC;";
//		        # ,
//		        #   file:  file_dir_date + '_Transaction.txt',
//			#   sql: """
//			#     SELECT t.transactionId, DATE_FORMAT(ADDDATE(t.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d') AS createdAt,
//      #       t.userId, DATE_FORMAT(ADDDATE(t.createdUser, INTERVAL 7 HOUR), '%Y-%m-%d') AS usercreatedat,
//      #       t.type,t.id, t.name, t.price, t.paymentSource, m.type_platform, m.type,
//			#       IF(t.activeCode = 'None', NULL, t.activeCode) AS code, a.consumerName
//      #     FROM hd1billing_db.tmp_user_tran t
//      #       LEFT JOIN hd1billing_db.business_code b on b.code=t.activeCode
//      #       LEFT JOIN hd1billing_db.bundle bb on bb.id =b.bundleId
//      #       LEFT JOIN hd1billing_db.code_campaign a on a.id = bb.campaignId
//      #       JOIN hd1billing_db.transaction tt on tt.id = t.transactionId
//      #       LEFT JOIN hd1billing_db.map_platform m on m.platform = tt.platform
//      #     WHERE t.hack='verified' AND t.paymentSource not in('IAP') AND t.createdAt >= '#{startDate}' AND t.createdAt < '#{endDate}'
//			#     ORDER BY t.createdAt;
//      #   """
//		      # ,
//		      #   file:  file_dir_date + '_TVOD.txt',
//			#   sql : """
//			#     SELECT t.userId, t.oldMovieId, t.name, t.knownAs, t.originalPrice, t.discount, t.price,
//			#       DATE_FORMAT(ADDDATE(t.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d %H:%i:%S'),
//			#       t.macAddress, m.type_platform, m.type, t.paymentSource,t.activeCode, t.codePrice, t.codePartner
//      #     FROM hd1billing_db.tmp_user_tran t
//      #       JOIN hd1billing_db.transaction tt on t.transactionId = tt.id
//      #       LEFT JOIN hd1billing_db.map_platform m on m.platform = tt.platform
//      #     WHERE t.type in('TVOD', 'Voucher') and  t.hack='Verified' and t.createdAt >= '#{startDate}' and t.createdAt < '#{endDate}';
//      #     """
//		      # ,
//		      #   file: file_dir_date + '_SVOD.txt',
//			#   sql : """
//			#     SELECT t.userId, t.id, DATE_FORMAT(ADDDATE(t.createdAt, INTERVAL 7 HOUR), '%Y-%m-%d %H:%i:%S'),t.price,
//			#       DATE_FORMAT(ADDDATE(t.expiryDate, INTERVAL 7 HOUR), '%Y-%m-%d %H:%i:%S'),
//			#       t.macAddress,m.type_platform, m.type, t.paymentSource,t.activeCode, t.codePrice, t.codePartner,t.name
//      #     FROM hd1billing_db.tmp_user_tran  t
//      #       JOIN hd1billing_db.transaction tt on t.transactionId = tt.id
//      #       LEFT JOIN hd1billing_db.map_platform m on m.platform = tt.platform
//      #     WHERE t.type = 'SVOD'  and t.hack='Verified' and t.createdAt >= '#{startDate}' and t.createdAt < '#{endDate}';
//      #     """
//		      # ,
//		      #   file: file_dir_date + '_Log.txt',
//			#   sql: """
//			#     SELECT t.userId,
//			#       t.macDevice, t.typeDevice, t.city, e.oldMovieId, e.title,e.knownAs,if(e.priceType=2, 'TVOD', if(e.priceType=3,'FREE','SVOD')) as type,
//      #       DATE_FORMAT(t.createdAt, '%Y-%m-%d') as viewAs,NULL as a, NULL as b, NULL as c
//      #     FROM hd1cm_db.tmp_log_fim t
//      #       JOIN hd1cm_db.entity_source s on s.sourceId = t.movieId
//      #       JOIN  hd1cm_db.entity e on e.id = s.entityId
//      #     WHERE e.status=1 AND char_length(t.userId)>31 AND t.userId like '%-%-%-%-%' AND  t.createdAt >= '#{startDate}' AND t.createdAt < '#{endDate}';
//      #     """
//		      ]

//		async.parallel [
//				(cb) ->
//						async.each query_list,(query, cb1) ->
//				file_dir = '/tmp/' + query.file
//		return sequelize.query query.sql,
//				type: sequelize.QueryTypes.SELECT
//
//				.then (result, a)->
//			# Gen file name
//
//				full_content = ''
//		for row in result
//				_row = ((if v == 'None' then '' else v)  for k,v of row)
//		full_content += _row.join('|') + '\n'
//
//			# Write file
//		fs.writeFile file_dir, full_content, (err)->
//		if err
//		throw err
//
//		sails.log.info('[ReportService.transactionFtp] Done ' + file_dir + ' ' + startDate + ' - ' + endDate);
//
//		dest = file_dir.split('/')
//		dest = dest[dest.length - 1]
//		dir_ftp = config.ftpDir
//		dir_month = endDate.replace(/-/g,'').substr(0,6)
//		file_ftp = dir_ftp + dir_month + '/' + dest
//
//		item =
//				input: file_dir
//		dest: file_ftp
//		list_file.push(item)
//		return cb1()
//				.catch (err)->
//		return cb1(err, null)
//        , (err) ->
//		if err
//		return cb(err)
//		return cb(null, list_file)
//
//    ], (err, result)->
//		if err
//		console.log {error: 1000, log: "[ReportService.transactionFtp] ERROR: #{err}", type: 'error'}
//		return done()
//		sails.log.info('[ReportService.transactionFtp] Done ' + startDate + ' - ' + endDate);
//		return done(result[0])
	}
}
