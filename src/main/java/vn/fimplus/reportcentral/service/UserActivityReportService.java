package vn.fimplus.reportcentral.service;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.UserActivityObject;
import vn.fimplus.reportcentral.model.report.UserActivityReport;
import vn.fimplus.reportcentral.repository.UserActivityRepository;

import java.io.IOException;
import java.util.*;

@Service
public class UserActivityReportService extends AbstractService{

    @Autowired
    UserActivityRepository userActivityRepository;

    private final String[] platformList = {"android", "ios", "tv_tizen", "tv_ceb", "tv_android", "tv_webos", "web", "web_mobile", "web_safari"};
    private int count;

    public int getCount(){
        return count;
    }
    private class ResultData{
        private String monthOfReport;
        private Map<String, Map<String, Integer>> data;

        public ResultData(String monthOfReport, Map<String, Map<String, Integer>> resultData){
            this.setMonthOfReport(monthOfReport);
            this.setData(resultData);
        }

        public String getMonthOfReport() {
            return monthOfReport;
        }

        public void setMonthOfReport(String monthOfReport) {
            this.monthOfReport = monthOfReport;
        }

        public Map<String, Map<String, Integer>> getData() {
            return data;
        }

        public void setData(Map<String, Map<String, Integer>> data) {
            this.data = data;
        }
    }

    public Map<String, Map<String, Map<String, Integer>>> getMonthReport(DateTime dateIn, DateTime dateEnd) throws IOException {
        Long beginDate = dateIn.getMillis();
        Long endDate = dateEnd.getMillis();
        //logger.info("check");
        this.count = 0;

        Map<String, Map<String, Map<String, Integer>>> result = new TreeMap<>();
        int monthDifference = Months.monthsBetween(dateIn,dateEnd).getMonths();

        for(String platform: this.platformList) {
            logger.info(platform);
            UserActivityReport userActivity = userActivityRepository.getPlatformActivityByDate(beginDate, endDate, platform);
            if(!userActivity.getList().isEmpty()){
                for (int i = 0; i <= monthDifference; i++) {
                    DateTime currentCheck = dateIn.plusMonths(i);
                    DateTime reportMonth = new DateTime().withDate(currentCheck.getYear(), currentCheck.getMonthOfYear(), 1);

                    Map<String, Map<String, Integer>> playCount = getPlayActivity(userActivity, platform);
                    count += playCount.get(platform).size();
                    ResultData platformResult = new ResultData(reportMonth.toString("yyyy-MM"), playCount);

                    Map<String, Map<String, Integer>> monthResult = result.get(platformResult.monthOfReport);
                    if (monthResult == null) {
                        monthResult = new HashMap<>();
                        result.put(platformResult.monthOfReport, monthResult);
                    }
                    monthResult.putAll(platformResult.getData());
                }
            }
        }
        return result;
    }

    private Map<String, Map<String, Integer>> getPlayActivity(UserActivityReport list, String platform){
        Map<String, Integer> result = new HashMap<>();

        if(platform.equals("android"))
             result = androidPlayActivity(list,"43565612-0c15-4438-8c86-2455d19e3198");
        else if(platform.equals("tv_android"))
            result = tvPlayActivity(list);
        else if(platform.equals("tv_tizen"))
            result = tvPlayActivity(list);
        else if(platform.equals("tv_webos"))
            result = tvPlayActivity(list);
        else if(platform.equals("tv_ceb"))
            result = tvPlayActivity(list);
        else if(platform.equals("web"))
            result = webPlayActivity(list, "THIẾU NHI");
        else if(platform.equals("web_safari"))
            result = webPlayActivity(list, "THIẾU NHI");
        else if(platform.equals("web_mobile"))
            result = webPlayActivity(list, "43565612-0c15-4438-8c86-2455d19e3198");
        else if(platform.equals("ios"))
            result = iOSPlayActivity(list, "43565612-0c15-4438-8c86-2455d19e3198");

        Map<String, Map<String, Integer>> resultMap = new HashMap<>();
        resultMap.put(platform, result);
        return resultMap;
    }

    private Map<String, Integer> androidPlayActivity(UserActivityReport list, String value){
        int index = 0;
        Map<String, Integer> result = new HashMap<>();
        while(index < list.getList().size()){
            if(list.getList().get(index).getEvent() != null && list.getList().get(index).getEvent().equals("click_menu") && list.getList().get(index).getValue().equals(value)){
                boolean exitMenu = false;
                String currentId = list.getList().get(index).getUserId();
                index++;
                while (!exitMenu && index < list.getList().size() && list.getList().get(index).getUserId().equals(currentId)){
                    if(list.getList().get(index).getEvent() == null){}
                    else if (list.getList().get(index).getEvent().equals("click_movie")) {

                        if(result.keySet().contains(list.getList().get(index).getUserId()))
                            result.put(list.getList().get(index).getUserId(), result.get(list.getList().get(index).getUserId())+1);
                        else
                            result.put(list.getList().get(index).getUserId(),1);

                    } else if (list.getList().get(index).getEvent().equals("click_menu") && !list.getList().get(index).getValue().equals(value)) {
                        exitMenu = true;
                    } else if (list.getList().get(index).getEvent().equals("search") && list.getList().get(index).getEvent().equals("click_log_out")) {
                        exitMenu = true;
                    }
                    else if(list.getList().get(index).getEvent().equals("click_back")){
                       exitMenu = true;
                    }
                    index++;
                }
            }
            else
                index++;
        }
        return result;
    }

    private Map<String, Integer> tvPlayActivity(UserActivityReport list){
        int index = 0;
        Map<String, Integer> result = new HashMap<>();
        if(list.getList().get(index).getPlatform().equals("tv_tizen")){
            while(index < list.getList().size()){
                if(list.getList().get(index).getEvent() != null && list.getList().get(index).getEvent().equals("icon_kid")){
                    boolean exitMenu = false;
                    String currentId = list.getList().get(index).getUserId();
                    index++;
                    while (!exitMenu && index < list.getList().size() && list.getList().get(index).getUserId().equals(currentId)){
                        if(list.getList().get(index).getEvent() == null){}
                        else if(list.getList().get(index).getEvent().equals("play_movie")){

                            if(result.keySet().contains(list.getList().get(index).getUserId()))
                                result.put(list.getList().get(index).getUserId(), result.get(list.getList().get(index).getUserId())+1);
                            else
                                result.put(list.getList().get(index).getUserId(),1);
                        }
                        else if(list.getList().get(index).getEvent().equals("search") || list.getList().get(index).getEvent().equals("click_back"))
                            exitMenu = true;
                        index++;
                    }
                }
                else
                    index++;
            }
        }
        else if(list.getList().get(index).getPlatform().equals("tv_webos")){
            while(index < list.getList().size()){
                if(list.getList().get(index).getEvent() != null && list.getList().get(index).getEvent().equals("icon_kid")){
                    boolean exitMenu = false;
                    String currentId = list.getList().get(index).getUserId();
                    index++;
                    while (!exitMenu && index < list.getList().size() && list.getList().get(index).getUserId().equals(currentId)){
                        if(list.getList().get(index).getEvent() == null){ }
                        else if(list.getList().get(index).getEvent().equals("click_play")){

                            if(result.keySet().contains(list.getList().get(index).getUserId()))
                                result.put(list.getList().get(index).getUserId(), result.get(list.getList().get(index).getUserId())+1);
                            else
                                result.put(list.getList().get(index).getUserId(),1);
                        }
                        else if(list.getList().get(index).getEvent().equals("search") || list.getList().get(index).getEvent().equals("click_back"))
                            exitMenu = true;
                        index++;
                    }
                }
                else
                    index++;
            }
        }
        else if(list.getList().get(index).getPlatform().equals("tv_ceb")){
            while(index < list.getList().size()){
                if(list.getList().get(index).getEvent() != null && list.getList().get(index).getEvent().equals("icon_kid")){
                    boolean exitMenu = false;
                    String currentId = list.getList().get(index).getUserId();
                    index++;
                    while (!exitMenu && index < list.getList().size() && list.getList().get(index).getUserId().equals(currentId)){
                        if(list.getList().get(index).getEvent() == null){ }
                        else if(list.getList().get(index).getEvent().equals("click_play")){

                            if(result.keySet().contains(list.getList().get(index).getUserId()))
                                result.put(list.getList().get(index).getUserId(), result.get(list.getList().get(index).getUserId())+1);
                            else
                                result.put(list.getList().get(index).getUserId(),1);
                        }
                        else if(list.getList().get(index).getEvent().equals("search") || list.getList().get(index).getEvent().equals("click_back"))
                            exitMenu = true;
                        index++;

                    }
                }
                else
                    index++;
            }
        }
        else{
            while(index < list.getList().size()){
                if(list.getList().get(index).getEvent() != null && list.getList().get(index).getEvent().equals("click_icon_kids")){
                    boolean exitMenu = false;
                    String currentId = list.getList().get(index).getUserId();
                    index++;
                    while (!exitMenu && index < list.getList().size() && list.getList().get(index).getUserId().equals(currentId)){
                        if(list.getList().get(index).getEvent() == null){}
                        else if(list.getList().get(index).getEvent().equals("click_play")){

                            if(result.keySet().contains(list.getList().get(index).getUserId()))
                                result.put(list.getList().get(index).getUserId(), result.get(list.getList().get(index).getUserId())+1);
                            else
                                result.put(list.getList().get(index).getUserId(),1);
                        }
                        else if(list.getList().get(index).getEvent().equals("search") || list.getList().get(index).getEvent().equals("click_back"))
                            exitMenu = true;
                        index++;
                    }
                }
                else
                    index++;
            }
        }
        return result;
    }

    private Map<String, Integer> iOSPlayActivity(UserActivityReport userList, String value){
        Map<String, Integer> result = new HashMap<>();
        int index = 0;
        while(index < userList.getList().size()){
            if(userList.getList().get(index).getEvent() != null && userList.getList().get(index).getEvent().equals("category") && userList.getList().get(index).getValue().equals(value)){
                String currentId = userList.getList().get(index).getUserId();
                index++;
                boolean exitMenu = false;
                while(!exitMenu && index < userList.getList().size() && userList.getList().get(index).getUserId().equals(currentId)){
                    if(userList.getList().get(index).getEvent() == null){}
                    else if(userList.getList().get(index).getEvent().equals("play_movie")){

                        if(result.keySet().contains(userList.getList().get(index).getUserId()))
                            result.put(userList.getList().get(index).getUserId(), result.get(userList.getList().get(index).getUserId())+1);
                        else
                            result.put(userList.getList().get(index).getUserId(),1);

                    }
                    else if(userList.getList().get(index).getEvent().equals("search") || userList.getList().get(index).getEvent().equals("click_back") || userList.getList().get(index).getEvent().equals("click_log_out"))
                        exitMenu = false;

                    index++;
                }
            }
            else
                index++;
        }
        return result;
    }

    private Map<String, Integer> webPlayActivity(UserActivityReport list, String value){
        Map<String, Integer> result = new HashMap<>();
        int index = 0;
        if(list.getList().get(index).getPlatform().equals("web")){
            while(index < list.getList().size()){
                if(list.getList().get(index).getEvent() != null && list.getList().get(index).getEvent().equals("menu_category") && list.getList().get(index).getValue().equals(value)){
                    boolean exitMenu = false;
                    String currentId = list.getList().get(index).getUserId();
                    index++;
                    while(!exitMenu && index < list.getList().size() && list.getList().get(index).getUserId().equals(currentId)){
                        if(list.getList().get(index).getEvent() == null){}
                        else if(list.getList().get(index).getEvent().equals("play_episode")){
                            if(result.keySet().contains(list.getList().get(index).getUserId()))
                                result.put(list.getList().get(index).getUserId(), result.get(list.getList().get(index).getUserId())+1);
                            else
                                result.put(list.getList().get(index).getUserId(),1);
                        }
                        else if(list.getList().get(index).getEvent().equals("search"))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("logout"))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("menu_category") && !list.getList().get(index).getEvent().equals(value))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("login_facebook") || list.getList().get(index).getEvent().equals("login_phone"))
                            exitMenu = true;
                        index++;
                    }
                }
                else
                    index++;
            }
        }
        else if(list.getList().get(index).getPlatform().equals("web_safari")){
            while(index < list.getList().size()){
                if(list.getList().get(index).getEvent() != null && list.getList().get(index).getEvent().equals("menu_category") && list.getList().get(index).getValue().equals(value)){
                    boolean exitMenu = false;
                    String currentId = list.getList().get(index).getUserId();
                    index++;
                    while(!exitMenu && index < list.getList().size() && list.getList().get(index).getUserId().equals(currentId)){

                        if(list.getList().get(index).getEvent() == null){}
                        else if(list.getList().get(index).getEvent().equals("play_episode")){
                            if(result.keySet().contains(list.getList().get(index).getUserId()))
                                result.put(list.getList().get(index).getUserId(), result.get(list.getList().get(index).getUserId())+1);
                            else
                                result.put(list.getList().get(index).getUserId(),1);
                        }
                        else if(list.getList().get(index).getEvent().equals("search"))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("logout"))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("menu_category") && !list.getList().get(index).getEvent().equals(value))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("login_facebook") || list.getList().get(index).getEvent().equals("login_phone"))
                            exitMenu = true;
                        index++;
                    }
                }
                else
                    index++;
            }
        }
        else{
            while(index < list.getList().size()){
                if(list.getList().get(index).getEvent() != null && list.getList().get(index).getEvent().equals("menu_category") && list.getList().get(index).getValue().equals(value)){
                    boolean exitMenu = false;
                    String currentId = list.getList().get(index).getUserId();
                    index++;
                    while(!exitMenu && index < list.getList().size() && list.getList().get(index).getUserId().equals(currentId)){
                        if(list.getList().get(index).getEvent() == null){}
                        else if(list.getList().get(index).getEvent().equals("play_movie")){
                            if(result.keySet().contains(list.getList().get(index).getUserId()))
                                result.put(list.getList().get(index).getUserId(), result.get(list.getList().get(index).getUserId())+1);
                            else
                                result.put(list.getList().get(index).getUserId(),1);
                        }
                        else if(list.getList().get(index).getEvent().equals("search"))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("login_phone") || list.getList().get(index).getEvent().equals("login_facebook"))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("profile"))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("logout"))
                            exitMenu = true;
                        else if(list.getList().get(index).getEvent().equals("menu_category") && !list.getList().get(index).getEvent().equals(value))
                            exitMenu = true;
                        index++;


                    }
                }
                else
                    index++;
            }
        }
        return result;
    }
}
