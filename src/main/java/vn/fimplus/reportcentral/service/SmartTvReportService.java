package vn.fimplus.reportcentral.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.report.SmartTvReport.A30Report;
import vn.fimplus.reportcentral.model.report.SmartTvReport.SmartTvReport;
import vn.fimplus.reportcentral.repository.SmartTvRepository.LGRepository;
import vn.fimplus.reportcentral.repository.SmartTvRepository.SamsungRepository;
import vn.fimplus.reportcentral.repository.SmartTvRepository.SonyRepository;
import vn.fimplus.reportcentral.util.DateTimeUtils;

import java.lang.invoke.SwitchPoint;
import java.util.*;

@Service
public class SmartTvReportService extends AbstractService{

    @Autowired
    SamsungRepository samsungRepository;
    @Autowired
    LGRepository lgRepository;
    @Autowired
    SonyRepository sonyRepository;

    public Map<String, List<String>> getFirsttransUserId(String platform, String date){
        Map<String, List<String>> result = generateListMap();
        List<SmartTvReport> list = null;
        switch (platform){
            case "samsung":
                list = samsungRepository.getBundlingAsFirstTrans(date);
                break;
            case "lg":
                list = lgRepository.getBundlingAsFirstTrans(date);
                break;
            case "sony":
                list = sonyRepository.getBundlingAsFirstTrans(date);
                break;
        }
        for(SmartTvReport i : list){
            int year = i.getCreatedAt().getYear();
            int month = i.getCreatedAt().getMonthOfYear();
            String key;
            if(month < 10)
                key = year +"0"+month;
            else
                key = year +""+month;
            result.get(key).add(i.getUserId());
        }
        return result;
    }

    public Map<String, List<String>> getIndependentUserId(String platform, String date){
        Map<String, List<String>> result = generateListMap();
        List<SmartTvReport> list = null;
        switch (platform){
            case "samsung":
                list = samsungRepository.getIndependentFromBundlingReport(date);
                break;
            case "lg":
                list = lgRepository.getIndependentFromBundlingReport(date);
                break;
            case "sony":
                list = sonyRepository.getIndependentFromBundlingReport(date);
                break;
        }
        for(SmartTvReport i : list){
            int year = i.getCreatedAt().getYear();
            int month = i.getCreatedAt().getMonthOfYear();
            String key;
            if(month < 10)
                key = year +"0"+month;
            else
                key = year +""+month;
            result.get(key).add(i.getUserId());
        }
        return result;
    }

    public Map<String, List<SmartTvReport>> getDetailReport(String platform, String date){
        Map<String, List<SmartTvReport>> result = new TreeMap<>();
        List<SmartTvReport> list = null;
        switch (platform){
            case "samsung":
                list = samsungRepository.getIndependentFromBundlingReport(date);
                break;
            case "lg":
                list = lgRepository.getIndependentFromBundlingReport(date);
                break;
            case "sony":
                list = sonyRepository.getIndependentFromBundlingReport(date);
                break;
        }
        List<SmartTvReport> tempList = new ArrayList<>();
        for(SmartTvReport i : list){
            int month = i.getCreatedAt().getMonthOfYear();
            if(i.getCreatedAt().getYear() == 2018 && month == 4){
                tempList.add(i);
            }
        }
        result.put("Independent", tempList);
        tempList = new ArrayList<>();
        switch (platform){
            case "samsung":
                list =  samsungRepository.getBundlingAsFirstTrans(date);
                break;
            case "lg":
                list =  lgRepository.getBundlingAsFirstTrans(date);
                break;
            case "sony":
                list = sonyRepository.getBundlingAsFirstTrans(date);
                break;
        }
        for(SmartTvReport i : list){
            int month = i.getCreatedAt().getMonthOfYear();
            if(i.getCreatedAt().getYear() == 2018 && month == 4){
                tempList.add(i);
            }
        }
        result.put("1stBundling", tempList);
        return result;
    }

    public Map<Integer, Map<String, Map<String, Integer>>> getIndependentFromBundling(String platform, String date){
        Map<Integer, Map<String, Map<String, Integer>>> result = generateResultMap();
        List<SmartTvReport> list = null;
        switch (platform){
            case "samsung":
                list = samsungRepository.getIndependentFromBundlingReport(date);
                break;
            case "lg":
                list = lgRepository.getIndependentFromBundlingReport(date);
                break;
            case "sony":
                list = sonyRepository.getIndependentFromBundlingReport(date);
                break;
        }
        for(SmartTvReport i : list){
            int year = i.getCreatedAt().getYear();
            Integer month = i.getCreatedAt().getMonthOfYear();
            if(i.getGroupContentId().equals("G001"))
                result.get(year).get(month.toString()).put("Basic", result.get(year).get(month.toString()).get("Basic") + 1);
            else
                result.get(year).get(month.toString()).put("Premium", result.get(year).get(month.toString()).get("Premium") + 1);
        }
        return result;
    }

    public Map<Integer, Map<String, Map<String, Integer>>> getExpiredSubcription(String platform, String date){
        Map<Integer, Map<String, Map<String, Integer>>> result = generateResultMap();
        List<SmartTvReport> list = null;
        switch (platform){
            case "samsung":
                list = samsungRepository.getAllBundlingReport(date);
                break;
            case "lg":
                list = lgRepository.getAllBundlingReport(date);
                break;
            case "sony":
                list = sonyRepository.getAllBundlingReport(date);
                break;
        }
        for(SmartTvReport i : list){
            int year = i.getExpiryDate().getYear();
            if(year <= 2018) {
                Integer month = i.getExpiryDate().getMonthOfYear();
                if (i.getGroupContentId().equals("G001"))
                    result.get(year).get(month.toString()).put("Basic", result.get(year).get(month.toString()).get("Basic") + 1);
                else
                    result.get(year).get(month.toString()).put("Premium", result.get(year).get(month.toString()).get("Premium") + 1);
            }
        }
        return result;
    }

    public Map<Integer, Map<String, Map<String, Integer>>> newBundlingSubcription(String platform, String date){
        Map<Integer, Map<String, Map<String, Integer>>> result = generateResultMap();
        List<SmartTvReport> list = null;
        switch (platform){
            case "samsung":
                list = samsungRepository.getBundlingAsFirstTrans(date);
                break;
            case "lg":
                list = lgRepository.getBundlingAsFirstTrans(date);
                break;
            case "sony":
                list = sonyRepository.getBundlingAsFirstTrans(date);
                break;
        }
        for(SmartTvReport i : list){
            int year = i.getCreatedAt().getYear();
            Integer month = i.getCreatedAt().getMonthOfYear();
            if(i.getGroupContentId().equals("G001"))
                result.get(year).get(month.toString()).put("Basic", result.get(year).get(month.toString()).get("Basic") + 1);
            else
                result.get(year).get(month.toString()).put("Premium", result.get(year).get(month.toString()).get("Premium") + 1);
        }
        return result;
    }

    public Map<String, Map<String, Integer>> a30Report(String platform){
        Map<String, Map<String, Integer>> result = null;
        switch (platform){
            case "samsung":
                result = samsungRepository.getA30Report();
                break;
            case "lg":
                result = lgRepository.getA30Report();
                break;
            case "sony":
                result = sonyRepository.getA30Report();
                break;
        }
        return result;
    }

    private Map<Integer, Map<String, Map<String, Integer>>> generateResultMap(){
        Map<Integer, Map<String, Map<String, Integer>>> result = new TreeMap<>();
        int time = DateTime.now().getYear();
        for(int i = 0; i <= (time - 2016); i++){
            int year = 2016 + i;
            Map<String, Map<String, Integer>> temp = new TreeMap<>();
            for(int j = 1; j <= 12; j++){
                Integer month = j;
                Map<String, Integer> temp1 = new TreeMap<>();
                temp1.put("Premium", 0);
                temp1.put("Basic", 0);
                temp.put(month.toString(), temp1);
            }
            result.put(year, temp);
        }
        return result;
    }

    private Map<String, List<String>> generateListMap(){
        Map<String, List<String>> result = new TreeMap<>();
        int time = DateTime.now().getYear();
        for(int i = 0; i <= (time-2016); i++){
            int  year = 2016 + i;
            for(int j = 1; j <= 12;j++){
                int month = j;
                String key;
                if(j < 10)
                    key = year+"0"+month;
                else
                    key = year+""+month;
                List<String> list = new ArrayList<>();
                result.put(key, list);
            }
        }
        return result;
    }
}
