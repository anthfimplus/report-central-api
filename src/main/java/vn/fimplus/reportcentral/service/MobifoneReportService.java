package vn.fimplus.reportcentral.service;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.report.MBFTransaction;
import vn.fimplus.reportcentral.repository.MobifoneReportRepository;
import vn.fimplus.reportcentral.util.DateTimeUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Service
public class MobifoneReportService extends AbstractService {
	
	
	@Autowired
	MobifoneReportRepository mobifoneReportRepository;


//	Map<String, MBFTransaction> alltrans;
	
	public Map<String, Map<String, Object>> getMonthReport(String packageCode, DateTime from, DateTime to) {
		from = from.withDayOfMonth(1).withTimeAtStartOfDay();
		to = to.withDayOfMonth(1).withTimeAtStartOfDay();
		
		Map<String, Map<String, Object>> result = new TreeMap<>();
//		alltrans = new HashMap<>();
//		getAll();
		int monthDiff = Months.monthsBetween(from, to).getMonths();
		for (int i = 0; i <= monthDiff; i++) {
			
			DateTime currentCheck = from.plusMonths(i);
			DateTime startOfMonth = DateTimeUtils.getStartOfMonth(currentCheck);
			DateTime endOfMonth = DateTimeUtils.getEndOfMonth(currentCheck);
			
			Map<String, Object> monthResult = new HashMap<>();
			result.put(startOfMonth.toString("yyyy-MM"), monthResult);

			Map<String, Object> opening = this.getOpening(packageCode, startOfMonth);
			monthResult.putAll(opening);

			Map<String, Object> ending = this.getEnding(packageCode, startOfMonth);
			monthResult.putAll(ending);

			Map<String, Object> renew = this.getRenew(packageCode, startOfMonth, endOfMonth);
			monthResult.putAll(renew);

			Map<String, Object> firstTime = this.getFirstTime(packageCode, startOfMonth, endOfMonth);
			monthResult.putAll(firstTime);

			Map<String, Object> extraCount = this.getExtra(packageCode, startOfMonth, endOfMonth);
			for (String key : extraCount.keySet()) {
				if (key.equals("buyExtra")) {
					int value = (int) monthResult.get(key);
					value += (int) extraCount.get(key);
					monthResult.put(key, value);
				} else
					monthResult.put(key, monthResult.get(key));
			}

			Map<String, Object> returnCount = this.getReturn(packageCode, startOfMonth, endOfMonth);
			monthResult.putAll(returnCount);
			for (String key : returnCount.keySet()) {
				if (key.equals("return")) {
					int value = (int) monthResult.get("firsttime");
					value += (int) returnCount.get(key);
					monthResult.put("firsttime", value);
				}
			}

			Map<String, Object> cancel = this.getCancel(packageCode, startOfMonth, endOfMonth);
			monthResult.putAll(cancel);
			
			Map<String, Object> activeUser = this.getActivated(packageCode, startOfMonth, endOfMonth);
			monthResult.putAll(activeUser);
		}
//		printMissing();
		return result;
	}

//	private void getAll() {
//		String thisMonthTransactionQuery = "SELECT * " +
//				"FROM hd1mbf_4g_db.transaction " +
//				"WHERE YEAR(ADDDATE(createdAt, INTERVAL 7 HOUR)) = 2017 AND MONTH(ADDDATE(createdAt, INTERVAL 7 HOUR)) = 10 AND " +
//				"      status = 2 AND packageCode = 'FIM30' AND transactionCode NOT IN ('CANCEL_RENEW', 'CANCEL') " +
//				"ORDER BY createdAt";
//
//
//		Connection conn = null;
//
//		try {
//			conn = mobifoneReportRepository.getDataSource().getConnection();
//			PreparedStatement ps = conn.prepareStatement(thisMonthTransactionQuery);
//			ResultSet resultSet = ps.executeQuery();
//			MBFTransaction.Mapper mapper = new MBFTransaction.Mapper();
//			while (resultSet.next()) {
//				MBFTransaction transaction = mapper.mapRow(resultSet, 0);
//
//				alltrans.put(transaction.getTransactionId(), transaction);
//			}
//			resultSet.close();
//			ps.close();
//		} catch (SQLException e) {
//			throw new RuntimeException(e);
//
//		} finally {
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {
//				}
//			}
//		}
//
//	}
//
//	public void printMissing() {
//		FileWriter writer = null;
//		try {
//			writer = new FileWriter("/Users/antran/mobifonedata/allmissingtrans.csv");
//
//			for (MBFTransaction trans : alltrans.values()) {
//				writer.append(trans.getTransactionId() + "," + trans.getMsisdn() + "," + trans.getTransactionCode() + "," + trans.getCreatedAt() + "\n");
//			}
//
//			writer.flush();
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
	private Map<String, Object> getActivated(String packageCode, DateTime from, DateTime to) {
		long timeStart = System.currentTimeMillis();
		String queryToCountUserExist = "SELECT COUNT(DISTINCT t.userId) AS result " +
				"FROM hd1mbf_4g_db.transaction t " +
				"   INNER JOIN hd1billing_db.transaction On transaction.userId = t.userId " +
				"WHERE " +
				"t.status = 2 AND t.packageCode = ?  " +
				"AND date_add(t.createdAt, INTERVAL 7 HOUR) BETWEEN ? AND ? ";
		
		long resultValue = mobifoneReportRepository.countByQuery(queryToCountUserExist,
				packageCode, from.toString(DateTimeUtils.DATE_FORMAT), to.toString(DateTimeUtils.DATE_FORMAT));
		Map<String, Object> resultData = new HashMap<>();
		resultData.put("activatedUser", resultValue);
		
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[MobifoneReportService.getActivated] " + from.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return resultData;
	}
	
	private Map<String, Object> getOpening(String packageCode, DateTime startDayOfMonth) {
		long timeStart = System.currentTimeMillis();
		DateTime prevMonthStart = startDayOfMonth.plusMonths(-1);
		DateTime prevMonthEnd = DateTimeUtils.getEndOfMonth(prevMonthStart);
		long totalOpening = mobifoneReportRepository.getRemainSubscription(packageCode, prevMonthStart, prevMonthEnd);
		Map<String, Object> resultData = new HashMap<>();
		resultData.put("opening", totalOpening);
		
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[MobifoneReportService.getOpening] " + startDayOfMonth.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return resultData;
	}
	
	private Map<String, Object> getEnding(String packageCode, DateTime startDayOfMonth) {
		long timeStart = System.currentTimeMillis();
		DateTime monthStart = startDayOfMonth;
		DateTime monthEnd = DateTimeUtils.getEndOfMonth(monthStart);
		
		long totalEnding = mobifoneReportRepository.getRemainSubscription(packageCode, monthStart, monthEnd);
		Map<String, Object> resultData = new HashMap<>();
		resultData.put("ending", totalEnding);
		
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[MobifoneReportService.getEnding] " + startDayOfMonth.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return resultData;
	}
	
	private Map<String, Object> getExtra(String packageCode, DateTime from, DateTime to) {
		long timeStart = System.currentTimeMillis();
		String lastMonthTransactionQuery = "SELECT " +
				"        t1.id, " +
				"        t1.msisdn, " +
				"        t1.transactionCode, " +
				"        ADDDATE(t1.createdAt, INTERVAL 7 HOUR) as createdAt " +
				"      FROM (SELECT " +
				"              t.id, " +
				"              t.msisdn, " +
				"              t.transactionCode, " +
				"              t.createdAt " +
				"            FROM hd1mbf_4g_db.transaction t " +
				"            WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
				"                  AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ?) t1 " +
				"        INNER JOIN (SELECT " +
				"                      t.msisdn, " +
				"                      MAX(t.createdAt) AS createdAt " +
				"                    FROM hd1mbf_4g_db.transaction t " +
				"                    WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
				"                          AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? " +
				"                    GROUP BY t.msisdn) t2 " +
				"          ON t1.msisdn = t2.msisdn AND t1.createdAt = t2.createdAt";
		
		String thisMonthTransactionQuery = "SELECT " +
				"  t.id, " +
				"  t.msisdn, " +
				"  t.transactionCode, " +
				"  ADDDATE(t.createdAt, INTERVAL 7 HOUR) as createdAt " +
				"FROM hd1mbf_4g_db.transaction t " +
				"WHERE t.packageCode = ? AND status = 2 AND t.transactionCode NOT IN ('CANCEL_RENEW', 'EXTEND') AND " +
				"   DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? " +
				"ORDER BY t.createdAt";
		
		Map<String, MBFTransaction> lastMonthState = new HashMap<>();
		Map<String, List<MBFTransaction>> cancelTransaction = new HashMap<>();
		Map<String, List<MBFTransaction>> thisMonthTransaction = new HashMap<>();
		
		Connection conn = null;
		
		try {
			DateTime queryFrom = from.plusMonths(-1);
			DateTime queryTo = to.plusMonths(-1).dayOfMonth().withMaximumValue();
			List<MBFTransaction> resultList = mobifoneReportRepository.getJdbc().query(lastMonthTransactionQuery, new MBFTransaction.Mapper(),
					packageCode, queryFrom.toString(DateTimeUtils.DATE_FORMAT), queryTo.toString(DateTimeUtils.DATE_FORMAT),
					packageCode, queryFrom.toString(DateTimeUtils.DATE_FORMAT), queryTo.toString(DateTimeUtils.DATE_FORMAT));
			
			for (MBFTransaction item : resultList) {
				lastMonthState.put(item.getMsisdn(), item);
			}
			
			conn = mobifoneReportRepository.getDataSource().getConnection();
			PreparedStatement ps = conn.prepareStatement(thisMonthTransactionQuery);
			ps.setString(1, packageCode);
			ps.setString(2, from.toString(DateTimeUtils.DATE_FORMAT));
			ps.setString(3, to.toString(DateTimeUtils.DATE_FORMAT));
			ResultSet resultSet = ps.executeQuery();
			MBFTransaction.Mapper mapper = new MBFTransaction.Mapper();
			while (resultSet.next()) {
				MBFTransaction transaction = mapper.mapRow(resultSet, 0);
				Map<String, List<MBFTransaction>> mapToPut = null;
				if (transaction.getTransactionCode().equals("CANCEL"))
					mapToPut = cancelTransaction;
				else
					mapToPut = thisMonthTransaction;
				
				List<MBFTransaction> userTransactionList = mapToPut.get(transaction.getMsisdn());
				if (userTransactionList == null) {
					userTransactionList = new ArrayList<>();
					mapToPut.put(transaction.getMsisdn(), userTransactionList);
				}
				userTransactionList.add(transaction);
			}
			resultSet.close();
			ps.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		
		int total = 0;
		Map<String, Integer> countForUser = new HashMap<>();
		List<MBFTransaction> allTransaction = new ArrayList<>();
		for (Map.Entry<String, List<MBFTransaction>> entry : thisMonthTransaction.entrySet()) {
			String userId = entry.getKey();
			List<MBFTransaction> transactions = entry.getValue();
			MBFTransaction lastMonthTrans = lastMonthState.get(userId);
			List<MBFTransaction> checkPoint = cancelTransaction.get(userId);
			boolean countFirstTrans = true;
			if (lastMonthTrans == null || lastMonthTrans.getTransactionCode().equals("CANCEL")) {
				// range start with first transaction open
				countFirstTrans = false;
			} else {
				// range start with 1st, also count the first transaction
			}
			int userTotal = 0;
			int i = 0;
			int transNo = 0;
			DateTime end = to;
			DateTime lastTransTime = null;
			boolean nextIsOpenTransQueue = !countFirstTrans; // if isOpen = true: next trans is open of queue, skip count
			do {
				if (checkPoint != null && !checkPoint.isEmpty() && i < checkPoint.size()) {
					MBFTransaction transaction = checkPoint.get(i);
					end = transaction.getCreatedAt();
				} else {
					end = to;
				}
				do {
					MBFTransaction trans = transactions.get(transNo);
					if (checkPoint != null && !checkPoint.isEmpty()) {
						if (trans.getCreatedAt().isAfter(end)) {
							i++;
							transNo++;
							break;
						}
					}
//					if (lastTransTime == null || Seconds.secondsBetween(trans.getCreatedAt(), lastTransTime).getSeconds() > 60)
					if (trans.getTransactionCode().equals("SUBSCRIPTION") || trans.getTransactionCode().equals("CHANGE_SUBSCRIPTION"))
						if (nextIsOpenTransQueue)
							nextIsOpenTransQueue = false;
						else {
							allTransaction.add(trans);
							userTotal++;
//							alltrans.remove(trans.getTransactionId());
						}
					transNo++;
				} while (transNo < transactions.size());
			} while (transNo < transactions.size());
			if (userTotal > 0) {
				total += userTotal;
				countForUser.put(userId, userTotal);
			}
		}
		
		Map<String, Object> resultData = new HashMap<>();
		resultData.put("buyExtra", total);
		resultData.put("buyExtraUniqueUser", countForUser.size());

//		FileWriter writer = null;
//		try {
//			writer = new FileWriter("/Users/antran/mobifonedata/" + from.toString("yyyy-MM") + "_extra.csv");
//
//			for (MBFTransaction trans : allTransaction) {
//				writer.append(trans.getTransactionId() + "," + trans.getMsisdn() + "," + trans.getTransactionCode() + "," + trans.getCreatedAt() + "\n");
//			}
//
//			writer.flush();
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[MobifoneReportService.getExtra] " + from.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return resultData;
	}
	
	private Map<String, Object> getFirstTime(String packageCode, DateTime from, DateTime to) {
		long timeStart = System.currentTimeMillis();
		String query =
				"SELECT " +
						"  trans.id, " +
						"  trans.msisdn, " +
						"  trans.transactionCode, " +
						"  ADDDATE(trans.createdAt, INTERVAL 7 HOUR) as createdAt " +
						"FROM hd1mbf_4g_db.transaction trans " +
						"WHERE trans.transactionCode IN ('SUBSCRIPTION', 'CHANGE_SUBSCRIPTION') AND trans.packageCode = ? AND trans.status = 2 AND " +
						"   DATE(ADDDATE(trans.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? AND " +
						"   trans.msisdn NOT IN ( " +
						"		SELECT DISTINCT t.msisdn " +
						"		FROM hd1mbf_4g_db.transaction t " +
						"		WHERE t.transactionCode IN ('SUBSCRIPTION', 'CHANGE_SUBSCRIPTION', 'EXTEND') AND t.packageCode = ? AND t.status = 2 AND " +
						"           DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) < ?)";
		
		List<MBFTransaction> resultList = mobifoneReportRepository.getJdbc().query(query, new MBFTransaction.Mapper(),
				packageCode, from.toString(DateTimeUtils.DATE_FORMAT), to.toString(DateTimeUtils.DATE_FORMAT),
				packageCode, from.toString(DateTimeUtils.DATE_FORMAT));
		
		Map<String, List<MBFTransaction>> thisMonthTransaction = convertToMsisdnTransactionMap(resultList);
		
		Map<String, Object> resultData = new HashMap<>();
		resultData.put("firsttime", thisMonthTransaction.size());

//		FileWriter writer = null;
//		try {
//			writer = new FileWriter("/Users/antran/mobifonedata/" + from.toString("yyyy-MM") + "_firsttime.csv");
//
//			for (List<MBFTransaction> transList : thisMonthTransaction.values())
//				for (MBFTransaction trans : transList) {
//					writer.append(trans.getTransactionId() + "," + trans.getMsisdn() + "," + trans.getTransactionCode() + "," + trans.getCreatedAt() + "\n");
//				}
//
//			writer.flush();
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[MobifoneReportService.getFirstTime] " + from.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return resultData;
	}
	
	private Map<String, Object> getReturn(String packageCode, DateTime from, DateTime to) {
		long timeStart = System.currentTimeMillis();
		String query =
				"SELECT " +
						"  trans.id, " +
						"  trans.msisdn, " +
						"  trans.transactionCode, " +
						"  ADDDATE(trans.createdAt, INTERVAL 7 HOUR) as createdAt " +
						"FROM hd1mbf_4g_db.transaction trans " +
						"WHERE " +
						"  trans.transactionCode IN ('SUBSCRIPTION', 'CHANGE_SUBSCRIPTION') AND trans.packageCode = ? AND trans.status = 2 AND " +
						"  DATE(ADDDATE(trans.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? AND " +
						"  trans.msisdn IN ( " +
						"    SELECT t3.msisdn " +
						"    FROM (SELECT " +
						"            t1.msisdn, " +
						"            t1.transactionCode, " +
						"            ADDDATE(t1.createdAt, INTERVAL 7 HOUR) AS createdAt " +
						"          FROM (SELECT " +
						"                  t.msisdn, " +
						"                  t.transactionCode, " +
						"                  t.createdAt " +
						"                FROM hd1mbf_4g_db.transaction t " +
						"                WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
						"                      AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) < ?) t1 " +
						"            INNER JOIN (SELECT " +
						"                          t.msisdn, " +
						"                          MAX(t.createdAt) AS createdAt " +
						"                        FROM hd1mbf_4g_db.transaction t " +
						"                        WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
						"                              AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) < ? " +
						"                        GROUP BY t.msisdn) t2 " +
						"              ON t1.msisdn = t2.msisdn AND t1.createdAt = t2.createdAt) t3 " +
						"    WHERE t3.transactionCode = 'CANCEL' OR (t3.createdAt < ?))";
		
		Connection conn = null;
		Map<String, List<MBFTransaction>> thisMonthTransaction = new HashMap<>();
		try {
			conn = mobifoneReportRepository.getDataSource().getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, packageCode);
			ps.setString(2, from.toString(DateTimeUtils.DATE_FORMAT));
			ps.setString(3, to.toString(DateTimeUtils.DATE_FORMAT));
			ps.setString(4, packageCode);
			ps.setString(5, from.toString(DateTimeUtils.DATE_FORMAT));
			ps.setString(6, packageCode);
			ps.setString(7, from.toString(DateTimeUtils.DATE_FORMAT));
			ps.setString(8, from.plusMonths(-1).toString(DateTimeUtils.DATE_FORMAT));
			ResultSet resultSet = ps.executeQuery();
			MBFTransaction.Mapper mapper = new MBFTransaction.Mapper();
			while (resultSet.next()) {
				MBFTransaction transaction = mapper.mapRow(resultSet, 0);
				
				List<MBFTransaction> userTransactionList = thisMonthTransaction.get(transaction.getMsisdn());
				if (userTransactionList == null) {
					userTransactionList = new ArrayList<>();
					thisMonthTransaction.put(transaction.getMsisdn(), userTransactionList);
				}
				userTransactionList.add(transaction);
//				alltrans.remove(transaction.getTransactionId());
			}
			resultSet.close();
			ps.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		Map<String, Object> resultData = new HashMap<>();
		resultData.put("return", thisMonthTransaction.size());

//		FileWriter writer = null;
//		try {
//			writer = new FileWriter("/Users/antran/mobifonedata/" + from.toString("yyyy-MM") + "_return.csv");
//
//			for (List<MBFTransaction> transList : thisMonthTransaction.values())
//				for (MBFTransaction trans : transList) {
//					writer.append(trans.getTransactionId() + "," + trans.getMsisdn() + "," + trans.getTransactionCode() + "," + trans.getCreatedAt() + "\n");
//				}
//
//			writer.flush();
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[MobifoneReportService.getReturn] " + from.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return resultData;
	}
	
	private Map<String, Object> getRenew(String packageCode, DateTime from, DateTime to) {
		long timeStart = System.currentTimeMillis();
		
		String lastMonthEnding =
				"SELECT allResult.msisdn " +
						"FROM (SELECT " +
						"        t1.msisdn, " +
						"        t1.transactionCode " +
						"      FROM (SELECT " +
						"              t.msisdn, " +
						"              t.transactionCode, " +
						"              t.createdAt " +
						"            FROM hd1mbf_4g_db.transaction t " +
						"            WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
						"                  AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ?) t1 " +
						"        INNER JOIN (SELECT " +
						"                      t.msisdn, " +
						"                      MAX(t.createdAt) AS createdAt " +
						"                    FROM hd1mbf_4g_db.transaction t " +
						"                    WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
						"                          AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? " +
						"                    GROUP BY t.msisdn) t2 " +
						"          ON t1.msisdn = t2.msisdn AND t1.createdAt = t2.createdAt) allResult " +
						"WHERE allResult.transactionCode <> 'CANCEL' ";
		
		String thisMonthTransactionQuery = "SELECT " +
				"  t.id, " +
				"  t.msisdn, " +
				"  t.transactionCode, " +
				"  ADDDATE(t.createdAt, INTERVAL 7 HOUR) as createdAt " +
				"FROM hd1mbf_4g_db.transaction t " +
				"WHERE t.packageCode = ? AND status = 2 AND t.transactionCode NOT IN ('CANCEL_RENEW') AND " +
				"   DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? " +
				"ORDER BY t.createdAt";
		
		List<String> lastMonthEndingMsisdn = new ArrayList<>();
		DateTime queryFrom = from.plusMonths(-1);
		DateTime queryTo = to.plusMonths(-1).dayOfMonth().withMaximumValue();
		List<Map<String, Object>> results = mobifoneReportRepository.getJdbc().queryForList(lastMonthEnding,
				packageCode, queryFrom.toString(DateTimeUtils.DATE_FORMAT), queryTo.toString(DateTimeUtils.DATE_FORMAT),
				packageCode, queryFrom.toString(DateTimeUtils.DATE_FORMAT), queryTo.toString(DateTimeUtils.DATE_FORMAT));
		
		for (Map values : results) {
			lastMonthEndingMsisdn.add((String) values.get("msisdn"));
		}
		
		List<MBFTransaction> resultList = mobifoneReportRepository.getJdbc().query(thisMonthTransactionQuery, new MBFTransaction.Mapper(),
				packageCode, from.toString(DateTimeUtils.DATE_FORMAT), to.toString(DateTimeUtils.DATE_FORMAT));
		
		Map<String, List<MBFTransaction>> thisMonthTransaction = convertToMsisdnTransactionMap(resultList);
		
		int total = 0;
		int buyExtra2 = 0;
		List<MBFTransaction> allTransaction = new ArrayList<>();
		for (Map.Entry<String, List<MBFTransaction>> entry : thisMonthTransaction.entrySet()) {
			List<MBFTransaction> transactions = entry.getValue();
			boolean countNextSub = false;
			for (int i = 0; i < transactions.size(); i++) {
				MBFTransaction trans = transactions.get(i);
				if (trans.getTransactionCode().equals("EXTEND")) {
					allTransaction.add(trans);
//					alltrans.remove(trans.getTransactionId());
					total++;
				} else if (countNextSub && (trans.getTransactionCode().equals("SUBSCRIPTION") || trans.getTransactionCode().equals("CHANGE_SUBSCRIPTION"))) {
					buyExtra2++;
					allTransaction.add(trans);
//					alltrans.remove(trans.getTransactionId());
					countNextSub = false;
				} else if (trans.getTransactionCode().equals("CANCEL")) {
					if (i > 0 || lastMonthEndingMsisdn.contains(trans.getMsisdn()))
						countNextSub = true;
				}
			}
		}
		
		Map<String, Object> resultData = new HashMap<>();
		resultData.put("renew", total);
		resultData.put("buyExtra", buyExtra2);

//		FileWriter writer = null;
//		try {
//			writer = new FileWriter("/Users/antran/mobifonedata/" + from.toString("yyyy-MM") + "_renew.csv");
//
//			for (MBFTransaction trans : allTransaction) {
//				writer.append(trans.getTransactionId() + "," + trans.getMsisdn() + "," + trans.getTransactionCode() + "," + trans.getCreatedAt() + "\n");
//			}
//
//			writer.flush();
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[MobifoneReportService.getRenew] " + from.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return resultData;
	}
	
	private Map<String, Object> getCancel(String packageCode, DateTime from, DateTime to) {
		long timeStart = System.currentTimeMillis();
		String query =
				"SELECT count(*) AS result " +
						"FROM (SELECT " +
						"        t1.msisdn, " +
						"        t1.transactionCode " +
						"      FROM (SELECT " +
						"              t.msisdn, " +
						"              t.transactionCode, " +
						"              t.createdAt " +
						"            FROM hd1mbf_4g_db.transaction t " +
						"            WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
						"                  AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ?) t1 " +
						"        INNER JOIN (SELECT " +
						"                      t.msisdn, " +
						"                      MAX(t.createdAt) AS createdAt " +
						"                    FROM hd1mbf_4g_db.transaction t " +
						"                    WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
						"                          AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? " +
						"                    GROUP BY t.msisdn " +
						"                   ) t2 " +
						"          ON t1.msisdn = t2.msisdn AND t1.createdAt = t2.createdAt) allResult " +
						"WHERE allResult.transactionCode = 'CANCEL' ";
		String queryNew =
				"SELECT count(*) AS result " +
						"            FROM hd1mbf_4g_db.transaction t " +
						"            WHERE t.packageCode = ? AND status = 2 AND t.transactionCode IN ('CANCEL_RENEW', 'CANCEL') " +
						"                  AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ?";
		
		long totalCancel = mobifoneReportRepository.countByQuery(queryNew,
				packageCode, from.toString(DateTimeUtils.DATE_FORMAT), to.toString(DateTimeUtils.DATE_FORMAT));
		Map<String, Object> resultData = new HashMap<>();
		resultData.put("cancel", totalCancel);
		
		long timeEnd = System.currentTimeMillis();
		this.logger.info("[MobifoneReportService.getCancel] " + from.toString("yyyy-MM") + " DONE. duration=" + (timeEnd - timeStart));
		return resultData;
	}
	
	private Map<String, List<MBFTransaction>> convertToMsisdnTransactionMap(List<MBFTransaction> transactions) {
		Map<String, List<MBFTransaction>> result = new HashMap<>();
		for (MBFTransaction item : transactions) {
			List<MBFTransaction> userTransactionList = result.get(item.getMsisdn());
			if (userTransactionList == null) {
				userTransactionList = new ArrayList<>();
				result.put(item.getMsisdn(), userTransactionList);
			}
			userTransactionList.add(item);
		}
		return result;
	}
}
