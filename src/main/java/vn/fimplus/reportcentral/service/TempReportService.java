package vn.fimplus.reportcentral.service;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import vn.fimplus.reportcentral.model.report.TransactionReport;
import vn.fimplus.reportcentral.model.report.svod.SvodReport;
import vn.fimplus.reportcentral.util.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class TempReportService extends AbstractService {
	
	
	@Autowired
	@Qualifier("commonJDBC")
	private JdbcTemplate jdbc;
	
//	public SvodReport tempReport() {
//		List<TransactionReport> result = new ArrayList<>();
//
//		StringBuilder sb = new StringBuilder();
//		sb.append("SELECT  ");
//		sb.append("    * ");
//		sb.append("FROM hd1report_db.transaction_report t ");
//		sb.append("WHERE t.type = 'SVOD' AND t.status = 'success' ");
//		sb.append("      -- Account Type ");
//		sb.append("      -- AND t.methodName = 'Credit Card' AND t.itemId LIKE '%TRIAL%' -- trial ");
//		sb.append("      -- AND t.methodName IN ('Credit Card', 'MOMO') AND t.itemId NOT LIKE '%TRIAL%' -- Auto Renew ");
//		sb.append("      -- AND t.methodName NOT IN ('Credit Card', 'MOMO', 'MOBIFONE')  -- Non-Auto Renew ");
//		sb.append("      -- AND t.methodName IN ('MOBIFONE')  -- MBF ");
//		sb.append("      -- Package Type ");
//		sb.append("      -- AND t.name = '' -- type (basic, premium) ");
//		sb.append("      AND t.createdAt BETWEEN ? AND ?");
//		sb.append("ORDER BY t.createdAt");
//
//		try {
//			result = jdbc.query(sb.toString(),
//					new Object[]{DateTimeUtils.toUTCString(startDate), DateTimeUtils.toUTCString(endDate)},
//					new TransactionReport.Mapper());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return result;
//	}
}
