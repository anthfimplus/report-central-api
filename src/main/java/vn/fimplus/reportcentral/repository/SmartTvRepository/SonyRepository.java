package vn.fimplus.reportcentral.repository.SmartTvRepository;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.model.report.SmartTvReport.SmartTvReport;
import vn.fimplus.reportcentral.repository.AbtractRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Repository
public class SonyRepository extends AbtractRepository {
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    @Qualifier("reportDataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("reportJDBC")
    private JdbcTemplate jdbc;

    @Autowired
    @Qualifier("reportNamedJDBC")
    private NamedParameterJdbcTemplate namedJdbc;

    @Autowired
    @Qualifier("localDataSource")
    private DataSource localDataSource;

    public DataSource getDataSource() {
        return dataSource;
    }

    public JdbcTemplate getJdbc() {
        return jdbc;
    }

    public NamedParameterJdbcTemplate getNamedJdbc() {
        return namedJdbc;
    }

    public List<SmartTvReport> getBundlingAsFirstTrans(String date){
        List<SmartTvReport> result = new ArrayList<>();
        String sql = "SELECT " +
                "t.id, " +
                "t.transactionId, " +
                "t.userId, " +
                "ADDDATE(MIN(t.createdAt), INTERVAL 7 HOUR) AS createdAt, " +
                "t.itemId, " +
                "t.name, " +
                "t.methodName, " +
                "t.expiryDate, " +
                "t.platform, " +
                "t.deviceName, " +
                "p.groupContentId, " +
                "t.activeCode " +
                "FROM hd1report_db.transaction_report t " +
                "LEFT JOIN hd1billing_db.package AS p ON p.id = t.itemId " +
                "WHERE " +
                "ADDDATE(t.createdAt, INTERVAL 7 HOUR) BETWEEN '2016-11-01' AND ? " +
                "AND t.status = 'success' " +
                "AND ((t.activeCode IS NOT NULL AND t.codePartner = 'sony_smartTV') OR " +
                "t.itemId IN ('4853d9bd-f6c5-4748-aa8a-8fecd6844c38')) " +
                "AND NOT (methodName = 'Voucher' AND itemId LIKE '%tvod%') " +
                "AND t.type = 'SVOD' " +
                "AND NOT EXISTS(SELECT 1 " +
                "FROM hd1report_db.transaction_report AS t1 " +
                "WHERE t1.`status` = 'success' AND t1.type = 'SVOD' " +
                "AND t1.userId = t.userId AND t1.createdAt < t.createdAt) " +
                "GROUP BY t.userId;";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, date);
            ResultSet rs = ps.executeQuery();
            SmartTvReport.Mapper mapper = new SmartTvReport.Mapper();
            while (rs.next()) {
                result.add(mapper.mapRow(rs, 0));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null)
                try {
                    conn.close();
                } catch (Exception e) {}
            }
        return result;
    }

    public List<SmartTvReport> getAllBundlingReport(String date){
        List<SmartTvReport> result = new ArrayList<>();
        String sql = "SELECT " +
                "t.id, " +
                "t.transactionId, " +
                "t.userId, " +
                "ADDDATE(t.createdAt, INTERVAL 7 HOUR) AS createdAt, " +
                "t.itemId, " +
                "t.name, " +
                "t.methodName, " +
                "t.expiryDate, " +
                "t.platform, " +
                "t.deviceName, " +
                "t.activeCode, " +
                "t.codePartner, " +
                "p.groupContentId " +
                "FROM hd1report_db.transaction_report t " +
                "LEFT JOIN hd1billing_db.package AS p ON p.id = t.itemId " +
                "WHERE ADDDATE(t.createdAt, INTERVAL 7 HOUR) BETWEEN '2016-11-01' AND ? " +
                "AND t.status = 'success' " +
                "AND ((t.activeCode IS NOT NULL AND t.codePartner = 'sony_smartTV') OR " +
                "t.itemId IN ('4853d9bd-f6c5-4748-aa8a-8fecd6844c38')) " +
                "AND NOT (t.methodName = 'Voucher' AND t.itemId LIKE '%tvod%') " +
                "AND t.type = 'SVOD';";
        Connection conn = null;
        try{
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, date);
            ResultSet rs = ps.executeQuery();
            SmartTvReport.Mapper mapper = new SmartTvReport.Mapper();
            while(rs.next()){
                result.add(mapper.mapRow(rs,0));
            }
        }catch (SQLException e){
            throw new RuntimeException(e);
        }finally {
            if(conn != null)
                try{
                    conn.close();
                }catch (SQLException e){}
        }
        return result;
    }

    public List<SmartTvReport> getIndependentFromBundlingReport(String date) {
        List<SmartTvReport> result = new ArrayList<>();
        String sql = "SELECT " +
                "r.id, " +
                "r.transactionId, " +
                "r.userId, " +
                "ADDDATE(MIN(r.createdAt), INTERVAL 7 HOUR) AS createdAt, " +
                "r.itemId, " +
                "r.name, " +
                "r.methodName, " +
                "r.expiryDate, " +
                "r.platform, " +
                "r.deviceName, " +
                "p.groupContentId " +
                "FROM hd1report_db.transaction_report AS r " +
                "JOIN ( " +
                "SELECT " +
                "userId, " +
                "createdAt " +
                "FROM hd1report_db.transaction_report " +
                "WHERE " +
                "ADDDATE(createdAt, INTERVAL 7 HOUR) BETWEEN '2016-11-01' AND ? " +
                "AND status = 'success' " +
                "AND ((activeCode IS NOT NULL AND codePartner = 'sony_smartTV') OR " +
                "itemId IN ('4853d9bd-f6c5-4748-aa8a-8fecd6844c38')) " +
                "AND NOT (methodName = 'Voucher' AND itemId LIKE '%tvod%') " +
                "AND type = 'SVOD' " +
                ") AS t ON t.userId = r.userId AND t.createdAt < r.createdAt " +
                "LEFT JOIN hd1billing_db.package AS p ON p.id = r.itemId " +
                "WHERE r.`status` = 'success' AND r.type = 'SVOD' " +
                "AND r.activeCode IS NULL " +
                "AND r.itemId NOT IN ('4853d9bd-f6c5-4748-aa8a-8fecd6844c38') " +
                "AND ADDDATE(r.createdAt, INTERVAL 7 HOUR) BETWEEN '2016-11-01' AND ? " +
                "AND NOT (methodName = 'Voucher' AND itemId LIKE '%tvod%') " +
                "GROUP BY r.userId;";
        Connection conn = null;
        try{
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, date);
            ps.setString(2, date);
            ResultSet rs = ps.executeQuery();
            SmartTvReport.Mapper mapper = new SmartTvReport.Mapper();
            while(rs.next()){
                result.add(mapper.mapRow(rs,0));
            }
        }catch (SQLException e){
            throw new RuntimeException(e);
        }finally {
            if(conn != null)
                try {
                    conn.close();
                }catch (SQLException e){}
        }
        return result;
    }

    public Map<String, Map<String, Integer>> getA30Report(){
        Map<String, Map<String, Integer>> result = new TreeMap<>();
        String sql = "SELECT createdAt, " +
                "t.activeAt AS activeAt, " +
                "if(t.activeAt >= createdAt, count(*), 0) AS count " +
                "FROM(SELECT a30.userId, " +
                "s.month AS createdAt, " +
                "a30.month AS activeAt " +
                "FROM a30 " +
                "INNER JOIN newuser_sony s " +
                "ON a30.userId = s.userId " +
                "order by createdAt, a30.userId) t " +
                "GROUP BY createdAt, activeAt " +
                "ORDER BY createdAt";
        Connection conn = null;
        try{
            conn = localDataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String createAt = rs.getString("createdAt") + "";
                createAt = createAt.substring(0, 4) + "-" + createAt.substring(4);
                String activeAt = rs.getString("activeAt") + "";
                activeAt = activeAt.substring(0, 4) + "-" + activeAt.substring(4);
                if(result.containsKey(createAt)){
                    result.get(createAt).put(activeAt, rs.getInt("count"));
                }
                else {
                    Map<String, Integer> monthly = new TreeMap<>();
                    monthly.put(activeAt, rs.getInt("count"));
                    result.put(createAt, monthly);
                }
            }
        }catch (SQLException e){
            throw new RuntimeException(e);
        }finally {
            if(conn != null)
                try {
                    conn.close();
                }catch (SQLException e){}
        }
        return result;
    }
}
