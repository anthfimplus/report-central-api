package vn.fimplus.reportcentral.repository;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.model.report.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class UserReportRepository extends AbtractRepository{
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    @Qualifier("reportDataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("localDataSource")
    private DataSource localDataSource;

    @Autowired
    @Qualifier("reportJDBC")
    private JdbcTemplate jdbc;

    @Autowired
    @Qualifier("reportNamedJDBC")
    private NamedParameterJdbcTemplate namedJdbc;

    public DataSource getDataSource() {
        return dataSource;
    }

    public JdbcTemplate getJdbc() {
        return jdbc;
    }

    public NamedParameterJdbcTemplate getNamedJdbc() {
        return namedJdbc;
    }

    public User getUserById(String userId){
        User result = null;
        String sql = "SELECT * " +
                "FROM hd1cas_db.User " +
                "WHERE id LIKE ?";
        Connection conn = null;
        try{
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + userId);
            ResultSet rs = ps.executeQuery();
            User.Mapper mapper = new User.Mapper();
            while(rs.next()){
                result = mapper.mapRow(rs, 0);
            }
        }catch(SQLException e){
            throw new RuntimeException(e);
        }finally {
            if(conn != null){
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
        return result;
    }

    public List<String> getList(){
        List<String> result = new ArrayList<>();
        String sql = "SELECT t.uid " +
                "FROM  home_click t ";
        String sql2 = "SELECT uid FROM mbf";
        Connection conn = null;
        Map<String, Integer> target = new HashMap<>();
        try{
            conn = localDataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql2);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
               target.put(rs.getString("uid"),1);
            }
            ps.close();
            rs.close();
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                if(target.containsKey(rs.getString("uid")))
                    result.add(rs.getString("uid"));
            }
        }catch (SQLException e){
            throw new RuntimeException(e);
        }finally {
            {
                if(conn != null){
                    try{
                        conn.close();
                    } catch (SQLException e){}
                }
            }
        }
        return result;
    }
}
