package vn.fimplus.reportcentral.repository;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.util.DateTimeUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

@Repository
public class MobifoneReportRepository extends AbtractRepository {
	
	@Autowired
	@Qualifier("reportDataSource")
	private DataSource dataSource;
	
	@Autowired
	@Qualifier("reportJDBC")
	private JdbcTemplate jdbc;
	
	
	@Autowired
	@Qualifier("reportNamedJDBC")
	private NamedParameterJdbcTemplate namedJdbc;
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	public JdbcTemplate getJdbc() {
		return jdbc;
	}
	
	@Override
	public NamedParameterJdbcTemplate getNamedJdbc() {
		return namedJdbc;
	}
	
	private final static String GET_ENDING_COUNT_QUERY =
			"SELECT count(*) AS result " +
					"FROM (SELECT " +
					"        t1.msisdn, " +
					"        t1.transactionCode " +
					"      FROM (SELECT " +
					"              t.msisdn, " +
					"              t.transactionCode, " +
					"              t.createdAt " +
					"            FROM hd1mbf_4g_db.transaction t " +
					"            WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
					"                  AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ?) t1 " +
					"        INNER JOIN (SELECT " +
					"                      t.msisdn, " +
					"                      MAX(t.createdAt) AS createdAt " +
					"                    FROM hd1mbf_4g_db.transaction t " +
					"                    WHERE t.packageCode = ? AND status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
					"                          AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? " +
					"                    GROUP BY t.msisdn) t2 " +
					"          ON t1.msisdn = t2.msisdn AND t1.createdAt = t2.createdAt) allResult " +
					"WHERE allResult.transactionCode <> 'CANCEL' ";
	
	
	public long getRemainSubscription(String packageCode, DateTime from, DateTime to) {
		return this.countByQuery(GET_ENDING_COUNT_QUERY,
				packageCode, from.toString(DateTimeUtils.DATE_FORMAT), to.toString(DateTimeUtils.DATE_FORMAT),
				packageCode, from.toString(DateTimeUtils.DATE_FORMAT), to.toString(DateTimeUtils.DATE_FORMAT));
	}
	
	/**
	 * Count by custom query, with packageCode, year, month.
	 * Select must have `result` as result value
	 * Ex: "Select count(*) as `result` from `tableA`"
	 *
	 * @param query   Query string
	 * @param objects Param list
	 * @return `result` value
	 */
	public long countByQuery(String query, Object... objects) {
		Map<String, Object> result = null;
		result = jdbc.queryForMap(query, objects);
		return (long) result.get("result");
	}
	
	public int countByQuery2(String query, String packageCode, DateTime from, DateTime to) {
		Connection conn = null;
		int result = 0;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setString(1, packageCode);
			ps.setString(2, from.toString(DateTimeUtils.DATE_FORMAT));
			ps.setString(3, to.toString(DateTimeUtils.DATE_FORMAT));
			ResultSet resultSet = ps.executeQuery();
			if (resultSet.next()) {
				result = resultSet.getInt("result");
			}
			resultSet.close();
			ps.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
			
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		
		return result;
	}
}
