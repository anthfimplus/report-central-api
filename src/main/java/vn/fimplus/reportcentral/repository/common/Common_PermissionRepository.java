package vn.fimplus.reportcentral.repository.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.model.common.Common_Permission;

import java.util.ArrayList;
import java.util.List;

@Repository
public class Common_PermissionRepository {
	
	@Autowired
	@Qualifier("commonJDBC")
	private JdbcTemplate jdbc;
	
	public List<Common_Permission> getAllPermissions() {
		List<Common_Permission> result = new ArrayList<>();
		try {
			String query = "SELECT * FROM `permission`";
			result = jdbc.query(query, new Common_Permission.Mapper());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
