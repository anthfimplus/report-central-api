package vn.fimplus.reportcentral.repository.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.model.common.Common_User;

@Repository
public class Common_UserRepository {
	
	@Autowired
	@Qualifier("commonJDBC")
	private JdbcTemplate jdbc;
	
	public Common_User findUserByUsername(String username) {
		Common_User result = null;
		try {
			String query = "SELECT * FROM `user` WHERE username = ?";
			result = jdbc.queryForObject(query, new Object[]{username}, new Common_User.Mapper());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
