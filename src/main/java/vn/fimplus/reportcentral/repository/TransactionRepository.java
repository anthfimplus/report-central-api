package vn.fimplus.reportcentral.repository;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.model.report.TransactionOppo;
import vn.fimplus.reportcentral.model.report.TransactionReport;
import vn.fimplus.reportcentral.util.DateTimeUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class TransactionRepository extends AbtractRepository {
	
	private static final String CRITERIA_TRIAL    = " AND t.methodName = 'Credit Card' AND t.itemId LIKE '%TRIAL%' ";
	private static final String CRITERIA_AUTO     = " AND t.methodName IN ('Credit Card', 'MOMO') AND t.itemId NOT LIKE '%TRIAL%' -- Auto Renew ";
	private static final String CRITERIA_NON_AUTO = " t.methodName NOT IN ('Credit Card', 'MOMO', 'MOBIFONE')  -- Non-Auto Renew ";
	private static final String CRITERIA_MBF      = " AND t.methodName IN ('MOBIFONE')  -- MBF ";
	
	public static final int SVOD_TYPE_TRIAL    = 0;
	public static final int SVOD_TYPE_AUTO     = 1;
	public static final int SVOD_TYPE_NON_AUTO = 2;
	public static final int SVOD_TYPE_MBF      = 3;

	private static String sql_newUser = "SELECT userId " +
			"FROM hd1cas_db.User" +
			"WHERE createdAt >= '2018-07-01' + interval 7 HOUR;";
	
	@Autowired
	@Qualifier("reportDataSource")
	private DataSource dataSource;
	
	@Autowired
	@Qualifier("reportJDBC")
	private JdbcTemplate jdbc;
	
	@Autowired
	@Qualifier("reportNamedJDBC")
	private NamedParameterJdbcTemplate namedJdbc;
	
	public DataSource getDataSource() {
		return dataSource;
	}
	
	public JdbcTemplate getJdbc() {
		return jdbc;
	}
	
	public NamedParameterJdbcTemplate getNamedJdbc() {
		return namedJdbc;
	}
	
	public List<TransactionReport> findSVODByDate(DateTime startDate, DateTime endDate) {
		List<TransactionReport> result = new ArrayList<>();
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT  ");
		sb.append("    t.id, ");
		sb.append("    t.transactionId, ");
		sb.append("    t.userId, ");
		sb.append("    DATE_ADD(t.createdAt, INTERVAL 7 HOUR), as createdAt");
		sb.append("    t.itemId, ");
		sb.append("    t.name, ");
		sb.append("    t.methodName, ");
		sb.append("    DATE_ADD(t.expiryDate, INTERVAL 7 HOUR), as expiryDate");
		sb.append("    t.platform, ");
		sb.append("    t.activeCode, ");
		sb.append("    t.codePartner ");
		sb.append("FROM hd1report_db.transaction_report t ");
		sb.append("WHERE t.type = 'SVOD' AND t.status = 'success' ");
		sb.append("      -- Account Type ");
		sb.append("      -- AND t.methodName = 'Credit Card' AND t.itemId LIKE '%TRIAL%' -- trial ");
		sb.append("      -- AND t.methodName IN ('Credit Card', 'MOMO') AND t.itemId NOT LIKE '%TRIAL%' -- Auto Renew ");
		sb.append("      -- AND t.methodName NOT IN ('Credit Card', 'MOMO', 'MOBIFONE')  -- Non-Auto Renew ");
		sb.append("      -- AND t.methodName IN ('MOBIFONE')  -- MBF ");
		sb.append("      -- Package Type ");
		sb.append("      -- AND t.name = '' -- type (basic, premium) ");
		sb.append("      AND t.createdAt BETWEEN ? AND ?");
		sb.append("ORDER BY t.createdAt");
		
		try {
			result = jdbc.query(sb.toString(),
					new Object[]{DateTimeUtils.toUTCString(startDate), DateTimeUtils.toUTCString(endDate)},
					new TransactionReport.Mapper());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public List<TransactionReport> getPartnerSVODData(DateTime from, DateTime to) {
		return getPartnerData("SVOD", from, to);
	}
	
	public List<TransactionReport> getPartnerTVODData(DateTime from, DateTime to) {
		return getPartnerData("TVOD", from, to);
	}
	
	public List<TransactionReport> getPartnerData(String type, DateTime startDate, DateTime endDate) {
		List<TransactionReport> result = new ArrayList<>();
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT  ");
		sb.append("    t.id, ");
		sb.append("    t.transactionId, ");
		sb.append("    t.userId, ");
		sb.append("    t.createdAt, ");
		sb.append("    t.itemId, ");
		sb.append("    t.name, ");
		sb.append("    t.methodName, ");
		sb.append("    t.expiryDate, ");
		sb.append("    t.platform, ");
		sb.append("    t.activeCode, ");
		sb.append("    t.codePartner ");
		sb.append("FROM hd1report_db.transaction_report t ");
		sb.append("WHERE t.type = :type AND t.status = 'success' ");
		//sb.append("      AND ((t.activeCode IS NOT NULL AND t.codePartner = 'samsung_smartTV') OR itemId IN ('cac38cc6-29ee-4b89-902d-0241bef2c8b5')) -- bundling ");
		sb.append("      AND (t.createdAt BETWEEN :startDate AND :endDate OR t.expiryDate BETWEEN :startDate AND :endDate)");
		
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("type", type);
			((MapSqlParameterSource) namedParameters).addValue("startDate", DateTimeUtils.toUTCString(startDate));
			((MapSqlParameterSource) namedParameters).addValue("endDate", DateTimeUtils.toUTCString(endDate));
			result = namedJdbc.query(sb.toString(), namedParameters, new TransactionReport.Mapper());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public List<String> checkPrevSVOD(DateTime startDate, int svodType, List<String> userIdToCheck) {
		List<String> result = new ArrayList<>();
		
		String svodTypeCriteria = "";
		switch (svodType) {
			case SVOD_TYPE_TRIAL:
				svodTypeCriteria = CRITERIA_TRIAL;
				break;
			case SVOD_TYPE_AUTO:
				svodTypeCriteria = CRITERIA_AUTO;
				break;
			case SVOD_TYPE_NON_AUTO:
				svodTypeCriteria = CRITERIA_NON_AUTO;
				break;
			case SVOD_TYPE_MBF:
				svodTypeCriteria = CRITERIA_MBF;
				break;
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT  ");
		sb.append("    t1.userId   ");
		sb.append("FROM (SELECT ");
		sb.append("          t.userId, ");
		sb.append("          t.createdAt ");
		sb.append("      FROM hd1report_db.transaction_report t ");
		sb.append("      WHERE ");
		sb.append("          t.type = 'SVOD' AND t.status = 'success' ");
		sb.append(svodTypeCriteria);
		sb.append("          AND t.createdAt < :startDate ");
		sb.append("          AND t.userId IN (:userIdList)) t1 ");
		sb.append("    INNER JOIN (SELECT ");
		sb.append("                    t.userId, ");
		sb.append("                    MAX(t.createdAt) AS createdAt ");
		sb.append("                FROM hd1report_db.transaction_report t ");
		sb.append("                WHERE t.type = 'SVOD' AND t.status = 'success' ");
		sb.append(svodTypeCriteria);
		sb.append("                      AND t.createdAt < :startDate ");
		sb.append("                      AND t.userId IN (:userIdList) ");
		sb.append("                GROUP BY t.userId) t2 ");
		sb.append("        ON t1.userId = t2.userId AND t1.createdAt = t2.createdAt ");
		sb.append("ORDER BY t1.createdAt ");
		
		try {
			SqlParameterSource namedParameters = new MapSqlParameterSource("userIdList", userIdToCheck);
			((MapSqlParameterSource) namedParameters).addValue("startDate", DateTimeUtils.toUTCString(startDate));
			result = namedJdbc.queryForList(sb.toString(), namedParameters, String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String getPackageName(String id, String date){
		String sql = "SELECT groupContentId " +
					"FROM ( " +
						"SELECT * " +
							"FROM hd1billing_db.transaction " +
							"WHERE " +
								"userId = ? " +
								"AND paymentResult = 'success' AND status in (8,19,21) " +
								"AND createdAt < ? " +
								"AND expiryDate > ? " +
                                "AND packageId is not null " +
							"ORDER BY createdAt desc " +
							"LIMIT 1 " +
						") t1 " +
						"INNER JOIN ( " +
							"SELECT id, groupContentId " +
							"FROM hd1billing_db.package " +
						") p " +
						"ON t1.packageId = p.id;";
		String result = null;
		Connection conn = null;
		try{
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, id);
			ps.setString(2, date);
			ps.setString(3, date);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
			    String packageCode = rs.getString("groupContentId");
				if(packageCode.equals("G001"))
				    result = "1-month-basic";
				else
				    result = "1-month-premium";
			}
		}catch (SQLException e){
			throw new RuntimeException(e);
		}finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
		return result;
	}

	public List<TransactionOppo> getOppoReport(){
		String sql = "SELECT t.id, " +
				"t.userId, " +
				"t.platform, " +
				"t.createdAt, " +
				"t.itemId, " +
				"t.expiryDate " +
				"FROM hd1billing_db.transaction t " +
				"WHERE t.packageId = 'oppo-3-month-basic' " +
				"AND t.status = 8;";
		List<TransactionOppo> result = new ArrayList<>();
		Connection conn = null;
		try{
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			TransactionOppo.Mapper mapper = new TransactionOppo.Mapper();
			while(rs.next()){
				result.add(mapper.mapRow(rs,0));
			}
		}catch (SQLException e){
			throw new RuntimeException(e);
		}finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
		return result;
	}

	public Map<String, Integer> getNonTransReport(Map<String,Integer> aMap){
		String sql = "SELECT t.id, " +
				"t.userId, " +
				"t.platform, " +
				"t.createdAt, " +
				"p.groupContentId " +
				"FROM ( " +
				"SELECT temp.*, " +
				"s.type " +
				"FROM ( " +
				"SELECT * " +
				"FROM hd1billing_db.transaction " +
				"WHERE createdAt between '2018-07-01 00:00:00' + interval 7 HOUR AND '2018-08-26 23:59:59' + interval 7 HOUR " +
				"AND status = 8 " +
				") temp " +
				"LEFT JOIN hd1billing_db.subscription_v2 s " +
				"ON temp.id = s.lastSuccessTransactionId " +
				"WHERE s.type = 'packageAll' " +
				") t " +
				"LEFT JOIN hd1billing_db.package p " +
				"ON t.packageId = p.id " +
				"WHERE t.platform is not null;";
		Connection conn = null;
		Map<String, Integer> result = new HashMap<>();
		try{
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				String userId = rs.getString("userId");
				if(aMap.containsKey(userId) && rs.getString("groupContentId").equals("G001")){
					if(result.containsKey("basic"))
						result.put("basic", result.get("basic") + 1);
					else
						result.put("basic",0);
				}

				else if(aMap.containsKey(userId) && rs.getString("groupContentId").equals("G002")){
					if(result.containsKey("premium"))
						result.put("premium", result.get("premium") + 1);
					else
						result.put("premium",0);
				}

			}
		}catch (SQLException e){
			throw new RuntimeException(e);
		}finally {
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}
		return result;
	}

	public Map<String, Map<String, Integer>> getDailyReport() {
		DateTime startDate = new DateTime().withDate(2018, 7, 1);
		DateTime endDate = new DateTime().withDate(2018, 8, 26);
		DateTime currentDate = startDate;
		Map<String, Map<String, Integer>> result = new TreeMap<>();
		while (currentDate.getMillis() <= endDate.getMillis()) {
			String sql = "SELECT t1.userId," +
					"p.groupContentId " +
					"FROM ( " +
					"SELECT temp.*, " +
					"s.type " +
					"FROM ( " +
					"SELECT * " +
					"FROM hd1billing_db.transaction " +
					"WHERE createdAt between ? + interval 7 HOUR AND ? + interval 7 HOUR " +
					"AND status = 8 " +
					") temp " +
					"LEFT JOIN hd1billing_db.subcription_v2 s " +
					"ON temp.id = s.lastSuccessTransactionId " +
					"WHERE s.type = 'packageAll' AND s.packageId = 'F10'" +
					") t1 " +
					"LEFT JOIN hd1billing_db.package p " +
					"ON t1.packageId = p.id " +
					"WHERE platform is not null " +
					"AND t1.userId in (SELECT User.id " +
					"FROM hd1cas_db.User " +
					"WHERE createdAt >= '2018-07-01' + interval 7 HOUR);";

			Connection conn = null;
			try {
				conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, currentDate.toString("yyyy-MM-dd"));
				ps.setString(2, currentDate.plusDays(1).toString("yyyy-MM-dd"));
				ResultSet rs = ps.executeQuery();
				String dateString = currentDate.toString("yyyy-MM-dd");
				while (rs.next()) {
					Map<String, Integer> temp = result.get(dateString);
					if (temp == null) {
						temp = new TreeMap<>();
						result.put(dateString, temp);
					}
					if(rs.getString("groupContentId").equals("G001")){
						if(temp.containsKey("basic"))
							temp.put("basic", temp.get("basic")+1);
						else
							temp.put("basic",1);
					}
					else{
						if(temp.containsKey("premium"))
							temp.put("premium", temp.get("premium") +1);
						else
							temp.put("premium", 1);
					}
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
					}
				}
			}
			currentDate = currentDate.plusDays(1);
		}
		return result;
	}
}
