package vn.fimplus.reportcentral.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class AbtractRepository {
	
	public abstract DataSource getDataSource();
	
	public abstract JdbcTemplate getJdbc();
	
	public abstract NamedParameterJdbcTemplate getNamedJdbc();
}
