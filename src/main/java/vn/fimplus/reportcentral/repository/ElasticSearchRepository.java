package vn.fimplus.reportcentral.repository;

import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.Client;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.cardinality.Cardinality;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.util.HttpJsonUtils;

import java.util.*;

import static org.elasticsearch.index.query.FilterBuilders.boolFilter;
import static org.elasticsearch.index.query.FilterBuilders.rangeFilter;


@Repository
public class ElasticSearchRepository {

    public Map<String, Integer> getUserList(Client client, String dateList, Long start, Long end){
        Map<String, Integer> result = new TreeMap<>();
        FilteredQueryBuilder filtered = QueryBuilders.filteredQuery(
                QueryBuilders.queryStringQuery("*").analyzeWildcard(true),
                boolFilter().must(rangeFilter("timehost").gt(start.toString()).lt(end.toString())));

        SearchResponse response = client.prepareSearch(dateList)
                .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                .setTypes("access")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(filtered)
                    .addAggregation(AggregationBuilders.cardinality("1").field("uid"))
                .setFrom(0).setSize(60).setExplain(true)
                .execute()
                .actionGet();
        return result;
    }

    public List<String> getUniqueUserByGenre(Client client, String[] dateList, Long start, Long end, String genre){
        String query = "";
        switch (genre){
            case "action":
                query = "genres: (\"Hành Động\" \"Hành Động\") AND NOT is_trailer: \"1\"";
                break;
            case "romance":
                query = "genres:(\"Lãng Mạn\" \"Lãng Mạn\") AND NOT is_trailer: \"1\"";
                break;
            case "series":
                query = "movieType: \"bo\" AND NOT is_trailer: \"1\"";
                break;
            case "independent":
                query = "movieType: \"le\" AND NOT is_trailer: \"1\"";
                break;
            case "other":
                query = "NOT genres: (\"Hành Động\" \"Hành Động\" \"Lãng Mạn\" \"Lãng Mạn\" \"Phim Việt\") AND NOT is_trailer: \"1\"";
                break;
            case "viet":
                query = "genres: \"Phim Việt\" AND NOT is_trailer: \"1\"";
                break;
        }
        List<String> result = new ArrayList<>();
        FilteredQueryBuilder filtered = QueryBuilders.filteredQuery(
                QueryBuilders.queryStringQuery(query).analyzeWildcard(true),
                boolFilter().must(rangeFilter("timeshost").gt(start.toString()).lt(end.toString())));

        SearchResponse response = client.prepareSearch(dateList)
                .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                .setTypes("access")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(filtered)
                    .addAggregation(AggregationBuilders.terms("1").field("uid").size(0))
                .setFrom(0).setSize(0).setExplain(true)
                .execute()
                .actionGet();

        if(response.getHits().getTotalHits() != 0){
            Terms terms = response.getAggregations().get("1");
            List<Terms.Bucket> buckets = terms.getBuckets();
            for(Terms.Bucket aBucket : buckets){
                String uid = aBucket.getKey();
                result.add(uid);
            }
        }
        return result;
    }

    public int getTimeStampById(Client client, String name, String[] datelist, Long start, Long end, String varName){
        int result = 0;
        FilteredQueryBuilder filtered = QueryBuilders.filteredQuery(
                QueryBuilders.queryStringQuery(varName + ": \"" + name + "\" AND pricetype: \"SVOD\"")
                        .analyzeWildcard(true),
                boolFilter().must(rangeFilter("timeshost").gt(start.toString()).lt(end.toString())));

        SearchResponse response = client.prepareSearch(datelist)
                .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                .setTypes("access")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(filtered)
                    .addAggregation(AggregationBuilders.sum("1").field("timechunk"))
                .setFrom(0).setSize(0).setExplain(true)
                .execute()
                .actionGet();

        if(response.getHits().getTotalHits() != 0){
            Sum timewatch = response.getAggregations().get("1");
            Double timewatchValue = timewatch.getValue();
            result = timewatchValue.intValue();
        }
        return result;
    }

    public Map<String, Map<String, Integer>> getpremiumPackage(Client client, String name, String[] dateList, Long start, Long end, String varName){
        Map<String, Map<String, Integer>> result = new TreeMap<>();

        FilteredQueryBuilder filtered = QueryBuilders.filteredQuery(
                QueryBuilders.queryStringQuery(varName + ": \"" + name +"\" AND (_missing_:packagename OR packagename: (\"1-month-premium\")) AND NOT is_trailer:\"1\" AND NOT agent:\"python-requests/2.8.1\" AND NOT clientip:[\"103.205.104.0\" TO \"103.205.107.255\"] AND NOT clientip:[\"118.69.169.192\" TO \"118.69.169.255\"] AND NOT clientip:\"210.245.18.158\" AND NOT clientip:\"210.245.122.197\"")
                        .analyzeWildcard(true),
                boolFilter().must(rangeFilter("timeshost").gt(start.toString()).lt(end.toString())));

        SearchResponse response = client.prepareSearch(dateList)
                .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                .setTypes("access")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(filtered)
                    .addAggregation(AggregationBuilders.cardinality("1").field("uid"))
                    .addAggregation(AggregationBuilders.sum("2").field("timechunk"))
                .setFrom(0).setSize(60).setExplain(true)
                .execute()
                .actionGet();

        HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/_cache/clear", "");

        if(response.getHits().getTotalHits() != 0) {
            Map<String, Integer> count = new TreeMap<>();
            Cardinality uid = response.getAggregations().get("1");
            Long uidValue = uid.getValue();
            Sum timewatch = response.getAggregations().get("2");
            Double timewatchValue = timewatch.getValue();
            count.put("uid", uidValue.intValue());
            count.put("watchTime", timewatchValue.intValue());
            result.put("1-month-premium", count);
        }
        return result;
    }

    public Map<String, Map<String, Integer>> getBasicPackage(Client client, String name, String[] dateList, Long start, Long end, String varName) {
        Map<String, Map<String, Integer>> result = new TreeMap<>();

        FilteredQueryBuilder filtered = QueryBuilders.filteredQuery(
                QueryBuilders.queryStringQuery(varName + ": \"" + name +"\" AND packagename: (\"1-month-basic\") AND NOT is_trailer:\"1\" AND NOT agent:\"python-requests/2.8.1\" AND NOT clientip:[\"103.205.104.0\" TO \"103.205.107.255\"] AND NOT clientip:[\"118.69.169.192\" TO \"118.69.169.255\"] AND NOT clientip:\"210.245.18.158\" AND NOT clientip:\"210.245.122.197\"")
                .analyzeWildcard(true),
                boolFilter().must(rangeFilter("timeshost").gt(start.toString()).lt(end.toString())));

        SearchResponse response = client.prepareSearch(dateList)
                .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                .setTypes("access")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(filtered)
                    .addAggregation(AggregationBuilders.cardinality("1").field("uid"))
                    .addAggregation(AggregationBuilders.sum("2").field("timechunk"))
                .setFrom(0).setSize(60).setExplain(true)
                .execute()
                .actionGet();

        HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/_cache/clear", "");

        if(response.getHits().getTotalHits() != 0) {
            Map<String, Integer> count = new TreeMap<>();
            Cardinality uid = response.getAggregations().get("1");
            Long uidValue = uid.getValue();
            Sum timewatch = response.getAggregations().get("2");
            Double timewatchValue = timewatch.getValue();
            count.put("uid", uidValue.intValue());
            count.put("watchTime", timewatchValue.intValue());
            result.put("1-month-basic", count);
        }
        return result;
    }

    public Map<String, Map<String, Integer>> getA90(Client client, String[] dateList,Long start, Long end) {
        FilteredQueryBuilder filtered = QueryBuilders.filteredQuery(
                QueryBuilders.queryStringQuery("packagename: \"1-month-basic\" AND NOT is_trailer:\"1\" AND NOT agent:\"python-requests/2.8.1\" AND NOT clientip:[\"103.205.104.0\" TO \"103.205.107.255\"] AND NOT clientip:[\"118.69.169.192\" TO \"118.69.169.255\"] AND NOT clientip:\"210.245.18.158\" AND NOT clientip:\"210.245.122.197\"")
                .analyzeWildcard(true),
                boolFilter().must(rangeFilter("timeshost").gt(start.toString()).lt(end.toString()))
        );

        SearchResponse response = client.prepareSearch(dateList)
                .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                .setTypes("access")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(filtered)
                    .addAggregation(AggregationBuilders.cardinality("1").field("uid"))
                .setFrom(0).setSize(60).setExplain(true)
                .execute()
                .actionGet();

        HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/_cache/clear", "");

        Map<String, Integer> count = new TreeMap<>();
        Map<String, Map<String, Integer>> result = new TreeMap<>();
        if(response.getHits().getTotalHits() != 0) {
            Cardinality uid = response.getAggregations().get("1");
            Long uidValue = uid.getValue();
            count.put("1-month-basic", uidValue.intValue());
        }

        filtered = null;
        response = null;

        filtered = QueryBuilders.filteredQuery(
                QueryBuilders.queryStringQuery("(_missing_:packagename OR packagename:\"1-month-premium\") AND NOT is_trailer:\"1\" AND NOT agent:\"python-requests/2.8.1\" AND NOT clientip:[\"103.205.104.0\" TO \"103.205.107.255\"] AND NOT clientip:[\"118.69.169.192\" TO \"118.69.169.255\"] AND NOT clientip:\"210.245.18.158\" AND NOT clientip:\"210.245.122.197\"")
                        .analyzeWildcard(true),
                boolFilter().must(rangeFilter("timeshost").gte(start.toString()).lte(end.toString()))
        );


        response = client.prepareSearch(dateList)
                .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                .setTypes("access")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(filtered)
                    .addAggregation(AggregationBuilders.cardinality("1").field("uid"))
                .setFrom(0).setSize(60).setExplain(true)
                .execute()
                .actionGet();

        HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/_cache/clear", "");

        if(response.getHits().getTotalHits() != 0) {
            Cardinality uid = response.getAggregations().get("1");
            Long uidValue = uid.getValue();
            count.put("1-month-premium", uidValue.intValue());
        }
        result.put("a90", count);
        return result;
    }

    public int getA30(Client client, String[] dateList, Long start, Long end){
        FilteredQueryBuilder filtered = QueryBuilders.filteredQuery(
                QueryBuilders.queryStringQuery("NOT agent:\"python-requests/2.8.1\" AND NOT clientip:[\"103.205.104.0\" TO \"103.205.107.255\"] AND NOT clientip:[\"118.69.169.192\" TO \"118.69.169.255\"] AND NOT clientip:\"210.245.18.158\" AND NOT clientip:\"210.245.122.197\"")
                        .analyzeWildcard(true),
                boolFilter().must(rangeFilter("timeshost").gt(start.toString()).lt(end.toString()))
        );

        SearchResponse response = client.prepareSearch(dateList)
                .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                .setTypes("access")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(filtered)
                    .addAggregation(AggregationBuilders.cardinality("1").field("uid"))
                .setFrom(0).setSize(60).setExplain(true)
                .execute()
                .actionGet();

        HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/_cache/clear", "");
        int result = 0;
        if(response.getHits().getTotalHits() != 0) {
            Cardinality uid = response.getAggregations().get("1");
            Long uidValue = uid.getValue();
             result = uidValue.intValue();
        }
        return result;
    }
}