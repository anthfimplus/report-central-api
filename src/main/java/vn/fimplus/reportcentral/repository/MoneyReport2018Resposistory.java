package vn.fimplus.reportcentral.repository;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.model.report.MoneyReport2018.SvodReport;
import vn.fimplus.reportcentral.model.report.MoneyReport2018.TvodReport;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class MoneyReport2018Resposistory extends AbtractRepository{

    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    @Qualifier("reportDataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("reportJDBC")
    private JdbcTemplate jdbc;

    @Autowired
    @Qualifier("reportNamedJDBC")
    private NamedParameterJdbcTemplate namedJdbc;

    public DataSource getDataSource() {
        return dataSource;
    }

    public JdbcTemplate getJdbc() {
        return jdbc;
    }

    public NamedParameterJdbcTemplate getNamedJdbc() {
        return namedJdbc;
    }

    public Map<String, Integer> packageCount(DateTime dateFrom, DateTime dateTo){
        Map<String, Integer> result = new HashMap<>();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
        String sql = "SELECT " +
                        "allResult.packageCode, " +
                        "count(allResult.transactionCode) AS countId " +
                    "FROM ( " +
                        "SELECT " +
                            "t1.msisdn, " +
                            "t1.transactionCode, " +
                            "t1.packageCode " +
                            "FROM ( " +
                                "SELECT " +
                                    "t.msisdn, " +
                                    "t.transactionCode, " +
                                    "t.createdAt, " +
                                    "t.packageCode " +
                                "FROM hd1mbf_4g_db.transaction t " +
                                "WHERE status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
                                    "AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ?) t1 " +
                                "INNER JOIN (" +
                                    "SELECT " +
                                        "t.msisdn, " +
                                        "MAX(t.createdAt) AS createdAt " +
                                    "FROM hd1mbf_4g_db.transaction t " +
                                    "WHERE status = 2 AND t.transactionCode <> 'CANCEL_RENEW' " +
                                        "AND DATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR)) BETWEEN ? AND ? " +
                                    "GROUP BY t.msisdn, t.packageCode) t2 " +
                                "ON t1.msisdn = t2.msisdn  AND t1.createdAt = t2.createdAt) allResult " +
                    "WHERE allResult.transactionCode <> 'CANCEL' " +
                    "group by allResult.packageCode;";
        Connection conn = null;
        try{
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, dateFrom.toString());
            ps.setString(2, dateTo.toString());
            ps.setString(3, dateFrom.toString());
            ps.setString(4, dateTo.toString());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                    result.put(rs.getString("packageCode"), rs.getInt("countId"));
            }
        }catch (SQLException e){
            logger.info("here");
            throw new RuntimeException(e);
        }finally {
            if(conn != null){
                try{
                    conn.close();
                }catch (SQLException e){}
            }
        }
        return result;
    }

    public List<TvodReport> tvod(DateTime dateFrom, DateTime dateTo){
        List<TvodReport> result = new ArrayList<>();
        String sql = "SELECT " +
                "tmp.id, " +
                "tmp.userId, " +
                "tmp.platform, " +
                "tmp.device_model, " +
                "tmp.uuid, " +
                "tmp.paymentObject, " +
                "tmp.paymentResult, " +
                "date_add(tmp.createdAt, INTERVAL 7 HOUR) AS createdAt, " +
                "tmp.itemId, " +
                "item.name, " +
                "tmp.shortTransactionId, " +
                "tmp.paymentChargeId, " +
                "tmp.transactionType " +
                "FROM " +
                "( " +
                "SELECT " +
                "t.*, " +
                "s.type AS transactionType " +
                "FROM hd1billing_db.transaction t " +
                "JOIN hd1billing_db.subscription_v2 s ON t.id = s.lastSuccessTransactionId " +
                "WHERE " +
                "s.type IN ('item', 'view-count') AND s.itemId IS NOT NULL " +
                "AND t.itemId IS NOT NULL AND t.createdAt >= ? AND t.createdAt < ? " +
                "AND t.status = 8 " +
                ") tmp " +
                "JOIN hd1billing_db.item ON tmp.itemId = item.id; ";
        Connection conn = null;
        try{
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, dateFrom.toString());
            ps.setString(2, dateTo.toString());
            ResultSet rs = ps.executeQuery();
            TvodReport.Mapper mapper = new TvodReport.Mapper();
            while(rs.next()){
                result.add(mapper.mapRow(rs, 0));
            }
        }catch (SQLException e){
            throw new RuntimeException(e);
        }finally {
            if(conn != null){
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
        return result;
    }

    public List<SvodReport> svod(DateTime dateParam){
        List<SvodReport> result = new ArrayList<>();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
        String sql =
                "SELECT " +
                "t.id AS transactionId, " +
                "t.userId, " +
                "ADDDATE(t.createdAt, INTERVAL 7 HOUR) AS createdAt, " +
                "t.packageId AS itemId, " +
                "t.status, " +
                "if(t.status NOT IN (8, 19, 21), NULL, IF(p.packageVisibility = 'mbf', 'MOBIFONE', 'Other')) AS methodName, " +
                "p.groupContentId, " +
                "ADDDATE(if(t.TransExpiryDate IS NULL, date_add(t.createdAt, INTERVAL if(t.packageId IS NOT NULL, p.length, 2) DAY), t.TransExpiryDate), INTERVAL 7 HOUR) AS expiryDate, " +
                "t.platform, " +
                "t.activeCode, " +
                "if(p.packageType = 1 AND p.campaignType = 'bundling', 'bundling', '') AS isBundling " +
//                "if(t.activeCode IS NOT NULL OR t.packageId IN ('4853d9bd-f6c5-4748-aa8a-8fecd6844c38', 'cac38cc6-29ee-4b89-902d-0241bef2c8b5', '1846638d-a3d3-4482-8419-aa170f39f3d6'), 'bundling', '') AS isBundling " +
                "FROM (" +
                "SELECT t.* " +
                "FROM hd1billing_db.subscription_v2 " +
                "LEFT JOIN (" +
                "SELECT tmp1.* " +
                "FROM (" +
                "SELECT " +
                "`transaction`.id, " +
                "`transaction`.userId, " +
                "`transaction`.createdAt, " +
                "`transaction`.packageId, " +
                "`transaction`.status, " +
                "`transaction`.paymentResult, " +
                "`transaction`.paymentChargeId, " +
                "`transaction`.paymentMethod, " +
                "`transaction`.paymentObject, " +
                "`transaction`.platform, " +
                "`transaction`.activeCode, " +
                "`transaction`.uuid, " +
                "`transaction`.expiryDate AS `TransExpiryDate` " +
                "FROM hd1billing_db.`transaction` " +
                "LEFT JOIN hd1billing_db.package " +
                "ON `transaction`.packageId = package.id " +
                "WHERE ADDDATE(`transaction`.createdAt, INTERVAL 7 HOUR) < ? " +
                "AND package.packageName <> 'tvod-voucher' " +
                "AND package.packageType = 1 " +
                "AND (`transaction`.status = 8 OR (`transaction`.status IN (19, 21) AND `transaction`.updatedAt >= ?)) " +
                "AND `transaction`.packageId IS NOT NULL) tmp1 " +
                "JOIN (" +
                "SELECT " +
                "userId, " +
                "max(createdAt) AS createdAt " +
                "FROM (" +
                "SELECT " +
                "`transaction`.createdAt, " +
                "`transaction`.userId " +
                "FROM hd1billing_db.`transaction` " +
                "LEFT JOIN hd1billing_db.package " +
                "ON `transaction`.packageId = package.id " +
                "WHERE ADDDATE(`transaction`.createdAt, INTERVAL 7 HOUR) < ? " +
                "AND package.packageName <> 'tvod-voucher' " +
                "AND package.packageType = 1 " +
                "AND (`transaction`.status = 8 OR (`transaction`.status IN (19, 21) AND `transaction`.updatedAt >= ?)) " +
                "AND `transaction`.packageId IS NOT NULL) tmp " +
                "GROUP BY userId) tmp2 " +
                "ON tmp1.userId = tmp2.userId " +
                "AND tmp1.createdAt = tmp2.createdAt) AS t " +
                "ON t.userId = subscription_v2.userId " +
                "WHERE ADDDATE(expiryDate, INTERVAL 7 HOUR) >= ? " +
                "AND type = 'packageAll' " +
                "AND t.id IS NOT NULL ORDER BY t.createdAt DESC) AS t " +
                "LEFT JOIN `hd1billing_db`.`package` p " +
                "ON p.id = t.packageId " +
                "WHERE (t.TransExpiryDate >= ? || (ADDDATE(ADDDATE(t.createdAt, INTERVAL 7 HOUR), INTERVAL p.length DAY) >= ?));";
        int count = 0;
        for(int i = 0; i < sql.length(); i++){
            if(sql.charAt(i) == '?')
                count++;
        }
        Connection conn =null;
        try{
            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            for(int i = 1; i <= count; i++){
                ps.setString(i,dateParam.toString(dtf));
            }
            ResultSet rs = ps.executeQuery();
            SvodReport.Mapper mapper = new SvodReport.Mapper();
            while(rs.next()){
                result.add(mapper.mapRow(rs, 0));
            }

        }
        /*try{
            SqlParameterSource parameterSource = new MapSqlParameterSource();
            ((MapSqlParameterSource)parameterSource).addValue("endDate", dateParam);
            result = namedJdbc.query(sql, parameterSource, new SvodReport.Mapper());
        }*/catch (SQLException e){
            throw new RuntimeException(e);
        }
        finally {
            if (conn != null){
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
        logger.info(result.toString());
        return result;
    }
}
