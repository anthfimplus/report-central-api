package vn.fimplus.reportcentral.repository;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Repository;
import vn.fimplus.reportcentral.model.report.UserActivityReport;
import vn.fimplus.reportcentral.util.HttpJsonUtils;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


@Repository
public class UserActivityRepository {

    protected final Log logger = LogFactory.getLog(getClass());

	public UserActivityReport getPlatformActivityByDate(long startDate, long endDate, String platform) throws IOException {
//		Node node = NodeBuilder.nodeBuilder().clusterName("yourclustername").node();
//		Client client = node.client();
//
//		SearchResponse response = client.prepareSearch("index1", "index2")
//				.setTypes("type1", "type2")
//				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//				.setQuery(QueryBuilders.termQuery("multi", "test"))             // Query
//				.setPostFilter(FilterBuilders.rangeFilter("age").from(12).to(18))   // Filter
//				.setFrom(0).setSize(60).setExplain(true)
//				.execute()
//				.actionGet();
		String query = "{\"size\": \"10000\", \"sort\": [{\"userId\": \"asc\"}, { \"@timestamp\": { \"order\": \"asc\", \"unmapped_type\": \"boolean\" } } ], \"query\": { \"filtered\": { \"query\": { \"query_string\": { \"analyze_wildcard\": true, \"query\": \"type: \\\"action\\\" AND platform: \\\"" + platform + "\\\"\"}}, \"filter\": { \"bool\": { \"must\": [ { \"range\": { \"@timestamp\": { \"gte\": 1506790800000, \"lte\": 1509382800000 } } }, {\"exists\" : {\"field\": \"userId\"}}], \"must_not\": [ {\"query\": {\"match\": {\"userId\": \"\"}}},{ \"query\": { \"match\": { \"event\": { \"query\": \"icon_notification\", \"type\": \"phrase\" } }} }, { \"query\": { \"match\": { \"event\": { \"query\": \"list_telco\", \"type\": \"phrase\" } } } } ] } } } }}";
		UserActivityReport report = new UserActivityReport();

        JsonNode result = HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/useractivity-*/action/_search?pretty&scroll=1m", query);
		while(result.get("hits").get("hits").size() > 0){
            report.getList(result);
		    result = HttpJsonUtils.sendJsonRequest("http://10.10.14.93:9200/_search/scroll?pretty&scroll=1m", result.get("_scroll_id").textValue());
		}
		return report.getUserActivityReport();
	}
}

