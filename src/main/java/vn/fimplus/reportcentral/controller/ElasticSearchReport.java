package vn.fimplus.reportcentral.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fimplus.reportcentral.service.ElasticSearch;
import vn.fimplus.reportcentral.service.UserReportService;
import vn.fimplus.reportcentral.util.ExcelExport;
import vn.fimplus.reportcentral.util.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/api/elasticsearch")
public class ElasticSearchReport {

    private static String directory = System.getProperty("user.dir");
    @Autowired
    ElasticSearch elasticSearch;
    @Autowired
    UserReportService userReportService;
    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value="testelastic", method = RequestMethod.GET)
    public ResponseEntity<?> test(){
        //Map<String, Map<String, Map<String, Integer>>> result = elasticSearch.test();
        return ResponseEntity.ok("");
    }

    @RequestMapping(value = "a90", method = RequestMethod.POST)
    public ResponseEntity<?> elasticSearchReport(@RequestBody Map<String,String> paramMap){
        long timeStart = System.currentTimeMillis();
        String type = paramMap.get("type");
        String fileName = directory + File.separator + "constant" + File.separator + "MovieDetail.xlsx";
        XSSFSheet mySheet = FileUtils.openFile(fileName);
        Map<String, Map<String, Map<String, Integer>>>result = elasticSearch.getReport(mySheet, type);
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.a90] DONE. duration=" + (timeEnd - timeStart));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "fullreport", method = RequestMethod.POST)
    public ResponseEntity<?> report(@RequestBody Map<String,String> paramMap){
        long timeStart = System.currentTimeMillis();
        String type = paramMap.get("type");
        String fileName = directory + File.separator + "constant" + File.separator + "MovieDetail.xlsx";
        XSSFSheet mySheet = FileUtils.openFile(fileName);
        Map<String, Map<String, Map<String, Integer>>>result = elasticSearch.getTotalActivityReport(mySheet, type);
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.full report] DONE. duration=" + (timeEnd - timeStart));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/packagename", method = RequestMethod.GET)
    public ResponseEntity<?> packageName(){
        long timeStart = System.currentTimeMillis();
        for(int i = 1; i <= 1; i++) {
            String fileName = directory + File.separator + "constant" + File.separator + "TOV-T" + i + "-User.xlsx";
            XSSFSheet mySheet = FileUtils.openFile(fileName);
            Map<String, String> result = userReportService.getPackageName(mySheet);
            ExcelExport.genericExport(result, "TOV-T" + i);
            long timeEnd = System.currentTimeMillis();
            logger.info("[REPORT.package name] DONE. duration=" + (timeEnd - timeStart));
        }
        return ResponseEntity.ok("finish");
    }

    @RequestMapping(value = "/getuserwatchtime", method = RequestMethod.GET)
    public ResponseEntity<?> getUserWatchTime(){
        long timeStart = System.currentTimeMillis();
        Map<String, Integer> result = null;
        for(int i = 1; i <= 6; i++) {
            String fileName = directory + File.separator + "constant" + File.separator + "A" + i + ".xlsx";
            XSSFSheet mySheet = FileUtils.openFile(fileName);
            result = elasticSearch.getTimeOnViewByUser(mySheet, "A"+i);
        }
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.user watch time] DONE. duration=" + (timeEnd - timeStart));

        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/getuserbygenres", method = RequestMethod.GET)
    public ResponseEntity<?> getUserByGenre(){
        Long timeStart = System.currentTimeMillis();
        String[] list = {"action", "romance", "series", "independent", "other", "viet"};
        for(String genre: list){
            Long reportStart = System.currentTimeMillis();
            elasticSearch.getUniqueUserByGenre(genre);
            long reportEnd = System.currentTimeMillis();
            logger.info("[REPORT." + genre + "] DONE. duration=" + (reportEnd - reportStart));
        }
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.user] DONE. duration=" + (timeEnd - timeStart));
        return ResponseEntity.ok("done");
    }

    @RequestMapping(value = "/geta30", method = RequestMethod.POST)
    public ResponseEntity<?> getA30Report(@RequestBody Map<String, String> paramMap){
        long timeStart = System.currentTimeMillis();
        org.joda.time.format.DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
        String fromDate = paramMap.get("fromDate");
        String toDate = paramMap.get("toDate");
        DateTime startDate = null;
        DateTime endDate = null;
        try{
            startDate = dtf.parseDateTime(fromDate);
            endDate = dtf.parseDateTime(toDate);
        }catch (Exception e){
            return ResponseEntity.ok("Invalid Date Format");
        }
        DateTime currentdate = startDate;
        List<DateTime> dateList = new ArrayList<>();
        while (currentdate.getMillis() <= endDate.getMillis()){
            dateList.add(currentdate);
            currentdate = currentdate.plusDays(1);
        }
        List<String> result = elasticSearch.getA30Report(dateList);
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.user] DONE. duration=" + (timeEnd - timeStart));
        return ResponseEntity.ok(result);
    }
}
