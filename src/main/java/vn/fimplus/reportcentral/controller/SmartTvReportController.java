package vn.fimplus.reportcentral.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import vn.fimplus.reportcentral.model.report.SmartTvReport.SmartTvReport;
import vn.fimplus.reportcentral.service.SmartTvReportService;
import vn.fimplus.reportcentral.util.ExcelExport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@RestController
@RequestMapping(value = "/api/smarttv")
public class SmartTvReportController extends AbstractController{

    @Autowired
    SmartTvReportService smartTvReportService;

    @RequestMapping(value = "/detailreport", method = RequestMethod.POST)
    public ResponseEntity<?> detailReport(@RequestBody Map<String, String> paramMap){
        Map<String, List<SmartTvReport>> result = smartTvReportService.getDetailReport(paramMap.get("platform"), paramMap.get("date"));
        ExcelExport.exportExcelSmartTv(result, paramMap.get("platform"));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/a30", method = RequestMethod.GET)
    public ResponseEntity<?> a30(){
        Map<String, Map<String, Map<String, Integer>>> result = new TreeMap<>();
        result.put("samsung", smartTvReportService.a30Report("samsung"));
        result.put("sony", smartTvReportService.a30Report("sony"));
        result.put("lg", smartTvReportService.a30Report("lg"));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/samsung/first")
    public ResponseEntity<?> samsungFristReport(@RequestBody Map<String, String> paramMap){
        Map<Integer, Map<String, Map<String, Integer>>> result = smartTvReportService.newBundlingSubcription("samsung", paramMap.get("date"));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/samsung/expired")
    public ResponseEntity<?> samsungExpiredReport(@RequestBody Map<String, String> paramMap){
        Map<Integer, Map<String, Map<String, Integer>>> result = smartTvReportService.getExpiredSubcription("samsung", paramMap.get("date"));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/samsung/independent")
    public ResponseEntity<?> samsungIndependentReport(@RequestBody Map<String, String> paramMap){
        Map<Integer, Map<String, Map<String, Integer>>> result = smartTvReportService.getIndependentFromBundling("samsung", paramMap.get("date"));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/samsung", method = RequestMethod.POST)
    public ResponseEntity<?> samsungReports(@RequestBody Map<String, String> paramMap){
        Map<String, Map<Integer, Map<String, Map<String, Integer>>>> result = new HashMap<>();
        result.put("Independent From Bundling", smartTvReportService.getIndependentFromBundling("samsung", paramMap.get("date")));
        result.put("Expired Subcription", smartTvReportService.getExpiredSubcription("samsung", paramMap.get("date")));
        result.put("First Transaction As Bundling", smartTvReportService.newBundlingSubcription("samsung", paramMap.get("date")));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/lg", method = RequestMethod.POST)
    public ResponseEntity<?> lgReports(@RequestBody Map<String, String> paramMap){
        Map<String, Map<Integer, Map<String, Map<String, Integer>>>> result = new HashMap<>();
        result.put("Independent From Bundling", smartTvReportService.getIndependentFromBundling("lg", paramMap.get("date")));
        result.put("Expired Subcription", smartTvReportService.getExpiredSubcription("lg", paramMap.get("date")));
        result.put("First Transaction As Bundling", smartTvReportService.newBundlingSubcription("lg", paramMap.get("date")));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/sony", method = RequestMethod.POST)
    public ResponseEntity<?> sonyReports(@RequestBody Map<String, String> paramMap){
        Map<String, Map<Integer, Map<String, Map<String, Integer>>>> result = new HashMap<>();
        result.put("Independent From Bundling", smartTvReportService.getIndependentFromBundling("sony", paramMap.get("date")));
        result.put("Expired Subcription", smartTvReportService.getExpiredSubcription("sony", paramMap.get("date")));
        result.put("First Transaction As Bundling", smartTvReportService.newBundlingSubcription("sony", paramMap.get("date")));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/platformreport", method = RequestMethod.POST)
    public ResponseEntity<?> platformReports(@RequestBody Map<String, String> paramMap){
        Map<String, Map<Integer, Map<String, Map<String, Integer>>>> result = new HashMap<>();
        result.put("Independent From Bundling", smartTvReportService.getIndependentFromBundling(paramMap.get("platform"), paramMap.get("date")));
        result.put("Expired Subcription", smartTvReportService.getExpiredSubcription(paramMap.get("platform"), paramMap.get("date")));
        result.put("First Transaction As Bundling", smartTvReportService.newBundlingSubcription(paramMap.get("platform"), paramMap.get("date")));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/userslist", method = RequestMethod.POST)
    public ResponseEntity<?> userListReport(@RequestBody Map<String, String> paramMap){
        long timeStart = System.currentTimeMillis();
        Map<String, Map<String, Map<String, List<String>>>> result = new TreeMap<>();
        String[] platformList = {"samsung", "sony", "lg"};
        for(String platform : platformList){
            Map<String, Map<String, List<String>>> result1 = result.get("platform");
            if(result1 == null){
                result1 = new TreeMap<>();
                result.put(platform, result1);
            }
            result1.put("Independent User List", smartTvReportService.getIndependentUserId(platform, paramMap.get("date")));
            result1.put("First Transaction As Bundling", smartTvReportService.getFirsttransUserId(platform, paramMap.get("date")));
        }
        ExcelExport.exportUserList(result, "userList");
        long timeEnd = System.currentTimeMillis();
        this.logger.info("[REPORT.getUsersList] DONE. duration=" + (timeEnd - timeStart));
        return ResponseEntity.ok(result);
    }
}
