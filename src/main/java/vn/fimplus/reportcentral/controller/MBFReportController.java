package vn.fimplus.reportcentral.controller;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import vn.fimplus.reportcentral.model.ApiResponse;
import vn.fimplus.reportcentral.service.MobifoneReportService;
import vn.fimplus.reportcentral.service.ReportService;
import vn.fimplus.reportcentral.service.UserActivityReportService;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/mbf")
public class MBFReportController extends AbstractController {

    @Autowired
    ReportService reportService;
    @Autowired
    MobifoneReportService mobifoneReportService;
    @Autowired
    UserActivityReportService userActivityReportService;

    private class ResultData {
        public Map<String, Map<String, Map<String, Integer>>> getData() {
            return data;
        }

        public void setData(Map<String, Map<String, Map<String, Integer>>> data) {
            this.data = data;
        }


        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public ResultData(int count, Map<String, Map<String, Map<String, Integer>>> data) {
            this.setTotalCount(count);
            this.setData(data);
        }

        public int getTotalCount() {
            return totalCount;
        }

        Map<String, Map<String, Map<String, Integer>>> data;
        int totalCount;
    }


    @RequestMapping(value = "/ftp")
    public ResponseEntity<?> reportFTP() {
        Date fromTime = null;
        Date toTime = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            //fromTime = sdf.parse(from);
           // toTime = sdf.parse(to);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(reportService.reportFTP(
                new DateTime().withDate(2017, 11, 10).withTime(0, 0, 0, 0),
                new DateTime().withDate(2017, 11, 11).withTime(5, 0, 0, 0)));
    }

    @RequestMapping(value = "/daily", method = RequestMethod.POST)
    public ResponseEntity<?> reportDaily(@RequestBody Map<String, String> paramMap) {
        Date fromTime = null;
        Date toTime = null;
        String from = paramMap.get("dateFrom");
        String to = paramMap.get("dateTo");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            fromTime = sdf.parse(from);
            toTime = sdf.parse(to);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(reportService.reportDaily(
                new DateTime().withDate(2017, 11, 10).withTime(0, 0, 0, 0),
                new DateTime().withDate(2017, 11, 11).withTime(5, 0, 0, 0)));
    }

    @RequestMapping(value = "/timereport", method = RequestMethod.POST)
    public ResponseEntity<?> reportMBFRenew(@RequestBody Map<String, String> paramMap) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
        //if (!accountService.verifyAuthentication(request, list))
        //    return responseError(ApiResponseConstant.MESSAGE.INVALID_TOKEN);

        DateTime fromTime;
        DateTime toTime;

        try {
            fromTime = dtf.parseDateTime(paramMap.get("dateFrom"));
            toTime = dtf.parseDateTime(paramMap.get("dateTo"));
        } catch (Exception e) {
            logger.info(e.getMessage());
            return ResponseEntity.ok(e.getMessage());
        }

        long timeStart = System.currentTimeMillis();
        Map<String, Map<String, Object>> result = mobifoneReportService.getMonthReport(paramMap.get("type"), fromTime, toTime);
        long timeEnd = System.currentTimeMillis();
        this.logger.info("[REPORT.getMonthReport] DONE. duration=" + (timeEnd - timeStart));

        return ResponseEntity.ok(ApiResponse.success(result));
    }

    @RequestMapping(value = "/useractivity", method = RequestMethod.GET)
    public ResponseEntity<?> reportTest() throws IOException {
        long timeStart = System.currentTimeMillis();
        Map<String, Map<String, Map<String, Integer>>> result = userActivityReportService.getMonthReport(new DateTime(), new DateTime());
        long timeEnd = System.currentTimeMillis();
        this.logger.info("[REPORT.test] DONE. duration=" + (timeEnd - timeStart));
        ResultData data = new ResultData(userActivityReportService.getCount(), result);

        return ResponseEntity.ok(ApiResponse.success(data));
    }
}
