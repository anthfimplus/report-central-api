package vn.fimplus.reportcentral.controller;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fimplus.reportcentral.model.ApiResponse;
import vn.fimplus.reportcentral.model.report.TransactionOppo;
import vn.fimplus.reportcentral.repository.TransactionRepository;
import vn.fimplus.reportcentral.repository.UserReportRepository;
import vn.fimplus.reportcentral.service.SubscriptionReportService;
import vn.fimplus.reportcentral.util.CipherUtils;
import vn.fimplus.reportcentral.util.ExcelExport;
import vn.fimplus.reportcentral.util.FileUtils;
import vn.fimplus.reportcentral.util.TestGoogleApi;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.*;


@Controller
@RequestMapping(value = "/test")
public class TestController extends AbstractController {

	private static String SPREADSHEET_ID = "1t0wRw8ZyWEZ6ESheqHHasIvm8-vTt04MwH4NuPeUIaE";
	@Autowired
	SubscriptionReportService subscriptionReportService;
	@Autowired
	UserReportRepository userReportRepository;
	@Autowired
	TransactionRepository transactionRepository;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public ResponseEntity<?> encrypt() {
		if ("Hành Động".contains("Hành"))
			System.out.println(0);
		else
			System.out.println(1);
		return ResponseEntity.ok("done");
	}

	@RequestMapping(value = "/decrypt", method = RequestMethod.POST)
	public ResponseEntity<?> decrypt(@RequestBody String body) {
		return ResponseEntity.ok(ApiResponse.success(CipherUtils.decryptDefault(body)));
	}

	@RequestMapping(value = "/googletest", method = RequestMethod.GET)
	public ResponseEntity<?> testGoogleApi(){
		List<TransactionOppo> result = transactionRepository.getOppoReport();
		Sheets sheetsService = null;
		try {
			sheetsService = TestGoogleApi.getSheetsService();
		String range = "A1:Z";
		ClearValuesRequest requestBody = new ClearValuesRequest();
		Sheets.Spreadsheets.Values.Clear request = sheetsService.spreadsheets().values().clear(SPREADSHEET_ID, range, requestBody);
		ClearValuesResponse clearResponse = request.execute();

		List<List<Object>> array = new ArrayList<>();
		array.add(Arrays.asList("transactionID", "userId", "platform", "createdAt", "expiredAt"));
		for(TransactionOppo aTransaction : result) {
			array.add(Arrays.asList(
					aTransaction.getId(),
					aTransaction.getUserId(),
					aTransaction.getPlatform(),
					aTransaction.getStringCreatedAt(),
					aTransaction.getStringExpiryDate()));
		}
		ValueRange body = new ValueRange().setValues(array);
		UpdateValuesResponse updateResponse = sheetsService.spreadsheets().values()
				.update(SPREADSHEET_ID, "A1", body)
				.setValueInputOption("RAW")
				.execute();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok("done");
	}

	@RequestMapping(value = "/sampletest", method = RequestMethod.GET)
	public ResponseEntity<?> report() {
		long timeStart = System.currentTimeMillis();
		List<String> result = userReportRepository.getList();
		long timeEnd = System.currentTimeMillis();
		logger.info("[REPORT.List] DONE. duration=" + (timeEnd - timeStart));
		ExcelExport.singleExport(result, "home_click_mbf");
		timeEnd = System.currentTimeMillis();
		logger.info("[REPORT.report] DONE. duration=" + (timeEnd - timeStart));
		return ResponseEntity.ok("");
	}

	@RequestMapping(value = "/sampletest2", method = RequestMethod.GET)
	public ResponseEntity<?> report2(){
		String directory = System.getProperty("user.dir");
		String fileName = directory + File.separator + "constant" + File.separator + "nontransuser.xlsx";
		XSSFSheet mySheet = FileUtils.openFile(fileName);
		Iterator<Row> rowIterator = mySheet.iterator();
		Map<String, Integer> compareMap = new TreeMap<>();
		while(rowIterator.hasNext()){
			Row row = rowIterator.next();
			compareMap.put(row.getCell(0).getStringCellValue(),1);
		}
		Map<String, Integer> result = transactionRepository.getNonTransReport(compareMap);
		return ResponseEntity.ok(result);
	}

	@RequestMapping(value = "/sampletest3", method = RequestMethod.GET)
	public ResponseEntity<?> report3(){
		Map<String, Map<String,Integer>> result = transactionRepository.getDailyReport();
		return ResponseEntity.ok(result);
	}
}
