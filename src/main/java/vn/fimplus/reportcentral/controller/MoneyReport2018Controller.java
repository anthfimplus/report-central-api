package vn.fimplus.reportcentral.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import vn.fimplus.reportcentral.repository.MoneyReport2018Resposistory;
import vn.fimplus.reportcentral.service.MoneyReport2018Service;
import vn.fimplus.reportcentral.util.ExcelExport;
import vn.fimplus.reportcentral.model.ApiResponse;

import java.util.*;

@RestController
@RequestMapping(value = "/api/newreport")
public class MoneyReport2018Controller {

    @Autowired
    MoneyReport2018Service moneyReport2018Service;
    @Autowired
    MoneyReport2018Resposistory moneyReport2018Resposistory;

    protected final Log logger = LogFactory.getLog(getClass());

    @RequestMapping(value = "svodreport", method = RequestMethod.POST)
    public ResponseEntity<?> svodReport(@RequestBody Map<String, String> paramMap){
        long timeStart = System.currentTimeMillis();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime datein = new DateTime();
        try{
            datein = dtf.parseDateTime(paramMap.get("dateTo"));
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Map<String, Object> result = moneyReport2018Service.getSvodReport(datein);
        System.out.println(result);
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.tvod] DONE. duration=" + (timeEnd - timeStart));
        //ExcelExport.exportExcel(result, "NewReport_SVOD" + datein.getMonthOfYear() + datein.getYear());
        return ResponseEntity.ok(ApiResponse.success(result));
    }

    @RequestMapping(value = "test", method = RequestMethod.POST)
    public ResponseEntity<?> test(@RequestBody Map<String, String> paramMap){
        long timeStart = System.currentTimeMillis();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime datein = new DateTime();
        try{
            datein = dtf.parseDateTime(paramMap.get("dateTo"));
        }
        catch (Exception e){
            e.printStackTrace();
        }
        List<String >result = moneyReport2018Service.test(datein);
        return  ResponseEntity.ok(ApiResponse.success(result));
    }

    @RequestMapping(value = "tvodreport", method = RequestMethod.POST)
    public ResponseEntity<?> tvodReport(@RequestBody Map<String, String> paramMap){
        long timeStart = System.currentTimeMillis();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime fromDate = new DateTime();
        DateTime toDate = new DateTime();
        try{
            fromDate = dtf.parseDateTime(paramMap.get("dateFrom")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
            toDate = dtf.parseDateTime(paramMap.get("dateTo")).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, Object> result = moneyReport2018Service.getTvodReport(fromDate, toDate);
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.tvod] DONE. duration=" + (timeEnd - timeStart));
        //ExcelExport.exportExcel(result, "NewReport_TVOD" + fromDate.getMonthOfYear() + fromDate.getYear());
        return ResponseEntity.ok(ApiResponse.success(result));
    }

    @RequestMapping(value = "packagecode", method = RequestMethod.POST)
    public ResponseEntity<?> packageCodeCount(@RequestBody Map<String, String> paramMap){
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime fromDate = new DateTime();
        DateTime toDate = new DateTime();
        try{
            fromDate = dtf.parseDateTime(paramMap.get("dateFrom")).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
            toDate = dtf.parseDateTime(paramMap.get("dateTo")).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        Map<String, Integer> result = moneyReport2018Resposistory.packageCount(fromDate, toDate);
        ExcelExport.exportExcel(result, "NewReport_MBF_Package" + fromDate.getMonthOfYear() + fromDate.getYear());
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "fullreport", method = RequestMethod.POST)
    public ResponseEntity<?> fullReport(@RequestBody Map<String, String> paramMap){
        long timeStart = System.currentTimeMillis();
        DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime fromDate = new DateTime();
        DateTime toDate = new DateTime();
        try{
            fromDate = dtf.parseDateTime(paramMap.get("dateFrom"));
            toDate = dtf.parseDateTime(paramMap.get("dateTo"));
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Map<String, Object> result1 = moneyReport2018Service.getSvodReport(toDate);
        System.out.println("Svod Completed");
        Map<String,Object> result2 = moneyReport2018Service.getTvodReport(fromDate, toDate);
        System.out.println("Tvod completed");
        Map<String, Integer> result3 = moneyReport2018Resposistory.packageCount(fromDate, toDate);
        System.out.println("PackageCount completed");
        Map<String, Object> result = new HashMap<>();
        result.putAll(result1);
        result.putAll(result2);
        result.put("MBF packages", result3);
        //ExcelExport.exportExcel(result, "NewReport" + fromDate.getMonthOfYear() + fromDate.getYear());
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.fullreport] DONE. duration=" + (timeEnd - timeStart));
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/periodicreport", method = RequestMethod.POST)
    public ResponseEntity<?> periodicReport(@RequestBody Map<String, Object> paramMap){
        long timeStart = System.currentTimeMillis();
        String apiType = (String)paramMap.get("apiType");
        ArrayList dateList = (ArrayList) paramMap.get("date");
        Map<String, Map<String, Integer>> result = moneyReport2018Service.periodic(dateList,apiType);
        long timeEnd = System.currentTimeMillis();
        logger.info("[REPORT.periodic] DONE. duration=" + (timeEnd - timeStart));
        return ResponseEntity.ok(ApiResponse.success(result));
    }
}
