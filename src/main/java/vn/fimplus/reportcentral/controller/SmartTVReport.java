package vn.fimplus.reportcentral.controller;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fimplus.reportcentral.constant.ApiResponseConstant;
import vn.fimplus.reportcentral.service.AccountService;

import java.util.Map;

public class SmartTVReport extends AbstractController{
    @Autowired
    AccountService accountService;
    @Autowired


    @RequestMapping(value = "/smartTV/new", method = RequestMethod.POST)
    public ResponseEntity<?> reportSmartTV(@RequestBody Map<String, Object> paramMap){
        Object[] list = {paramMap.get("dateFrom"), paramMap.get("dateTo")};

        if(!accountService.verifyAuthentication(request,list))
            return responseError(ApiResponseConstant.MESSAGE.INVALID_TOKEN);

        DateTime fromTime = null;
        DateTime toTime = null;

        try{
            fromTime = new DateTime(Long.parseLong((String)list[0]));
            toTime = new DateTime(Long.parseLong((String)list[1]));
        }catch(Exception e){
            return ResponseEntity.ok(e.getMessage());
        }


        return ResponseEntity.ok("result");
    }
}
