package vn.fimplus.reportcentral.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import vn.fimplus.reportcentral.constant.ApiResponseConstant;
import vn.fimplus.reportcentral.constant.SystemConstant;
import vn.fimplus.reportcentral.model.ApiResponse;

import javax.servlet.http.HttpServletRequest;

public class AbstractController {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	HttpServletRequest request;
	
	public String getUserToken() {
		return request.getHeader(SystemConstant.USER_TOKEN);
	}
	
	public ResponseEntity<ApiResponse> responseOK(Object message) {
		return ResponseEntity.ok(ApiResponse.success(message));
	}
	
	public ResponseEntity<ApiResponse> responseError(String message) {
		return ResponseEntity.ok(ApiResponse.error(ApiResponseConstant.STATUS.ERROR, message));
	}
}
