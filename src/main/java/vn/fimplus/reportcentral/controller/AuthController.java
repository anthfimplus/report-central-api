package vn.fimplus.reportcentral.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.util.MultiValueMap;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fimplus.reportcentral.constant.ApiResponseConstant;
import vn.fimplus.reportcentral.model.ApiResponse;
import vn.fimplus.reportcentral.model.common.Common_User;
import vn.fimplus.reportcentral.service.AccountService;
import vn.fimplus.reportcentral.util.JsonUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping(value = "/api/auth")
public class AuthController extends AbstractController {
	
	@Autowired
	private AccountService accountService;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)

	public ResponseEntity<?> login(@RequestBody Map<String, Object> paramMap) {
		String username = (String) paramMap.get("username");
		String password = (String) paramMap.get("password");

		if (username == null || username.isEmpty() || password == null || password.isEmpty())
			return responseError(ApiResponseConstant.MESSAGE.WRONG_FORMAT);
		Common_User user = accountService.login(username, password);
		if (user != null) {
			Map<String, Object> responseData = new HashMap<>();
			responseData.put("token", user.getToken());
			responseData.put("user", user);
//			String permissString = user.getPermissions();
//			List<String> permiss = new ArrayList<String>();
//			if (permissString.length() > 0) {
//				permissString = permissString.substring(1, permissString.length() - 1);
//				permiss = Arrays.asList(permissString.split("%"));
//			}
//			responseData.put("permission", (Object) permiss);

			logger.info(JsonUtils.generateJson(responseOK(responseData)));
			return responseOK(responseData);
		}
		return responseError(ApiResponseConstant.MESSAGE.WRONG_PASSWORD);
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ResponseEntity<?> logout() {

		if (!accountService.verifyAuthentication(request))
			return responseError(ApiResponseConstant.MESSAGE.INVALID_TOKEN);
		accountService.logout(this.getUserToken());
		return responseOK(null);
	}

}
