package vn.fimplus.reportcentral.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import vn.fimplus.reportcentral.model.report.TransactionOppo;
import vn.fimplus.reportcentral.repository.TransactionRepository;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Controller
@RequestMapping(value = "/api/oppo")
public class OppoProject extends AbstractController{

    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    TestController testController;


    @RequestMapping(value = "/getreport", method = RequestMethod.GET)
    public ResponseEntity monitor(){
            long timeStart = System.currentTimeMillis();
            List<TransactionOppo> result = transactionRepository.getOppoReport();
            long timeEnd = System.currentTimeMillis();
            this.logger.info("[REPORT.test] DONE. duration=" + (timeEnd - timeStart));
            return ResponseEntity.ok("");
    }

    @RequestMapping(value = "/monitor", method = RequestMethod.GET)
    public ResponseEntity runMonitor(){
        Timer time = new Timer();
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Running Oppo Report");
                testController.testGoogleApi();
                System.out.println("Oppo Report Complete");
            }
        }, 0, 3600000);
        return ResponseEntity.ok("");
    }
}
