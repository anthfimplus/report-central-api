$(document).ready(function(){

    // SUBMIT FORM
    $("#tvod-form").submit(function(event) {
        // Prevent the form from submitting via the browser.
        event.preventDefault();

        var inputs = $(this).find('input');

        // prepare data from input-form
        var data = {
            'fromDate' : $(inputs[0]).val().toString(),
            'toDate' : $(inputs[1]).val().toString()
        }

        ajaxPost(data, "/api/subscription/tvod");

        // reset input data
        // $(inputs[0]).val("");
        // $(inputs[1]).val("")
    });

    $("#mbf-form").submit(function (event) {
        event.preventDefault();

        var data = {
            'packageCode': "FIM30",
            'fromDate': $('#from-date').val().toString(),
            'toDate': $('#to-date').val().toString()
        };

        ajaxPost(data, "/api/report/mbf")
    });

    function ajaxPost(data, target){

        // DO POST
        $.ajax({
            type : "POST",
            contentType: "application/json",
            url : target,
            data : JSON.stringify(data),
            dataType: 'json',
            success : function(result) {
                $("#postResultMsg").html("<p style='background-color:#7FA7B0; color:white; padding:20px 20px 20px 20px'>" + JSON.stringify(result));
            },
            error : function(e) {
                console.log(e);
                alert("Error!");
            }
        });

    }

});